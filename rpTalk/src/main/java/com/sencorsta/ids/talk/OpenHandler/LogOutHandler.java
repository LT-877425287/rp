package com.sencorsta.ids.talk.OpenHandler;

import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.PushKickOffMessage;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.MsgService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.talk.proto.ImtalkLogOutMessage;
import io.netty.channel.Channel;

/**
* @description: 主动退出
* @author TAO
* @date 2019/7/2 0:40
*/
@ClientEvent("LogOut")
public class LogOutHandler extends OpenMessageHandler implements RedisService {

    @Override
    public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {
        Out.debug("------------------------LogOut主动退出------------------------------");
        ImtalkLogOutMessage.LogOutRes.Builder res=ImtalkLogOutMessage.LogOutRes.newBuilder();
        PushKickOffMessage.PushKickOff.Builder push=PushKickOffMessage.PushKickOff.newBuilder();

        try{
            ImtalkLogOutMessage.LogOutReq req=ImtalkLogOutMessage.LogOutReq.parseFrom(protobuf);

            push.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.ZKICKOFF));

            MsgService.pushAndSaved(pushProto(userId,"push.KickOff",push));

            return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SUCCESS)));
        }catch (InvalidProtocolBufferException e){
            e.printStackTrace();
            return  responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SERVER_ERROR)));
        }
    }
}
