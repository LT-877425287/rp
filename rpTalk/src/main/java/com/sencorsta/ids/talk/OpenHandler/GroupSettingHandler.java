package com.sencorsta.ids.talk.OpenHandler;

import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.*;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.talk.proto.ImtalkGroupSettingByProto;
import com.sencorsta.ids.talk.proto.ImtalkPushGroupUpdateMsg;
import com.sencorsta.ids.talk.proto.ImtalkPushMessageByProto;
import com.sencorsta.utils.date.DateUtil;
import io.netty.channel.Channel;

import java.util.List;
import java.util.Map;

/**
* @description: 群信息修改
* @author TAO
* @date 2019/7/15 10:06
*/

//TODO 这里走第二种方式
@ClientEvent("GroupSetting")
public class GroupSettingHandler extends OpenMessageHandler implements RedisService {

    @Override
    public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {

        Out.debug("---------------------群信息修改接口总和GroupSetting-------------------------");

        ImtalkGroupSettingByProto.GroupSettingRes.Builder res=ImtalkGroupSettingByProto.GroupSettingRes.newBuilder();

        //服务器推的群状态跟新
        ImtalkPushGroupUpdateMsg.PushGroupUpdate.Builder pushUpdate= ImtalkPushGroupUpdateMsg.PushGroupUpdate.newBuilder();

        //服务器推送的消息
        ImtalkPushMessageByProto.PushMsg.Builder pushsys= ImtalkPushMessageByProto.PushMsg.newBuilder();

        try{
            ImtalkGroupSettingByProto.GroupSettingReq req=ImtalkGroupSettingByProto.GroupSettingReq.parseFrom(protobuf);
            res.setSign(req.getSign());//确认前后端标识

            //得到群Id
            String groupId=req.getGroupId();

            //得到修改类型集合
            List<Integer> typeList=req.getTypeList();

            //得到用户要修改的信息
            List<String> dataList=req.getDataList();

            Out.debug("typeList---------------"+typeList);
            Out.debug("dataList---------------"+dataList);

            if (typeList.size()!=dataList.size()){//数据有误
                return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.DEFICIENCY)));
            }

            //通过群id得到群的详情信息---用作当前的有些选项是否可改----比如选项8
            Map<String, String> groupInfo=R_PROPERTY.hgetAll(groupId);

            //发起者的详情信息
            JSONObject userInfo= JSONObject.parseObject((R_GROUP.hget(groupId,userId)));
            //权限判断

            //修改的消息内容
            String msgContent="";
            String text="";
            for (int i=0;i<typeList.size();i++){

                int type=typeList.get(i);

                String _data=dataList.get(i);

                Out.debug("---------------------这次修改的类型"+type+"这次修改的数据"+_data+"-----------------------------------------");

                if (type!=1&&type!=11){//排除修改群名和公告
                    if ("1".equals(_data)){
                        _data="0";
                        text="禁用";
                    }else{
                        _data="1";
                        text="开启";
                    }
                }

                if (type==1){
                    if(!groupInfo.get("update").equals("1")){
                        return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.JURISDICTION_INSUFFICIENT)));
                    }
                }else if (userInfo.get("adminType").equals("3")) {
                    return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.JURISDICTION_INSUFFICIENT)));
                }

                switch(type){
                    case 1:
                        //群聊名称修改
                        Out.debug("--------------群聊名称修改---------------");
                        R_PROPERTY.hset(groupId,"groupName",_data);
                        groupInfo.put("groupName",_data);
                        msgContent+="群聊名称修改为"+_data+"\n\n";
                        break;
                    case 2:
                        //是否禁言（是，否）
                        Out.debug("---------------是否禁言--------------");
                        R_PROPERTY.hset(groupId,"silent",_data);
                        groupInfo.put("silent",_data);
                        //获取消息内容
                        msgContent+=text+"禁言"+"\n\n";
                        break;
                    case 3:
                        //是否可以成员间互聊（是，否）
                        Out.debug("---------------是否可以成员间互聊--------------");
                        R_PROPERTY.hset(groupId,"chat",_data);
                        groupInfo.put("chat",_data);
                        msgContent+=text+"成员间互聊"+"\n\n";
                        break;
                    case 4:
                        //是否群成员可以邀请联系人入群（是，否）
                        Out.debug("---------------是否群成员可以邀请联系人入群--------------");
                        R_PROPERTY.hset(groupId,"invite",_data);
                        groupInfo.put("invite",_data);
                        msgContent+=text+"成员邀请联系人入群"+"\n\n";
                        break;
                    case 5:
                        //是否群成员发送图片和视频（是，否）
                        Out.debug("------------是否群成员发送图片和视频-----------------");
                        R_PROPERTY.hset(groupId,"videoimages",_data);
                        groupInfo.put("videoimages",_data);
                        msgContent+=text+"成员发送图片和视频"+"\n\n";
                        break;
                    case 6:
                        //是否群成员发送文件（是，否）
                        Out.debug("-----------------是否群成员发送文件------------");
                        R_PROPERTY.hset(groupId,"file",_data);
                        groupInfo.put("file",_data);
                        msgContent+=text+"成员发送文件"+"\n\n";
                        break;
                    case 7:
                        //是否群成员发送长文（是，否）注：关闭后群成员仅能发送最多40个汉字
                        Out.debug("-------------是否群成员发送长文----------------");
                        R_PROPERTY.hset(groupId,"longText",_data);
                        groupInfo.put("longText",_data);
                        msgContent+=text+"成员发送长文"+"\n\n";
                        break;
                    case 8:
                        //允许群成员修改群名称和群头像（是，否）注：关闭后仅群主及管理员可以设置
                        Out.debug("-------------允许群成员修改群名称和群头像----------------");
                        R_PROPERTY.hset(groupId,"update",_data);
                        groupInfo.put("update",_data);
                        msgContent+=text+"成员修改群名称和群头像"+"\n\n";
                        break;
                    case 9:
                        //是否群成员之间互相添加好友（是，否）
                        Out.debug("-------------是否群成员之间互相添加好友----------------");
                        R_PROPERTY.hset(groupId,"add",_data);
                        groupInfo.put("add",_data);
                        msgContent+=text+"成员互相添加好友"+"\n\n";
                        break;
                    case 10:
                        //是否群成员之间禁止查看个人信息（仅管理员）
                        Out.debug("-----------是否群成员之间禁止查看个人信息------------------");
                        R_PROPERTY.hset(groupId,"info",_data);
                        groupInfo.put("info",_data);
                        msgContent+=text+"成员查看个人信息"+"\n\n";
                        break;
                    case 11:
                        //公告
                        Out.debug("--------------公告---------------");
                        R_PROPERTY.hset(groupId,"Announcement",_data);
                        groupInfo.put("Announcement",_data);
                        msgContent+=_data+"\n\n";
                        break;
                }
            }

            Object[] memberList = R_GROUP.hkeys(groupId).toArray();

            Out.debug("本群共多少人---------------------"+memberList.length);

            pushUpdate.setGroupId(groupId);//群聊id
            //循环群列表给每个接收者发送更新
            for (Object receiver: memberList){
                Out.debug("=======================groupUpdate群信息修改接口总和GroupSetting==================================");
                MsgService.pushAndSavedUnique((pushProto(String.valueOf(receiver),"push.groupUpdate",pushUpdate)),"push.groupUpdate-"+receiver);
            }

            //定义系统消息
            JSONObject msg=new JSONObject();

            Out.debug("---------------------综合的消息"+msgContent+"-------------------------------------");

            msg.put("srcId","0");
            //获取目标用户id
            msg.put("tagId",groupId);
            //获取消息内容
            msg.put("msgContent",msgContent);
            //内容类型 1.文字 2.图片  等等...--0.系统消息
            msg.put("msgType",1014);
            //获取消息类型1.单发  2.群发
            msg.put("tagType",2);
            //消息时间
            msg.put("time", DateUtil.getDateTime());
            //添加Redis的消息库中
            String msgId= IdService.newMsgId();
            R_MESSAGES.set(msgId,msg.toJSONString());

            pushsys.setUserId(groupId);
            pushsys.setMsgId(msgId);

            //AuroraPush.pushAurora(msgId);//极光推送
            //系统推送消息
            for (Object receiver: memberList){
                MsgService.pushAndSaved(pushProto(String.valueOf(receiver),"push.Msg",pushsys));
            }

            res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SUCCESS));
            return responseProto(res);
        }catch (InvalidProtocolBufferException e){
            e.printStackTrace();
            return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SERVER_ERROR)));
        }
    }

}
