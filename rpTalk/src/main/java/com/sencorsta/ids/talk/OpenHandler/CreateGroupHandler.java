package com.sencorsta.ids.talk.OpenHandler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.*;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.talk.proto.ImtalkCreateGroupByProto;
import com.sencorsta.ids.talk.proto.ImtalkPushGroupUpdateMsg;
import com.sencorsta.ids.talk.proto.ImtalkPushMessageByProto;
import com.sencorsta.utils.date.DateUtil;
import io.netty.channel.Channel;

import java.util.List;


/**
 * @description: 创建群聊
 * @author TAO
 * @date 2019/6/19 14:28
 */

@ClientEvent("CreateGroup")
public class CreateGroupHandler extends OpenMessageHandler implements RedisService {
    @Override
    public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {
        Out.debug("---------------------------------创建群CreateGroup--------------------------------");
        //创建创建群的Proto的res返回前端的对象
        ImtalkCreateGroupByProto.CreateGroupRes.Builder res= ImtalkCreateGroupByProto.CreateGroupRes.newBuilder();

        //服务器推的群状态跟新
        ImtalkPushGroupUpdateMsg.PushGroupUpdate.Builder pushUpdate= ImtalkPushGroupUpdateMsg.PushGroupUpdate.newBuilder();

        //服务器推送的消息
        ImtalkPushMessageByProto.PushMsg.Builder pushsys= ImtalkPushMessageByProto.PushMsg.newBuilder();

        try{
            ImtalkCreateGroupByProto.CreateGroupReq req= ImtalkCreateGroupByProto.CreateGroupReq.parseFrom(protobuf);
            res.setSign(req.getSign());//返回前端标识信息
            //发起人

            //得到邀请的好友列表
            List<String> list=req.getFriendListList();

            if (list.size()<=1){//创建群的成员列表为空
                return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.DEFICIENCY)));
            }

            //准备群成员的默认权限列表
            JSONObject siro=new JSONObject();//权限列表对象
            siro.put("adminType",3);//这里1代表是  0代表否  1.群主  2.管理员
            siro.put("silent",1);//后期直接用对接key匹配    3.禁言
            siro.put("nickname","");//                      4.在本群的昵称

            //群id
            String groupId= IdService.newGroupId();
            String groupName="";//设置群名
            int count=list.size();//群聊人数
            for (int i = 0; i < list.size(); i++) {
                //找出群主---并设置
                String userIdtemp=list.get(i);
                if (userIdtemp.equals(userId)){
                    siro.put("adminType",1);
                }
                siro.put("nickname",R_USER.hget(userIdtemp,"nickname"));//通过邀请的好友的id得到用户的昵称
                R_GROUP.hset(groupId,userIdtemp, String.valueOf(siro));//设置群成员权限
                siro.put("adminType",3);//将管理员权限撤销
                if (i<3){
                    groupName+=R_USER.hget(String.valueOf(list.get(i)),"nickname")+"、";
                }
            }



            groupName=groupName.substring(0,groupName.length()-1)+"...";//群聊名
            //设置群的基本信息      一下配置0代表停用   1代表开启
            R_PROPERTY.hset(groupId,"groupName",groupName);//群名
            R_PROPERTY.hset(groupId,"count", String.valueOf(count));//当前人数
            R_PROPERTY.hset(groupId,"main",userId);//设置群主
            R_PROPERTY.hset(groupId,"silent", String.valueOf(1));//设置禁言                     是否禁言
            R_PROPERTY.hset(groupId,"chat", String.valueOf(1));//是否开启聊天                   是否可以成员间互聊
            R_PROPERTY.hset(groupId,"invite", String.valueOf(1));//是否开启邀请                 是否群成员可以邀请联系人入群（是，否）
            R_PROPERTY.hset(groupId,"videoimages", String.valueOf(1));//是否开启图片/视频       是否群成员发送图片和视频（是，否）
            R_PROPERTY.hset(groupId,"file", String.valueOf(1));//是否开启文件                   是否群成员发送文件（是，否）
            R_PROPERTY.hset(groupId,"longText", String.valueOf(1));//是否开启发送长文           是否群成员发送长文（是，否）注：关闭后群成员仅能发送最多40个汉字
            R_PROPERTY.hset(groupId,"update", String.valueOf(1));//是否开启update               是否群成员修改群名称和群头像（是，否）注：关闭后仅群主及管理员可以设置
            R_PROPERTY.hset(groupId,"add", String.valueOf(1));//是否开启add                     是否群成员之间互相添加好友（是，否）
            R_PROPERTY.hset(groupId,"info", String.valueOf(1));//是否开启forbid                 是否群成员之间禁止查看个人信息（仅管理员）
            R_PROPERTY.hset(groupId,"Announcement", "欢迎加入本群");//是否开启forbid                 是否群成员之间禁止查看个人信息（仅管理员）

            //设置用户与群的关系
            R_USER_R_GROUP.lpush(userId,groupId);

            pushUpdate.setGroupId(groupId);//群聊id
            //循环群列表给每个接收者发送更新
            for (String receiver: list){
                MsgService.pushAndSavedUnique((pushProto(receiver,"push.groupUpdate",pushUpdate)),"push.groupUpdate-"+receiver);
            }

            //定义系统消息
            JSONObject msg=new JSONObject();
            msg.put("srcId","0");
            //获取目标用户id
            msg.put("tagId",groupId);
            //获取消息内容
            //msg.put("msgContent","欢迎加入群聊，现在可以聊天了");
            String msgContent="欢迎加入群聊，现在可以聊天了";
            msg.put("msgContent",msgContent);
            //内容类型 1.文字 2.图片  等等...--0.系统消息
            msg.put("msgType",1000);
            //获取消息类型1.单发  2.群发
            msg.put("tagType",2);
            //消息时间
            String time= DateUtil.getDateTime();
            msg.put("time",time);
            //添加Redis的消息库中
            String msgId= IdService.newMsgId();
            R_MESSAGES.set(msgId,msg.toJSONString());

            pushsys.setUserId(groupId);
            pushsys.setMsgId(msgId);

            //AuroraPush.pushAurora(msgId);//极光推送
            //系统推送消息
            for (String receiver: list){
                MsgService.pushAndSaved(pushProto(String.valueOf(receiver),"push.Msg",pushsys));
            }


            res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SUCCESS));
            return responseProto(res);
        }catch (InvalidProtocolBufferException e){
            e.printStackTrace();
            return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SERVER_ERROR)));
        }
    }

}
