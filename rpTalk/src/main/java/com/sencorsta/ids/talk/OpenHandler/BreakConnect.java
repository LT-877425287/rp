package com.sencorsta.ids.talk.OpenHandler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.MsgService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.configure.TypeSerialize;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.talk.proto.ImtalkBreakConnectByProto;
import io.netty.channel.Channel;

import java.util.List;
import java.util.Map;

/**
 * @author TAO
 * @description: 获取离线消息
 * @date 2019/6/21 10:07
 */
@ClientEvent("BreakConnect")
public class BreakConnect extends OpenMessageHandler implements RedisService {
    @Override
    public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {
        Out.info("---------------------------离线推送Offline message---------------------------");
        ImtalkBreakConnectByProto.BreakConnectRes.Builder res = ImtalkBreakConnectByProto.BreakConnectRes.newBuilder();

        try {
            ImtalkBreakConnectByProto.BreakConnectReq req = ImtalkBreakConnectByProto.BreakConnectReq.parseFrom(protobuf);
            res.setSign(req.getSign());//前后端消息标识

            //通过上线用户id从redis中得到mail信息
            List<String> maillist = (R_OFF_LINE_MAIL.lrangeAll(MAILLIST + userId));
            Map<String, String> mailhash = (R_OFF_LINE_MAIL.hgetAll(MAILHASH + userId));
            for (String str : maillist) {
                JSONObject js = JSON.parseObject(str);
                RpcMessage msg = pushBytes(userId, js.getString("path"), js.getBytes("data"), TypeSerialize.TYPE_PROTOBUF);
                MsgService.pushAndSaved(msg);
            }

            for (String key : mailhash.keySet()) {
                String str = mailhash.get(key);
                JSONObject js = JSON.parseObject(str);
                RpcMessage msg = pushBytes(userId, js.getString("path"), js.getBytes("data"), TypeSerialize.TYPE_PROTOBUF);
                MsgService.pushAndSavedUnique(msg, key);
            }
            //将当前用户的离线消息清空
            Out.debug("--------------------离线消息清空----------------");
            Out.debug("--------------------离线消息清空----------------");
            Out.debug("--------------------离线消息清空----------------");


            R_OFF_LINE_MAIL.del(MAILLIST + userId);
            R_OFF_LINE_MAIL.del(MAILHASH + userId);


            return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SUCCESS)));
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SERVER_ERROR)));
        }
    }
}
