package com.sencorsta.ids.talk.http;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.ParamsVerify;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 好友设置
* @author TAO
* @date 2019/8/1 15:41
*/
public class FriendSetting extends HttpHandler implements RedisService {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-------------------好友设置FriendSetting------------------");
        //入参非空验证
        String result= ParamsVerify.params(params);
        if(result!=null){
            return result;
        }

        int type=params.getInteger("type");

        String data=params.getString("data");

        String tagId=params.getString("tagId");

        String userId=params.getString("userId");

        switch (type){
            case 1://设置黑名单blacklist
                Out.debug("---------------------设置黑名单blacklist--------------------");
                break;
            case 2://设置置顶stick
                Out.debug("---------------------设置置顶stick--------------------");
                break;
            case 3://修改备注remark
                Out.debug("---------------------修改备注remark--------------------");
                JSONObject friendInfo=JSONObject.parseObject(R_FRIEND.hget(userId,tagId));
                friendInfo.put("remark",data);
                R_FRIEND.hset(userId,tagId, String.valueOf(friendInfo));
                break;
        }
        return success();
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Talk/FriendSetting";
    }
}
