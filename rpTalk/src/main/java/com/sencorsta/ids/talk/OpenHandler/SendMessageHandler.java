package com.sencorsta.ids.talk.OpenHandler;


import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
//import com.sencorsta.ids.common.service.AuroraPush;
import com.sencorsta.ids.common.service.MsgService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
//import com.sencorsta.ids.talk.imTalk;
import com.sencorsta.ids.talk.proto.ImtalkPushMessageByProto;
import com.sencorsta.ids.talk.proto.ImtalkSendMessageByProto;
import io.netty.channel.Channel;

import java.util.Arrays;
import java.util.Set;

/**
* @description: 发送消息
* @author TAO
* @date 2019/6/14 14:17
*/

@ClientEvent("SendMessage")
public class SendMessageHandler extends OpenMessageHandler implements RedisService {

	@Override
	public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {
		Out.debug("-------------------------发送消息SendMessage--------------------------------");
		Out.debug("执行方法: -> SendMessage参数:"+Arrays.toString(protobuf));


		ImtalkSendMessageByProto.SendMessageRes.Builder res=ImtalkSendMessageByProto.SendMessageRes.newBuilder();
		ImtalkPushMessageByProto.PushMsg.Builder push= ImtalkPushMessageByProto.PushMsg.newBuilder();
		try{

			ImtalkSendMessageByProto.SendMessageReq req=ImtalkSendMessageByProto.SendMessageReq.parseFrom(protobuf);
			res.setSign(req.getSign());//前后端标识
			String tagId=req.getTagId();//获取目标用户id
			String msgId=req.getMsgId();///获取消息id

			Out.info("tagId"+tagId);
			Out.info("msgId"+msgId);
			//通过msgId取出消息详情
			JSONObject jsonObject=JSONObject.parseObject(R_MESSAGES.get(msgId));

			Out.debug("------------------------"+jsonObject);
			//取出消息类型
			int type= Integer.parseInt(jsonObject.get("tagType").toString());

			//封装服务器推送的信息
			push.setMsgId(msgId);//消息详情Id
			push.setUserId(userId);//发送用户Id

			//通过消息type开始推送交易给目标
			if (type==1){
				Out.debug(userId);
				Out.debug(push.getUserId());

				RpcMessage msg=pushProto(tagId,"push.Msg",push);
				//imTalk.getInstance().process.addEvent(()->{
				//	AuroraPush.pushAurora(msgId);//极光推送
				//});

				MsgService.pushAndSaved(msg);
			}else{
				//通过目标id转换得到得到群
				String groupid=tagId;//得到群id

				//得到该群成员
				Set<String> groupList=R_GROUP.hkeys(groupid);

				//发送方的id    这里是群发所以是群id
				push.setUserId(groupid);
				//AuroraPush.pushAurora(msgId);//极光推送
				for (String receiver: groupList){
					if (!receiver.equals(userId)){
						Out.debug("开始给-----------------------------------"+receiver);
						MsgService.pushAndSaved(pushProto(String.valueOf(receiver),"push.Msg",push));
					}
				}
			}

			//返回前端信息
			res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SUCCESS));
			return responseProto(res);
		}catch (InvalidProtocolBufferException e){
			e.printStackTrace();
			res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SERVER_ERROR));
			return responseProto(res);
		}
	}
}
