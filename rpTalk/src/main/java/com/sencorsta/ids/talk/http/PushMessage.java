package com.sencorsta.ids.talk.http;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.IdService;
import com.sencorsta.ids.common.service.ParamsVerify;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.date.DateUtil;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @author TAO
 * @description: 用户发送消息
 * @date 2019/6/13 13:23
 */
public class PushMessage extends HttpHandler implements RedisService {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-------------------------用户发送消息--PushMessage------------------------");
        //入参非空验证
        String result = ParamsVerify.params(params);
        if (result != null) {
            return result;
        }
        //用户Id
        String userId = params.getString("userId");
        //目标id
        String tagId = params.getString("tagId");
        //消息内容
        String msgContent = params.getString("msgContent");
        //消息类型
        String tagTypey = params.getString("tagType");
        //预留字段1
        int fromtype = params.getInteger("fromtype");
        String fromcontent = params.getString("fromcontent");






        //强转int
        int tagTyp = Integer.parseInt(tagTypey);

        //这里做个人、群聊是否存在
        switch (tagTyp){
            case 1://个人
                if(!R_USER.exists(tagId)){
                    Out.debug("该用户不存在"+tagId);
                    return error(ErrorCodeList.TAGID_IS_NULL);
                }
                if (StringUtil.isEmpty(R_FRIEND.hget(tagId,userId))){
                    return error(ErrorCodeList.TD_RELIEVE_FRIEND);
                }
                if (StringUtil.isEmpty(R_FRIEND.hget(userId,tagId))){
                    return error(ErrorCodeList.TD_RELIEVE_FRIEND1);
                }
                break;
            case 2://群聊
                if(!R_GROUP.exists(tagId)){
                    Out.debug("该群不存在"+tagId);
                    return error(ErrorCodeList.GROUPID_IS_NULL);//群聊不存在
                }
                break;
        }


        //消息内容类型
        String msgTypey = params.getString("msgType");
        if (StringUtil.isEmpty(msgTypey)) {
            return error(ErrorCodeList.CONTENTTYPEY_IS_NULL);
        }
        int msgType = Integer.parseInt(msgTypey);


        int platformType = params.getInteger("platformType");//1.移动端  2.PC端


        //TODO 这里做判断
        //1.群是否禁言
        //2.个人是否禁言

        JSONObject msg = new JSONObject();
        msg.put("srcId", userId);
        //获取目标用户id
        msg.put("tagId", tagId);
        //获取消息内容
        msg.put("msgContent", msgContent);
        //内容类型 1.文字 2.图片  等等...
        msg.put("msgType", msgType);
        //获取消息类型1.单发  2.群发
        msg.put("tagType", tagTyp);
        msg.put("fromtype", fromtype);
        msg.put("fromcontent", fromcontent);
        //消息时间
        String time = DateUtil.getDateTime();
        msg.put("time", time);

        //获取消息id
        String msgId = IdService.newMsgId();

        //添加Redis的消息库中
        R_MESSAGES.set(msgId, msg.toJSONString());
        return success(msgId);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    public String getPath() {
        return "/Talk/PushMessage";
    }



}



