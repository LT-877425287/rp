package com.sencorsta.ids.talk.http;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 通过userId得到用户详情
* @author TAO
* @date 2019/7/23 9:53
*/
public class GetUserInfo extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("--------------获取用户详情--GetUserInfo----------",userId);
        //得到userId
        String userId=params.getString("userId");

        //Out.info("userId==>",userId);
        //非空
        if (StringUtil.isEmpty(userId)){
            return error(ErrorCodeList.USERID_IS_NULL);
        }
        //返回对应的用户信息
        JSONObject data=new JSONObject();
        data.put("phone",R_USER.hget(userId,"phone"));
        data.put("userId",R_USER.hget(userId,"userId"));
        data.put("nickname",R_USER.hget(userId,"nickname"));
        data.put("portrait",R_USER.hget(userId,"portrait"));

        data.put("sex",Integer.valueOf(R_USER.hget(userId,"sex")));
        data.put("signature",R_USER.hget(userId,"signature"));

        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Talk/GetUserInfo";
    }
}
