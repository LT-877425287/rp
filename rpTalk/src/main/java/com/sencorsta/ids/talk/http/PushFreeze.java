package com.sencorsta.ids.talk.http;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.PushKickOffMessage;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.MsgService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 冻结推送
* @author TAO
* @date 2020/1/5 17:30
*/
public class PushFreeze extends HttpHandler {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("--------------------冻结推送--------PushFreeze-----------------------");
        String userId=params.getString("userId");
        PushKickOffMessage.PushKickOff.Builder push=PushKickOffMessage.PushKickOff.newBuilder();
        push.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.FREEZE_KICKOFF));
        MsgService.pushAndSaved(PushAndSaved.pushProto(userId,"push.KickOff",push));
        return success();
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Talk/PushFreeze";
    }
}
