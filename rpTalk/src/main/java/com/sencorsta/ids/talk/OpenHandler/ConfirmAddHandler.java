package com.sencorsta.ids.talk.OpenHandler;

import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.IdService;
import com.sencorsta.ids.common.service.MsgService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.talk.proto.ImtalkConfirmAddByProto;

import com.sencorsta.ids.talk.proto.ImtalkPushMessageByProto;
import com.sencorsta.utils.date.DateUtil;
import io.netty.channel.Channel;

/**
* @description: 确认添加
* @author TAO
* @date 2019/6/19 11:21
*/
@ClientEvent("ConfirmAdd")
public class ConfirmAddHandler extends OpenMessageHandler implements RedisService {

    @Override
    public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {
        Out.debug("-------------------------确认添加ConfirmAdd--------------------------------");
        //创建确认添加好友的Proto的res返回前端的对象
        ImtalkConfirmAddByProto.ConfirmAddRes.Builder res=ImtalkConfirmAddByProto.ConfirmAddRes.newBuilder();

        //服务器推送的消息
        ImtalkPushMessageByProto.PushMsg.Builder pushsys=ImtalkPushMessageByProto.PushMsg.newBuilder();

        try{
            //创建确认添加好友的Proto的req接收前端数据的对象
            ImtalkConfirmAddByProto.ConfirmAddReq req=ImtalkConfirmAddByProto.ConfirmAddReq.parseFrom(protobuf);
            String myId=userId;//我的id
            String oppositeId=req.getOppositeId();//对方的id
            res.setSign(req.getSign());//返回前端标识信息

            //创建本地信息对象
            JSONObject nativeInfo=new JSONObject();

            nativeInfo.put("blacklist",0);//设置黑名单
            nativeInfo.put("stick",0);//设置置顶

            //R_FRIEND库中互舔

            //发起方的好友列表
            nativeInfo.put("remark",R_USER.hget(oppositeId,NICKNAME));//设置好友列表的备注
            R_FRIEND.hset(myId,oppositeId, String.valueOf(nativeInfo));

            //接收方的好友列表
            nativeInfo.put("remark",R_USER.hget(myId,NICKNAME));//从新设置对方的好友列表的备注
            R_FRIEND.hset(oppositeId,myId,String.valueOf(nativeInfo));

            //TODO 这里要把离线的当前消息清空

            //定义系统消息
            JSONObject msg=new JSONObject();
            msg.put("srcId","0");
            //获取目标用户id
            msg.put("tagId",myId);
            //获取消息内容
            //msg.put("msgContent","欢迎加入群聊，现在可以聊天了");

            msg.put("msgContent","你已添加"+R_USER.hget(oppositeId,"nickname")+"现在可以聊天了");
            //内容类型 1.文字 2.图片  等等...--0.系统消息
            msg.put("msgType",1005);
            //获取消息类型1.单发  2.群发
            msg.put("tagType",1);
            //消息时间
            msg.put("time", DateUtil.getDateTime());
            //添加Redis的消息库中
            String msgId= IdService.newMsgId();
            R_MESSAGES.set(msgId,msg.toJSONString());

            //定义给发起方的消息
            pushsys.setUserId(oppositeId);
            pushsys.setMsgId(msgId);

            //AuroraPush.pushAurora(msgId);//极光推送
            MsgService.pushAndSaved(pushProto(myId,"push.Msg",pushsys));

            //定义发给接收方的消息
            msg.put("srcId","0");
            msg.put("tagId",oppositeId);
            msg.put("msgContent","对方通过了你的好友验证，现在可以聊天了");

            msgId= IdService.newMsgId();
            R_MESSAGES.set(msgId,msg.toJSONString());

            pushsys.setUserId(myId);
            pushsys.setMsgId(msgId);
            //AuroraPush.pushAurora(msgId);//极光推送
            MsgService.pushAndSaved(pushProto(oppositeId,"push.Msg",pushsys));


            //返回对方用户的详情信息
            return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SUCCESS)));
        }catch (InvalidProtocolBufferException e){
            e.printStackTrace();
            return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SERVER_ERROR)));
        }
    }
}
