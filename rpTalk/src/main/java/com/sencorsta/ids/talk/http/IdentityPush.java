package com.sencorsta.ids.talk.http;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.MsgService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.talk.proto.GamePushIdentityByProto;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 改变代理身份推送
* @author TAO
* @date 2019/12/27 19:23
*/
public class IdentityPush extends HttpHandler implements RedisService {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-------------------改变代理身份推送-----IdentityPush-----------------");
        String userId=params.getString("userId");
        int type=params.getInteger("type");
        GamePushIdentityByProto.PushIdentity.Builder push=GamePushIdentityByProto.PushIdentity.newBuilder();
        push.setUserId(userId);//这个是谁发的的那个人.
        push.setType(type);
        MsgService.pushAndSaved(PushAndSaved.pushProto(userId,"push.Identity",push));
        return success();
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Talk/IdentityPush";
    }
}
