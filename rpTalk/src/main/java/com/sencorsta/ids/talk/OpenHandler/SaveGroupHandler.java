package com.sencorsta.ids.talk.OpenHandler;

import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.talk.proto.ImtalkSaveGroupByProto;
import io.netty.channel.Channel;

import java.util.List;

/**
* @description: 保存群聊
* @author TAO
* @date 2019/6/20 14:24
*/

@ClientEvent("SaveGroup")
public class SaveGroupHandler extends OpenMessageHandler implements RedisService {

    @Override
    public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {
        Out.debug("-----------------------------保存群聊SaveGroup-------------------------------");
        ImtalkSaveGroupByProto.SaveGroupRes.Builder res=ImtalkSaveGroupByProto.SaveGroupRes.newBuilder();
        try{
            ImtalkSaveGroupByProto.SaveGroupReq req=ImtalkSaveGroupByProto.SaveGroupReq.parseFrom(protobuf);
            res.setSign(req.getSign());//前后端消息标识

            //群聊id
            String groupId=req.getGroupId();

            //判断该群聊是否存在
            if (R_USER_R_GROUP.exists(groupId)){
                Out.debug("该群Id不存在");
                return  error(ErrorCodeList.GROUPID_IS_NULL);
            }

            //设置用户与群的关系
            List<String> r_user_r_group=R_USER_R_GROUP.lrangeAll(userId);

            Out.debug("当前用户"+userId);
            Out.debug("当前用户保存的群"+r_user_r_group);


            if(!r_user_r_group.contains(groupId)){//当前用户的群聊结合中没有这个群，取消保存
                Out.debug("--------------------当前用户没有保存该群----需要保存----------------------");
                //当前用户不在群里
                R_USER_R_GROUP.lpush(userId,groupId);
            }else{
                Out.debug("--------------------当前用户以保存该群----需要解除----------------------");
                R_USER_R_GROUP.lrem(userId,0,groupId);
            }
            res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SUCCESS));
            return responseProto(res);
        }catch (InvalidProtocolBufferException e){
            e.printStackTrace();
            return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SERVER_ERROR)));
        }
    }

}
