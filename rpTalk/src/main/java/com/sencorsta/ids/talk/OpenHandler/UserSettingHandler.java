package com.sencorsta.ids.talk.OpenHandler;

import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.talk.proto.ImtalkUserSettingByProto;
import com.sencorsta.utils.net.HttpRequester;
import com.sencorsta.utils.net.HttpRespons;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.Channel;



/**
* @description: 个人设置接口总和
* @author TAO
* @date 2019/7/4 13:36
*/
@ClientEvent("UserSetting")
public class UserSettingHandler extends OpenMessageHandler implements RedisService {

    @Override
    public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {
        Out.debug("---------------------个人设置接口总和UserSetting-------------------------");
        ImtalkUserSettingByProto.UserSettingRes.Builder res=ImtalkUserSettingByProto.UserSettingRes.newBuilder();
        try{
            ImtalkUserSettingByProto.UserSettingReq req=ImtalkUserSettingByProto.UserSettingReq.parseFrom(protobuf);
            res.setSign(req.getSign());//前后端标识

            //修改类型
            int type=req.getType();

            //得到原始的数据
            String originalData=req.getOriginalData();
            Out.debug("原始的"+originalData);

            //开始向账号中心发起验证
            HttpRequester requester=new HttpRequester();

            HttpRespons respons=new HttpRespons();

            //得到用户要修改的信息
            String upData=req.getData();

            if (StringUtil.isEmpty(upData)){
                Out.debug("----------------修改的数据为空--------------");
                return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.DEFICIENCY)));
            }

            Out.debug("要修改",upData);
            switch(type){
                case 1 ://1.昵称
                    Out.debug("修改昵称");
                    R_USER.hset(userId,NICKNAME,upData);
                    break;
                case 2 ://3.性别
                    Out.debug("性别");
                    R_USER.hset(userId,SEX,upData);
                    break;
                case 3 : //4.头像
                    Out.debug("头像");
                    R_USER.hset(userId,PORTRAIT,upData);
                break;
                case 4 : //4.签名
                    Out.debug("签名");
                    R_USER.hset(userId,SIGNATURE,upData);
                    break;
                /*case 5 : //4.签名
                    Out.debug("修改密码");
                    R_USER.hset(userId,SIGNATURE,upData);
                    break;*/
            }
            res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SUCCESS));
            return responseProto(res);
        }catch (InvalidProtocolBufferException e){
            e.printStackTrace();
            return responseProto(res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SERVER_ERROR)));
        }
    }


}
