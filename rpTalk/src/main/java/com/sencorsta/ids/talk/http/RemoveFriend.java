package com.sencorsta.ids.talk.http;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.Map;

/**
* @description: 删除好友
* @author TAO
* @date 2019/7/15 15:59
*/
public class RemoveFriend extends HttpHandler implements RedisService {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-------------------------删除好友--RemoveFriend--------------------------");

        //得到用户Id
        String userId=params.getString("userId");
        if (StringUtil.isEmpty(userId)){
            return error(ErrorCodeList.USERID_IS_NULL);
        }
        //得到目标的Id
        String tagId=params.getString("tagId");
        if (StringUtil.isEmpty(tagId)){
            return error(ErrorCodeList.TAGID_IS_NULL);
        }

        //获取自己的好友列表、并判断好友列表是否存在tagId
        Map<String,String> friendList=(R_FRIEND.hgetAll(userId));

        if (friendList.containsKey(tagId)){
            //好友列表并删除对方tagId
            Long re=R_FRIEND.hdel(userId,tagId);
            if (re<1){
                return error(ErrorCodeList.DELETE_FRIENDERROR);
            }
        }

        return  success();
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    public String getPath(){
        return "/Talk/RemoveFriend";
    }
}
