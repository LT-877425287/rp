package com.sencorsta.ids.talk.http;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.Map;

/**
* @description: 获取群详情信息/成员列表
* @author TAO
* @date 2019/7/15 12:20
*/
public class GetGroupMember extends HttpHandler implements RedisService {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("----------------------获取群成员列表GetGroupMember--------------------------");

        String groupId=params.getString("groupId");
        if (StringUtil.isEmpty(groupId)){
            return error(ErrorCodeList.GROUPID_IS_NULL);
        }

        //获取群的详情信息
        Map<String, String> groupInfo=R_PROPERTY.hgetAll(groupId);

        //获取群成员
        Map<String, String> memberList= R_GROUP.hgetAll(groupId);

        JSONArray array=new JSONArray();

        memberList.keySet().forEach(key->{
            JSONObject jsonObject2=JSONObject.parseObject(memberList.get(key));
            Out.debug("-----------------key"+key);
            jsonObject2.put("userId",key);
            array.add(jsonObject2);
        });

        //封装给前端的数据
        JSONObject data=new JSONObject();
        data.put("groupInfo",groupInfo);
        data.put("memberList",array);
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    public String getPath(){
        return "/Talk/GetGroupMember";
    }
}
