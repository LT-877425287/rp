package com.sencorsta.ids.talk.http;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description:目标用户拉取消息
* @author TAO
* @date 2019/6/16 20:02
*/
public class PullMessage extends HttpHandler implements RedisService {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("----------------------目标用户拉取消息--PullMessage------------------------");
        String userId=params.getString("userId");
        Out.info("拉消息用户的id==>"+userId);
        String msgId= params.getString("msgId");//消息id

        //1.通过消息id拿到消息详情
        JSONObject msg=JSONObject.parseObject(R_MESSAGES.get(msgId));

        Out.debug("用户拉的消息id"+msgId);
        Out.debug("消息------->"+msg);
        //做判断
        if (msg==null){
            return error(ErrorCodeList.MSG_IS_NULL);
        }
       //TODO 这里少传userId
       if (("1").equals(msg.getString("tagType"))){//单聊就判断接收方是不是自己
           Out.debug("个人消息--------");
           Out.debug("tagId-->"+msg.get("tagId"));
           Out.debug("userId-->"+userId);
           //接收个人消息时
           if(!String.valueOf(msg.get("tagId")).equals(userId)){
               return error(ErrorCodeList.NO_MSG_RECEPTION);//不是该消息的接收者
           }
       }else{//接收群消息时
           //获取群id
           String groupId= String.valueOf(msg.get("tagId"));
           Out.debug("群Id为-->"+groupId);
           Out.debug("userId-->"+userId);
           //在用户-群的关系中判断这个用户中是否有该群
           //Out.debug("----------------------"+R_GROUP.hgetAll(groupId));
           if (!R_GROUP.exists(groupId)){//群不存在 就拉离线消息
               Out.debug("该群不存在"+groupId);
               Out.debug("群不存在推送的消息：看看是不是群解散消息消息-->"+R_MESSAGES.get(msgId));
               return success(JSONObject.parseObject(R_MESSAGES.get(msgId)));
           }else{//群存在
               //通过群id得到群成员--
               //Set<String> memberList = R_GROUP.hkeys(groupId);
               //if (!memberList.contains(userId)){//并且判断是否存在该用户
               //    //Out.debug("该用户不存在");
               //    return error(ErrorCodeList.NO_MSG_RECEPTION);//不是该消息的接收者
               //}
           }
        }
        return success(JSONObject.parseObject(R_MESSAGES.get(msgId)));
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    public String getPath(){
        return "/Talk/PullMessage";
    }

}
