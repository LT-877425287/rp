package com.sencorsta.ids.gateway.http;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.ParamsVerify;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.common.service.Regular;
import com.sencorsta.ids.common.service.ServerKey;
import com.sencorsta.ids.core.application.proxy.ProxyClient;
import com.sencorsta.ids.core.configure.SysConfig;
import com.sencorsta.ids.core.entity.Server;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.net.HttpRequester;
import com.sencorsta.utils.net.HttpRespons;
import com.sencorsta.utils.string.CodeUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.io.IOException;

/**
* @description: 手机登入
* @author TAO
* @date 2019/11/12 16:12
*/
public class LoginPhone extends HttpHandler implements RedisService {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-------------------------手机登陆-------------------------");
        String result= ParamsVerify.params(params);
        if (result!=null){
            return result;
        }

        String phone = params.getString("phone");//手机号码
        String password = params.getString("password");//验证码

        //手机号码验证
        if (!Regular.phone(phone)){
            return error(ErrorCodeList.PHONE_NUMBER_ERROR);
        }

        HttpRequester req=new HttpRequester();
        HttpRespons res=new HttpRespons();

        try {
            //获取公钥加密的数据
            JSONObject args= ServerKey.Verify();//--加密服务器类型
            //公钥有误  加密失败
            if (args==null){
                return error(ErrorCodeList.PUBLICKEY_IS_ERROR);
            }
            args.put("phone",phone);
            args.put("password",password);

            //这里入参的phone  如果账号中心有的话就就不重新生成UUID  只重新生成ACCODE
            res=req.sendPost(SysConfig.getInstance().get("ac.LoginPhone"),args);

        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject jsz=JSONObject.parseObject(res.getContent());
        //如果账号中心出现问题  return  一下代码不执行
        if (!"0".equals(String.valueOf(jsz.get("code")))){
            return res.getContent();
        }else{
            String UUID=JSONObject.parseObject(String.valueOf(jsz.get("data"))).getString("UUID");
            String ACCODE=JSONObject.parseObject(String.valueOf(jsz.get("data"))).getString("ACCODE");
            JSONObject jszMap=JSONObject.parseObject(R_ACCOUNT.hget(ACCOUNT,UUID));
            if (jszMap==null){
                return error(ErrorCodeList.LOGIN_ACC_NOT_FIND);
            }

            R_SYSTEM.set(TOKEN + ACCODE, UUID);
            R_SYSTEM.expire(TOKEN + ACCODE, 60 * 60 * 24 * 30);
            JSONObject json=new JSONObject();
            String userId=jszMap.getString("userId");
            json.put("UUID",UUID);
            json.put("ACCODE",ACCODE);
            json.put("userId",userId);
            json.put("phone",phone);
            json.put("proxyType","proxy");
            try {
                //转码生成token
                String loginData = CodeUtil.encode(json.toJSONString());
                Server bestServer= ProxyClient.getBestServerByType("proxy");
                if (bestServer == null) {
                    return error(ErrorCodeList.SERVER_NOT_AVAILABLE);
                }
                JSONObject data=new JSONObject();
                data.put("userId",userId);
                data.put("token",loginData);
                data.put("host",bestServer.openPublicHost);
                data.put("port",bestServer.openPort);
                return success(data);
            } catch (IOException e) {
                e.printStackTrace();
                return error(ErrorCodeList.SERVER_ERROR);
            }
        }
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Gateway/LoginPhone";
    }
}
