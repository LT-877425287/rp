package com.sencorsta.ids.gateway.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.application.proxy.ProxyClient;
import com.sencorsta.ids.core.entity.Server;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.string.CodeUtil;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.io.IOException;

/**
* @description: Token校验
* @author TAO
* @date 2019/11/13 16:11
*/
public class TokenVerify extends HttpHandler implements RedisService {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.info("----------------------Token校验---------------");
        String token = params.getString("token");

        Out.info(token);
        if(StringUtil.isEmpty(token)){
            return error(ErrorCodeList.TOKEN_IS_NULL);
        }
        //token="Kr49NTU1NTU1NSh4aD63BTXJGk4Y/QxAL8FVRvH5BwQ8Kj54ZEFLoZzkSlrG1dUL1ds+WXdJsrcYRQhOw9IrGuHCGs/BZWcs2wgdaJdYMxKmgW/HbEwRXSw8r/SNxMTp3iKKC9GJwL5tCCDZfQZr7HAJ5LkkL9eX8VQMgrq1/9gobbYT0kAfG5S8BGeVMCAd5ru65kBfUYZdpHAkRrEAAXgGeDGEv3jwdz2fl07CXPInDJZ9dcXONqs6ND/7NTU1";
        try {

            JSONObject jsonToken= JSON.parseObject(CodeUtil.decode(token));
            String UUID=jsonToken.getString("UUID");
            String userId=jsonToken.getString("userId");
            String ACCODE=jsonToken.getString("ACCODE");

            //判断有没有这个人
            if (StringUtil.isEmpty(userId)){
                return error(ErrorCodeList.LOGIN_TOKEN_INVALID);
            }

            //判断token是否存在
            if(!R_SYSTEM.exists(TOKEN+ACCODE)){
                return error(ErrorCodeList.TOKEN_FAILURE);
            }

            String tempData=null;
            try {
                tempData=CodeUtil.decode(ACCODE);
            } catch (IOException e) {
                return error(ErrorCodeList.LOGIN_TOKEN_INVALID);
            }
            JSONObject deData= JSON.parseObject(tempData);

            if (!UUID.equals(deData.getString("UUID"))){
                return error(ErrorCodeList.UUID_IS_ERROR);
            }

            //设置时间
            R_SYSTEM.expire(TOKEN+ACCODE,60*60*24*30);
            Server bestServer= ProxyClient.getBestServerByType("proxy");
            JSONObject data=new JSONObject();
            data.put("userId",userId);
            data.put("token",token);
            data.put("host",bestServer.openPublicHost);
            data.put("port",bestServer.openPort);
            return success(data);
        } catch (IOException e) {
            e.printStackTrace();
            return error(ErrorCodeList.LOGIN_TOKEN_INVALID);
        }


    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Gateway/TokenVerify";
    }
}
