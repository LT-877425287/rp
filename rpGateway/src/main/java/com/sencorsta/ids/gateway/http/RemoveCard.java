package com.sencorsta.ids.gateway.http;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.ParamsVerify;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 移除卡
* @author TAO
* @date 2019/12/22 18:01
*/
public class RemoveCard extends HttpHandler {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-------------------RemoveCard------移除卡-------------------");
        String result= ParamsVerify.params(params);
        if (result!=null){
            return result;
        }
        String userId=params.getString("userId");
        String cardnumber=params.getString("cardnumber");
        String sql="DELETE FROM `card` WHERE `userId`=? AND `cardnumber`=?";
        Object[] args=new Object[]{
                userId,cardnumber
        };
        if (!MysqlService.getInstance().delete(sql,args)){
            return error(ErrorCodeList.UNBUNDLE_IS_ERROR);
        }
        return success();

    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Gateway/RemoveCard";
    }
}
