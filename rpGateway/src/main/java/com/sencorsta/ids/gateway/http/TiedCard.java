package com.sencorsta.ids.gateway.http;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.ParamsVerify;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 绑卡
* @author TAO
* @date 2019/12/22 17:09
*/
public class TiedCard extends HttpHandler {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("------------------绑卡-------TiedCard--------------------");
        String result=ParamsVerify.params(params);
        if (result!=null){
            return result;
        }
        String userId=params.getString("userId");//持卡人姓名
        String cardholder=params.getString("cardholder");//持卡人姓名
        String cardnumber=params.getString("cardnumber");//银行卡号
        String cardname=params.getString("cardname");//银行卡类型名字
        String cardaddress=params.getString("cardaddress");//开户行地址


        {
            String sql="INSERT INTO `card`(`userId`,`cardholder`,`cardnumber`,`cardname`,`cardaddress`) VALUES(?,?,?,?,?)";
            Object[] args=new Object[]{
                    userId,cardholder,cardnumber,cardname,cardaddress
            };
            if (MysqlService.getInstance().insert(sql,args)<0){
                return error(ErrorCodeList.TIED_CARD_IS_ERROR);
            }
        }

        return success();
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Gateway/TiedCard";
    }
}
