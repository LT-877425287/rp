package com.sencorsta.ids.gateway.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.*;
import com.sencorsta.ids.core.configure.SysConfig;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.net.HttpRequester;
import com.sencorsta.utils.net.HttpRespons;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.io.IOException;

/**
* @description: 手机+密码注册
* @author TAO
* @date 2019/11/13 11:07
*/
public class RegisterPhone extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-------------------手机+密码注册------------------");
        String result=ParamsVerify.params(params);
        if (result != null) {
            return result;
        }

        String phone = params.getString("phone");//手机号码
        String code = params.getString("code");//验证码
        String password=params.getString("password");//密码
        String SecondaryPassword=params.getString("SecondaryPassword");//二次密码
        String inviteCode=params.getString("InviteCode");//邀请码


        //手机号非空
        if (StringUtil.isEmpty(phone)) {
            return error(ErrorCodeList.PHONE_IS_NULL);
        }
        //手机号码验证
        if (!Regular.phone(phone)){
            return error(ErrorCodeList.PHONE_NUMBER_ERROR);
        }
        //code非空
        if (StringUtil.isEmpty(code)) {
            return error(ErrorCodeList.CODE_IS_NULL);
        }
        //密码非空
        if (StringUtil.isEmpty(password)) {
            return error(ErrorCodeList.PWD_IS_NULL);
        }
        if (password.length()<6){
            return error(ErrorCodeList.PWD_LENGTH_SHORT);
        }
        //二次确认
        if (!password.equals(SecondaryPassword)){
            return error(ErrorCodeList.REGIST_PSW_NOT_EQUALS);
        }

        HttpRequester req=new HttpRequester();
        HttpRespons res=new HttpRespons();
        try{
            //获取公钥加密的数据
            JSONObject args= ServerKey.Verify();
            //公钥有误  加密失败
            if (args==null){
                return error(ErrorCodeList.PUBLICKEY_IS_ERROR);
            }

            args.put("phone",phone);
            args.put("code",code);
            args.put("password",password);
            args.put("six",SysConfig.getInstance().getBoolean("is.six.enable"));

            res=req.sendPost(SysConfig.getInstance().get("ac.RegisterPhone"),args);
        }catch (IOException e){
            return error(ErrorCodeList.ACC_IS_ERROR);
        }

        JSONObject jsz=JSONObject.parseObject(res.getContent());
        //如果账号中心出现问题  return  一下代码不执行
        if (!"0".equals(String.valueOf(jsz.get("code")))){
            return res.getContent();
        }

        String time=JSONObject.parseObject(String.valueOf(jsz.get("data"))).getString("time");
        String UUID=JSONObject.parseObject(String.valueOf(jsz.get("data"))).getString("UUID");

        String userId=IdService.newUserId();
        JSONObject json=new JSONObject();
        json.put("userId",userId);
        json.put("time",time);
        json.put("phone",phone);
        R_ACCOUNT.hset(ACCOUNT,UUID,json.toJSONString());

        //绑定邀请码
        if(!StringUtil.isEmpty(inviteCode)){
            try{
                //获取公钥加密的数据
                JSONObject args= new JSONObject();
                args.put("agency",userId);
                args.put("invitecode",inviteCode);

                Out.debug("userId==>",userId);
                Out.debug("inviteCode==>",inviteCode);
                Out.debug(SysConfig.getInstance().get("agency"));

                res=req.sendPost(SysConfig.getInstance().get("agency"),args);

                Out.debug("11111111111",JSON.toJSON(res));
            }catch (IOException e){
                return error(ErrorCodeList.INVITECODE_IS_ERROR);
            }

            if (!"0".equals(String.valueOf(jsz.get("code")))){
                return res.getContent();
            }else{
                Out.debug("成功");
            }
        }


        return success();
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Gateway/RegisterPhone";
    }
}


