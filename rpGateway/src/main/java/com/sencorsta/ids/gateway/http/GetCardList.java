package com.sencorsta.ids.gateway.http;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.ParamsVerify;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @author TAO
 * @description: 获取绑定的银行卡
 * @date 2019/12/22 17:48
 */
public class GetCardList extends HttpHandler {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("----------------获取绑定的银行卡---------GetCardList----------------");
        String result= ParamsVerify.params(params);
        if (result!=null){
            return result;
        }
        String userId = params.getString("userId");
        String sql = "select `cardholder`,`cardnumber`,`cardname`,`cardaddress` from `card` where `userId`=?";
        Object[] args = new Object[]{
                userId
        };
        JSONArray jsonArray = MysqlService.getInstance().select(sql,args);
        return success(jsonArray);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Gateway/GetCardList";
    }
}
