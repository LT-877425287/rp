package com.sencorsta.ids.gateway.http;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.IdService;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.date.DateUtil;
import com.sencorsta.utils.string.Md5Tools;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 机器人注册
* @author TAO
* @date 2019/12/5 19:01
*/
public class RobotRegister extends HttpHandler implements RedisService {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        int count=0;
        long it=90000000000L;
        for (int i=0;i<10000;i++){
            String currTime = DateUtil.getDateTime();
            String userId= String.valueOf(it+i);
            R_USER.hset(userId,"userId",userId);
            R_USER.hset(userId,"loginFirstTime",currTime);
            R_USER.hset(userId,"portrait", userId);
            {
                String sql="select `nickName` from `randomNickNmae` limit "+count+++",1";
                JSONArray result= MysqlService.getInstance().select(sql);
                JSONObject jsz=JSONObject.parseObject(String.valueOf(result.get(0)));
                if (jsz.keySet().size()>0){
                    R_USER.hset(userId,"nickname",jsz.getString("nickName"));
                }
            }
            R_USER.hset(userId,"sex","1");
            R_USER.hset(userId,"phone", userId);
            R_USER.hset(userId,"paypwd","0");
            R_USER.hset(userId,"signature","这个人什么都没留下");
            R_USER.hset(userId,"invitecode", userId);
            R_USER.hset(userId,"type", String.valueOf(2));//1.普通用户  2.机器人
            R_USER.hset(userId,"loginLastTime",currTime);
            //初始化钱包信息--开通钱包
            {
                String sql="INSERT INTO `wallet`(`userId`) VALUES(?)";
                Object[] args=new Object[]{
                        userId
                };
                MysqlService.getInstance().insert(sql,args);
            }

            {
                String uuid=IdService.newUUID();
                String sql="INSERT INTO `account`(`UUID`,`phone`,`phone_password`,`sign`)  VALUES(?,?,?,?) ";
                Object[] args=new Object[]{
                        uuid,userId, Md5Tools.MD5(userId),2
                };
                if (MysqlService.getInstance().insert(sql,args)<1){
                    return error(ErrorCodeList.REGISTER_IS_ERROR);
                }
                JSONObject json=new JSONObject();
                json.put("userId",userId);
                json.put("time",currTime);
                json.put("phone",userId);
                R_ACCOUNT.hset(ACCOUNT,uuid,json.toJSONString());
            }

        }

        return "";
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/RobotRegister";
    }
}
