package com.sencorsta.ids.gateway.db;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.IdService;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.MysqlServiceS;
import com.sencorsta.ids.core.log.Out;

/**
 * @author TAO
 * @description: 交易流水
 * @date 2019/11/23 14:10
 */
public class TradeRecord {

    //drawWater：系统抽水
    //amount：实际金额
    //生成交易流水   // 1.发包扣钱  2.结算扣钱  3.加钱（不参与手续费） 4.加钱（参与手续费） 5.撤回
    public static void generateRecord(String userId, String rpId, long amount, long drawWater, int tradeWay, int gameId, int roomId) {
        String sql = "INSERT INTO `trade_record`(`recordId`,`userId`,`rpId`,`amount`,`drawWater`,`tradeWay`,`gameId`,`roomId`) VALUE(?,?,?,?,?,?,?,?)";
        Object[] args = new Object[]{
                IdService.newTraderId(), userId, rpId, amount, drawWater, tradeWay, gameId, roomId
        };
        MysqlService.getInstance().insertAsyn(sql, args);
    }

    //得到今天盈利
    public static long getProfit(String userId) {
        String sql = "select sum(`amount`) amount from `trade_record` where TO_DAYS(`createTime`) = TO_DAYS(NOW()) and `userId`=? ";
        Object[] args = new Object[]{
                userId
        };
        JSONArray result = MysqlServiceS.getInstance().select(sql, args);
        if (result.size() > 0) {
            JSONObject jsz = JSONObject.parseObject(String.valueOf(result.get(0)));
            if (jsz.keySet().size() > 0) {
                return jsz.getLong("amount");
            }
        }
        return 0;
    }


    //得到单游戏的流水情况
    public static JSONArray getTrade(int gameId, String userId, int type) {
        String sql = "";
        switch (type) {
            case 1://今天
                sql = "select `rpId`,`roomId`,`sendAmount`,`sendAmountback`,`robMoney`,`resultMoney`,`drawWater`,`total`,`bonus`,DATE_FORMAT(`createTime`, '%Y-%m-%d %H:%i:%s') createTime from `settlement_result` where TO_DAYS(`createTime`) = TO_DAYS(NOW()) and ";
                break;
            case 2://昨天
                sql = "SELECT `rpId`,`roomId`,`sendAmount`,`sendAmountback`,`robMoney`,`resultMoney`,`drawWater`,`total`,`bonus`,DATE_FORMAT(`createTime`, '%Y-%m-%d %H:%i:%s') createTime from `settlement_result` WHERE TO_DAYS(NOW()) - TO_DAYS( `createTime`)=1 and ";

                break;
            case 3://全部
                sql = "SELECT `rpId`,`roomId`,`sendAmount`,`sendAmountback`,`robMoney`,`resultMoney`,`drawWater`,`total`,`bonus`,DATE_FORMAT(`createTime`, '%Y-%m-%d %H:%i:%s') createTime from `settlement_result` WHERE 1=1 and ";
                break;
        }
        sql += " `userId`=? AND `gameId`=?";

        Object[] args = new Object[]{
                userId, gameId
        };
        return MysqlServiceS.getInstance().select(sql, args);
    }


    public static long getTradeAmount(int gameId, String userId, int type) {
        String sql = "";
        switch (type) {
            case 1://今天
                sql = "select SUM(`total`) amount from `settlement_result` where TO_DAYS(`createTime`) = TO_DAYS(NOW()) and ";
                break;
            case 2://昨天
                sql = "SELECT SUM(`total`) amount from `settlement_result` WHERE TO_DAYS(NOW()) - TO_DAYS( `createTime`)=1 and ";

                break;
            case 3://全部
                sql = "SELECT SUM(`total`) amount from `settlement_result` WHERE 1=1 and ";
                break;
        }
        sql += " `userId`=? AND `gameId`=? ";
        Object[] args = new Object[]{
                userId, gameId
        };
        JSONArray result = MysqlServiceS.getInstance().select(sql, args);
        Out.debug("result==>", result);
        if (result.size() > 0) {
            JSONObject jsz = JSONObject.parseObject(String.valueOf(result.get(0)));
            if (jsz.keySet().size() > 0) {
                return jsz.getLong("amount");
            }
        }
        return 0;
    }


    //得到多有游戏的流水情况
    public static JSONArray getAllTrade(String userId, int type) {
        String sql = "";
        switch (type) {
            case 1://今天
                sql = "select `gameId`,`rpId`,`roomId`,`sendAmount`,`sendAmountback`,`robMoney`,`resultMoney`,`drawWater`,`total`,`bonus`,DATE_FORMAT(`createTime`, '%Y-%m-%d %H:%i:%s') createTime from `settlement_result` where TO_DAYS(`createTime`) = TO_DAYS(NOW()) and ";
                break;
            case 2://昨天
                sql = "select `gameId`,`rpId`,`roomId`,`sendAmount`,`sendAmountback`,`robMoney`,`resultMoney`,`drawWater`,`total`,`bonus`,DATE_FORMAT(`createTime`, '%Y-%m-%d %H:%i:%s') createTime FROM `settlement_result` WHERE TO_DAYS(NOW()) - TO_DAYS( `createTime`)=1 and ";
                break;
            case 3://全部
                sql = "select `gameId`,`rpId`,`roomId`,`sendAmount`,`sendAmountback`,`robMoney`,`resultMoney`,`drawWater`,`total`,`bonus`,DATE_FORMAT(`createTime`, '%Y-%m-%d %H:%i:%s') createTime FROM `settlement_result` WHERE 1=1 and ";
                break;
        }
        sql += " `userId`=? ";

        Object[] args = new Object[]{
                userId
        };
        return MysqlServiceS.getInstance().select(sql, args);
    }


    public static long getAllAmount( String userId, int type) {
        String sql = "";
        switch (type) {
            case 1://今天
                sql = "select SUM(`total`) amount from `settlement_result` where TO_DAYS(`createTime`) = TO_DAYS(NOW()) and ";
                break;
            case 2://昨天
                sql = "SELECT SUM(`total`) amount FROM `settlement_result` WHERE TO_DAYS(NOW()) - TO_DAYS( `createTime`)=1 and ";

                break;
            case 3://全部
                sql = "SELECT SUM(`total`) amount FROM `settlement_result` WHERE 1=1 and ";
                break;
        }
        sql += " `userId`=? ";
        Object[] args = new Object[]{
                userId
        };
        JSONArray result = MysqlServiceS.getInstance().select(sql, args);
        Out.debug("result==>", result);
        if (result.size() > 0) {
            JSONObject jsz = JSONObject.parseObject(String.valueOf(result.get(0)));
            if (jsz.keySet().size() > 0) {
                return jsz.getLong("amount");
            }
        }
        return 0;
    }

    //得到单游戏的流水情况
    public static JSONArray getBonus(String userId, int type) {
        String sql = "";
        switch (type) {
            case 1://今天
                sql = "select `rpId`,`roomId`,`bonus`,DATE_FORMAT(`createTime`, '%Y-%m-%d %H:%i:%s') createTime from `settlement_result` where TO_DAYS(`createTime`) = TO_DAYS(NOW()) and ";
                break;
            case 2://昨天
                sql = "SELECT `rpId`,`roomId`,`bonus`,DATE_FORMAT(`createTime`, '%Y-%m-%d %H:%i:%s') createTime FROM `settlement_result` WHERE TO_DAYS(NOW()) - TO_DAYS( `createTime`)=1 and ";
                break;
            case 3://全部
                sql = "SELECT `rpId`,`roomId`,`bonus`,DATE_FORMAT(`createTime`, '%Y-%m-%d %H:%i:%s') createTime FROM `settlement_result` WHERE 1=1 and ";
                break;
        }
        sql += " `gameId`=1 AND `bonus`>0 AND `userId`=?";

        Object[] args = new Object[]{
                userId
        };
        return MysqlServiceS.getInstance().select(sql, args);
    }


    public static long getBonusAmount( String userId, int type) {
        String sql = "";
        switch (type) {
            case 1://今天
                sql = "SELECT SUM(`bonus`) amount from `settlement_result` where TO_DAYS(`createTime`) = TO_DAYS(NOW()) and ";
                break;
            case 2://昨天
                sql = "SELECT SUM(`bonus`) amount FROM `settlement_result` WHERE TO_DAYS(NOW()) - TO_DAYS( `createTime`)=1 and ";
                break;
            case 3://全部
                sql = "SELECT SUM(`bonus`) amount FROM `settlement_result` WHERE 1=1 and ";
                break;
        }
        sql += " `gameId`=1 AND `bonus`>0 AND `userId`=? ";
        Object[] args = new Object[]{
                userId
        };
        JSONArray result = MysqlServiceS.getInstance().select(sql, args);
        Out.debug("result==>", result);
        if (result.size() > 0) {
            JSONObject jsz = JSONObject.parseObject(String.valueOf(result.get(0)));
            if (jsz.keySet().size() > 0) {
                return jsz.getLong("amount");
            }
        }
        return 0;
    }

}

