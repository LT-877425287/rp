package com.sencorsta.ids.gateway.http;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.common.service.Regular;
import com.sencorsta.ids.common.service.ServerKey;
import com.sencorsta.ids.core.configure.SysConfig;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.net.HttpRequester;
import com.sencorsta.utils.net.HttpRespons;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.io.IOException;

/**
* @description: 忘记密码
* @author TAO
* @date 2019/12/12 18:20
*/
public class ForgetPassWord extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-------------------忘记密码---------ForgetPassWord-----------------");
        String phone=params.getString("phone");
        String code=params.getString("code");
        String newPasswold=params.getString("newPasswold");
        String SecondaryPassword=params.getString("SecondaryPassword");//二次密码

        //手机号非空
        if (StringUtil.isEmpty(phone)) {
            return error(ErrorCodeList.PHONE_IS_NULL);
        }
        //手机号码验证
        if (!Regular.phone(phone)){
            return error(ErrorCodeList.PHONE_NUMBER_ERROR);
        }
        //code非空
        if (StringUtil.isEmpty(code)) {
            return error(ErrorCodeList.CODE_IS_NULL);
        }
        //密码非空
        if (StringUtil.isEmpty(newPasswold)) {
            return error(ErrorCodeList.PWD_IS_NULL);
        }
        if (newPasswold.length()<6){
            return error(ErrorCodeList.PWD_LENGTH_SHORT);
        }

        //二次确认
        if (!newPasswold.equals(SecondaryPassword)){
            return error(ErrorCodeList.REGIST_PSW_NOT_EQUALS);
        }

        HttpRequester req=new HttpRequester();
        HttpRespons res=new HttpRespons();
        try{
            //获取公钥加密的数据
            JSONObject args= ServerKey.Verify();
            //公钥有误  加密失败
            if (args==null){
                return error(ErrorCodeList.PUBLICKEY_IS_ERROR);
            }

            args.put("phone",phone);
            args.put("code",code);
            args.put("password",newPasswold);

            res=req.sendPost(SysConfig.getInstance().get("ac.ForgetPassWord"),args);
        }catch (IOException e){
            return error(ErrorCodeList.ACC_IS_ERROR);
        }

        JSONObject jsz=JSONObject.parseObject(res.getContent());
        //如果账号中心出现问题  return  一下代码不执行
        if (!"0".equals(String.valueOf(jsz.get("code")))){
            return res.getContent();
        }

        return success();
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Gateway/ForgetPassWord";
    }
}
