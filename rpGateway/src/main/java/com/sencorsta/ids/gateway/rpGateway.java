package com.sencorsta.ids.gateway;

import com.sencorsta.ids.core.application.Application;

public class rpGateway extends Application{

	static {
		instance = new rpGateway();
	}

	public static rpGateway getInstance() {
		return (rpGateway) instance;
	}

	@Override
	protected void onStarted() {
		super.onStarted();
	}
}
