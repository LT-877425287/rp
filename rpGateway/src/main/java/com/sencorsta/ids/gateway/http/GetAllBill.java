package com.sencorsta.ids.gateway.http;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;

import com.sencorsta.ids.gateway.db.TradeRecord;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.ArrayList;
import java.util.List;

/**
* @description: 获取全部账单
* @author TAO
* @date 2019/12/24 15:23
*/
public class GetAllBill extends HttpHandler implements RedisService {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("---------------获取全部账单-----GetAllBill----------------------");
        String userId=params.getString("userId");
        int type=params.getInteger("type");
        JSONObject data=new JSONObject();
        JSONArray jsz=TradeRecord.getAllTrade(userId,type);


        List<JSONObject> result=new ArrayList<>();
        for (var item :jsz){
            JSONObject jsonObject=JSONObject.parseObject(String.valueOf(item));
            String roomId=jsonObject.getString("roomId");
            String rpId=jsonObject.getString("rpId");
            String gameId=jsonObject.getString("gameId");
            String createTime=jsonObject.getString("createTime");

            long total=jsonObject.getLongValue("total");
            JSONObject js = new JSONObject();
            js.put("roomId", roomId);
            js.put("gameId", gameId);
            js.put("rpId", rpId);
            js.put("createTime", createTime);
            js.put("tradeWay", 1);
            js.put("amount", total);
            result.add(js);


//            long sendAmount=jsonObject.getLongValue("sendAmount");
//            long robMoney=jsonObject.getLongValue("robMoney");
//            long sendAmountback=jsonObject.getLongValue("sendAmountback");
//            long resultMoney=jsonObject.getLongValue("resultMoney");
//            long drawWater=jsonObject.getLongValue("drawWater");
//            long bonus=jsonObject.getLongValue("bonus");
//
//
//            if (sendAmount!=0){
//                JSONObject js=new JSONObject();
//                js.put("roomId",roomId);
//                js.put("rpId",rpId);
//                js.put("createTime",createTime);
//                js.put("gameId",gameId);
//                js.put("tradeWay",1);
//                js.put("amount",sendAmount);
//                result.add(js);
//            }
//            if (robMoney!=0){
//                JSONObject js=new JSONObject();
//                js.put("roomId",roomId);
//                js.put("rpId",rpId);
//                js.put("gameId",gameId);
//                js.put("createTime",createTime);
//                js.put("tradeWay",2);
//                js.put("amount",robMoney);
//                result.add(js);
//            }
//            if (sendAmountback!=0){
//                JSONObject js=new JSONObject();
//                js.put("roomId",roomId);
//                js.put("rpId",rpId);
//                js.put("createTime",createTime);
//                js.put("tradeWay",3);
//                js.put("amount",sendAmountback);
//                result.add(js);
//            }
//            if (resultMoney!=0){
//                JSONObject js=new JSONObject();
//                js.put("roomId",roomId);
//                js.put("rpId",rpId);
//                js.put("createTime",createTime);
//                js.put("tradeWay",4);
//                js.put("amount",resultMoney);
//            }
//            if (drawWater!=0){
//                JSONObject js=new JSONObject();
//                js.put("roomId",roomId);
//                js.put("rpId",rpId);
//                js.put("createTime",createTime);
//                js.put("tradeWay",5);
//                js.put("amount",drawWater);
//            }
//            if (bonus!=0){
//                JSONObject js=new JSONObject();
//                js.put("roomId",roomId);
//                js.put("rpId",rpId);
//                js.put("createTime",createTime);
//                js.put("tradeWay",6);
//                js.put("amount",bonus);
//            }
        }
        data.put("result", result);
        data.put("sumAmount",TradeRecord.getAllAmount(userId,type));
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Gateway/GetAllBill";
    }
}
