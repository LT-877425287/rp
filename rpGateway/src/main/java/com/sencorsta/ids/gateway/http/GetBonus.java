package com.sencorsta.ids.gateway.http;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.gateway.db.TradeRecord;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.ArrayList;
import java.util.List;

/**
* @description: 获取奖金
* @author TAO
* @date 2019/12/24 15:23
*/
public class GetBonus extends HttpHandler implements RedisService {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("---------------获取奖金-----GetBonus----------------------");
        String userId=params.getString("userId");
        int type=params.getInteger("type");
        Out.error(userId);
        Out.error(type);
        JSONObject data=new JSONObject();

        JSONArray jsz=TradeRecord.getBonus(userId,type);

        List<JSONObject> result=new ArrayList<>();
        for (var item :jsz) {
            JSONObject jsonObject = JSONObject.parseObject(String.valueOf(item));
            String roomId = jsonObject.getString("roomId");
            String rpId = jsonObject.getString("rpId");
            String createTime = jsonObject.getString("createTime");
            long bonus = jsonObject.getLongValue("bonus");

            if (bonus!=0){
                JSONObject js=new JSONObject();
                js.put("roomId",roomId);
                js.put("rpId",rpId);
                js.put("createTime",createTime);
                js.put("tradeWay",1);
                js.put("amount",bonus);
                result.add(js);
            }
        }

        data.put("result",result);
        data.put("sumAmount",TradeRecord.getBonusAmount(userId,type));
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Gateway/GetBonus";
    }
}
