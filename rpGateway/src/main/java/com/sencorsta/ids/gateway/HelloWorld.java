package com.sencorsta.ids.gateway;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

public class HelloWorld extends HttpHandler implements RedisService {

	@Override
	public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
		return "HelloWorld.....current-->rpGateway...httpServer---->OK";

	}

	@Override
	public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
		return doPost(ctx,request,params);
	}

	public String getPath() {
		return "/HelloWorld";
	}

}
