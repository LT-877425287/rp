package com.sencorsta.ids.gateway.http;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import static com.sencorsta.ids.common.service.RedisService.R_SYSTEM;
import static com.sencorsta.ids.common.service.RedisService.SERVERSTATUS;

/**
 * @author TAO
 * @description: 获取服务器状态
 * @date 2019/11/12 15:09
 */
public class IsStart extends HttpHandler {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-----------------获取服务器状态-------------------------");
        String result=R_SYSTEM.get(SERVERSTATUS);
        return success(JSONObject.parseObject(result));
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    public String getPath() {
        return "/Gateway/IsStart";
    }
}
