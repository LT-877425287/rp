package com.sencorsta.ids.account;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: HelloWorld
* @author TAO
* @date 2019/7/30 13:51
*/
public class HelloWorld extends HttpHandler {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return "HelloWorld.....current-->rpAccount...httpServer---->OK";
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }


    @Override
    public String getPath() {
        return "/HelloWorld";
    }


}
