package com.sencorsta.ids.account.http.common;

/**
* @description: 手机号码验证
* @author TAO
* @date 2019/7/18 10:14
*/
public class Regular {

    public static boolean phone(String phone) {
        if (phone.length() != 11) {
            return false;
        }
        return true;
    }

}
