package com.sencorsta.ids.account.http.phone;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.account.http.common.EmbedServerVerify;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.common.service.Regular;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.date.DateUtil;
import com.sencorsta.utils.string.CodeUtil;
import com.sencorsta.utils.string.Md5Tools;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.io.IOException;


/**
 * @author TAO
 * @description: 手机+密码登入
 * @date 2019/11/13 13:00
 */
public class LoginPhone extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("---------------手机+密码登入------------------");

        String phone = params.getString("phone");
        String password = params.getString("password");

        //手机号非空
        if (StringUtil.isEmpty(phone)) {
            return error(ErrorCodeList.PHONE_IS_NULL);
        }
        //手机号码验证
        if (!Regular.phone(phone)) {
            return error(ErrorCodeList.PHONE_NUMBER_ERROR);
        }

        //密码非空
        if (StringUtil.isEmpty(password)) {
            return error(ErrorCodeList.PWD_IS_NULL);
        }
        if (password.length() < 6) {
            return error(ErrorCodeList.PWD_LENGTH_SHORT);
        }

        JSONObject resultData;
        {
            String sql = "SELECT `UUID`,`phone_password`,`status` FROM `account` WHERE `phone`=?";
            Object[] arge = new Object[]{
                    phone
            };
            JSONArray result = MysqlService.getInstance().select(sql, arge);

            if (result.size() == 0) {
                return error(ErrorCodeList.LOGIN_ACC_NOT_FIND);
            }
            resultData = JSONObject.parseObject(String.valueOf(result.get(0)));
            if (resultData.getInteger("status")==2) {
                return error(ErrorCodeList.ACCOUNT_FREEZE);
            }
            if (!Md5Tools.MD5(password).equals(resultData.getString("phone_password"))) {
                return error(ErrorCodeList.LOGIN_PWD_INCORRECT);
            }
        }

        {//登入成功更新登入时间
            String sql = "update `account` set `loginTime`=now() where `phone`=? ";
            Object[] arge = new Object[]{
                    phone
            };
            MysqlService.getInstance().updateAsyn(sql, arge);
        }

        String UUID=resultData.getString("UUID");
        JSONObject json = new JSONObject();
        json.put("UUID", UUID);
        json.put("time", DateUtil.getDateTime());
        try {
            String ACCODE = CodeUtil.encode(json.toJSONString());
            JSONObject res = new JSONObject();
            res.put("ACCODE", ACCODE);
            res.put("UUID", UUID);

            return success(res);
        } catch (IOException e) {
            e.printStackTrace();
            return error(ErrorCodeList.SERVER_ERROR);
        }
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public Boolean getValidate(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return EmbedServerVerify.Verify(params);
    }


    @Override
    public String getPath() {
        return "/LoginPhone";
    }
}
