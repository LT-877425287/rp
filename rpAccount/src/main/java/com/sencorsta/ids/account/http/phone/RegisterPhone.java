package com.sencorsta.ids.account.http.phone;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.account.http.common.EmbedServerVerify;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.IdService;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.common.service.Regular;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.date.DateUtil;
import com.sencorsta.utils.string.Md5Tools;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.io.IOException;

/**
* @description: 手机+密码注册
* @author TAO
* @date 2019/11/13 10:22
*/
public class RegisterPhone extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-----------------手机+密码注册----------------");
        String phone = params.getString("phone");//手机号码
        String code = params.getString("code");//验证码
        boolean six = params.getBoolean("six");//666666
        String password=params.getString("password");//密码

        //手机号非空
        if (StringUtil.isEmpty(phone)) {
            return error(ErrorCodeList.PHONE_IS_NULL);
        }
        //手机号码验证
        if (!Regular.phone(phone)){
            return error(ErrorCodeList.PHONE_NUMBER_ERROR);
        }
        //code非空
        if (StringUtil.isEmpty(code)) {
            return error(ErrorCodeList.CODE_IS_NULL);
        }
        //密码非空
        if (StringUtil.isEmpty(password)) {
            return error(ErrorCodeList.PWD_IS_NULL);
        }
        if (password.length()<6){
            return error(ErrorCodeList.PWD_LENGTH_SHORT);
        }
        Out.debug("six==>",six);
        if (!six){//开启666666
            //取出REDIS中的code
            String r_code = R_SYSTEM.hget("code:" + phone, "code");
            Out.debug(phone,"==>redis的code：" , r_code);
            //判断code有效
            if (StringUtil.isEmpty(r_code)) {
                return error(ErrorCodeList.CODE_PAST);//redis中没了就代表过期
            }

            //验证码有误  输入的!=REDIS的
            if (!r_code.equals(code)) {
                return error(ErrorCodeList.CODE_ERROR);
            }
        }

        //判断手机号码是否注册过
        {
            String sql="SELECT id FROM `account` WHERE `phone`="+phone;
            JSONArray result=MysqlService.getInstance().select(sql);
            if (result.size()>0){
                return error(ErrorCodeList.REGIST_NUMBER_REGISTED);
            }
        }

        String UUID= IdService.newUUID();
        {
            String sql="INSERT INTO `account`(`UUID`,`phone`,`phone_password`)  VALUES(?,?,?) ";
            Object[] args=new Object[]{
                    UUID,phone, Md5Tools.MD5(password)
            };
            if (MysqlService.getInstance().insert(sql,args)<1){
                return error(ErrorCodeList.REGISTER_IS_ERROR);
            }
        }

        JSONObject data=new JSONObject();
        data.put("UUID",UUID);
        data.put("time",DateUtil.getDateTime());
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }
    @Override
    public Boolean getValidate(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return EmbedServerVerify.Verify(params);

    }
    @Override
    public String getPath() {
        return "/RegisterPhone";
    }
}
