package com.sencorsta.ids.account.http.phone;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.account.http.common.EmbedServerVerify;
import com.sencorsta.ids.account.http.common.SmsService;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.common.service.Regular;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.date.DateUtil;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @description: 手机验证码
 * @author TAO
 * @date 2019/6/25 15:32
 */
public class GetCode extends HttpHandler implements RedisService {

	@Override
	public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
		Out.debug("-----------------获取手机验证码-------------------------");
		//获取手机号码
		String phone = params.getString("phone");
		//手机号非空
		if (StringUtil.isEmpty(phone)){
			return error(ErrorCodeList.PHONE_IS_NULL);
		}
		//手机号码验证
		if (!Regular.phone(phone)){
			return error(ErrorCodeList.PHONE_NUMBER_ERROR);
		}
		Out.debug(R_SYSTEM.hget("code:"+phone,"code")+"---------------------------");

		//限制号码重复1分钟内提交
		if (!StringUtil.isEmpty(R_SYSTEM.hget("code:"+phone,"code"))){//获取手机号码到redis中查是否60秒还在
			return error(ErrorCodeList.CODE_SEND);
		}

		//TODO 限制号码一天内提交5次
		JSONObject data=new JSONObject();
		data.put("phone",phone);
		data.put("time", DateUtil.getDateTime());
		//每一天第一条设置初始值5
		if (!R_SYSTEM.hexists(CODE_NUMBER,phone)){
			Out.debug("第一次登开始像redis中存数据库");
			data.put("count",4);
		}else {//如果code存在就将count-1

			Out.debug("这个那个账号存在了开始修改发送验证码的机会");
			//先取出次数
			JSONObject jszdata=JSON.parseObject(R_SYSTEM.hget(CODE_NUMBER,phone));

			if((int)jszdata.get("count")==0){
				Out.debug("code机会已用完");
				return error(ErrorCodeList.CODE_OFTEN);
			}

			Out.debug("取出的数据"+data);

			//读取当前剩余次数
			int count=(int)jszdata.get("count")-1;
			//从新存储到redis中
			data.put("count",count);

		}

		Out.debug("最后存到redis中的数据"+data);

		R_SYSTEM.hset(CODE_NUMBER,phone,String.valueOf(data));
		//得到code
		JSONObject resCode= SmsService.sendSMS(phone);
		if (resCode.getInteger("code")==0){
			String code=resCode.getString("smsCode");
			Out.debug("phone:"+phone," smsCode:"+code);
			//存入redis key：number value code
			R_SYSTEM.hset("code:"+phone,"code",code);
			R_SYSTEM.expire("code:"+phone,60);
			return success(code);
		}else {
			return error(ErrorCodeList.SERVER_CODE);
		}
	}

	@Override
	public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
		return doPost(ctx,request,params);
	}

	@Override
	public Boolean getValidate(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
		return EmbedServerVerify.Verify(params);
	}

	public String getPath() {
		return "/phone/GetCode";
	}

}
