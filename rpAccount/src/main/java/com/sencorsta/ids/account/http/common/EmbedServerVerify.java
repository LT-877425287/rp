package com.sencorsta.ids.account.http.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.ParamsVerify;
import com.sencorsta.ids.core.configure.SysConfig;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.utils.string.RSAEncrypt;

import java.security.NoSuchAlgorithmException;

/**
* @description: 内嵌接口服务器身份验证验证
 * 就是在账号中心接口的getValidate方法里调用--------被动的服务器身份验证
* @author TAO
* @date 2019/7/30 13:18
*/
public class EmbedServerVerify {

    public static Boolean Verify(JSONObject params) {
        Out.debug("--------------------内嵌接口服务器身份验证验证---------------------");
        //所有入参非空验证
        String result= ParamsVerify.params(params);
        if (result!=null){
            return false;
        }
        //公钥加密的数据
        String data=params.getString("data");

        //得到发送的数据
        String serverType=params.getString("type");

        Out.debug("账号中心接收到的加密数据------"+data);
        Out.debug("账号中心接收到的服务器类型------"+serverType);

        try {
            //获取私钥---用于解密
            final String privateKey= SysConfig.getInstance().get("ac.privateKey");

            //用私钥将data解密
            String decode = RSAEncrypt.decrypt(data, privateKey);//给自己--私钥

            if (!decode.equals(serverType)){
                return false;//公钥有误
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;//公钥有误
        } catch (Exception e) {
            e.printStackTrace();
            return false;//公钥有误
        }
        return true;
    }

}
