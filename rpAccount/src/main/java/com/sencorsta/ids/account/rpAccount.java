package com.sencorsta.ids.account;

import com.sencorsta.ids.account.http.common.TaskRedisCode;
import com.sencorsta.ids.core.application.Application;
import com.sencorsta.ids.core.log.Out;

public class rpAccount extends Application{

	static {
		instance = new rpAccount();
	}

	public static rpAccount getInstance() {
		return (rpAccount) instance;
	}

	@Override
	protected void onStarted() {
		super.onStarted();
		Out.debug("服务器启动完毕-开始准备任务调度");
		TaskRedisCode.task();
	}
}
