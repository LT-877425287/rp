package com.sencorsta.ids.account.http.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.utils.net.HttpRequester;
import com.sencorsta.utils.net.HttpRespons;


/**
　　* @description: 短息业务
　　* @author TAO
　　* @date 2019/6/17 18:15
　　*/
public class SmsService {


    static String url="https://platform.18sms.com/msg/send";

    public static JSONObject sendSMS(String phone) {
        String code=String.valueOf((int)((Math.random()*9+1)*100000));
        JSONObject json=new JSONObject();
        json.put("user_name","f0572g36");
        json.put("password","ZrZ_Oxs_4vX3Tv4");
        json.put("send_model",1);
        json.put("needstatus",1);
        json.put("product_type",1);
        json.put("phone",phone);
        json.put("sms_detail","您的验证码是："+code);

        HttpRequester req=new HttpRequester();
        HttpRespons res;
        try {
            res=req.sendPost(url,json);
        }catch (Exception e){
            Out.error(e.getMessage());
            e.printStackTrace();

            JSONObject resJson=new JSONObject();
            resJson.put("msg", ErrorCodeList.SERVER_CODE.Msg);
            resJson.put("code", ErrorCodeList.SERVER_CODE.code);
            return resJson;

            //return String.valueOf(ErrorCodeList.SERVER_CODE);
        }
        JSONObject resJson=new JSONObject();
        resJson.put("smsCode",code);
        resJson.put("code",0);
       return resJson;
    }


}
