package com.sencorsta.ids.account.http.common;

import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.function.FunctionSystem;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.utils.date.DateUtil;

import java.util.Date;
import java.util.concurrent.TimeUnit;
/**
　　* @description: redis中的验证码次数0点清空任务
　　* @author TAO
　　* @date 2019/7/4 12:39
　　*/
public class TaskRedisCode implements RedisService {

    public static void task(){
        long initalDelay= DateUtil.getNextDayMillisecond()-new Date().getTime();
        FunctionSystem.addScheduleJob(()->{
            Out.warn("开始任务调度每天凌晨0:0删除redis0号系统库的CODE_NUMBER  注意了！！！");
            Out.warn("开始任务调度每天凌晨0:0删除redis0号系统库的CODE_NUMBER  注意了！！！");
            Out.warn("开始任务调度每天凌晨0:0删除redis0号系统库的CODE_NUMBER  注意了！！！");
            R_SYSTEM.del(CODE_NUMBER);
        },initalDelay,24*60*60*1000, TimeUnit.MILLISECONDS);
    }



}
