package com.sencorsta.ids.common.OpenHandler;


import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.LoginService;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.entity.ErrorMsg;
import com.sencorsta.ids.core.tcp.opensocket.LocalMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import io.netty.channel.Channel;

@ClientEvent("ConnectByJson")
public class ConnectByJsonHandle extends LocalMessageHandler {

	@Override
	public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {
		return response(LoginService.login(json.getString("token"), channel));
	}


}
