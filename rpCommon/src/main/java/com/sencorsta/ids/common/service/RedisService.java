package com.sencorsta.ids.common.service;


import com.sencorsta.ids.core.data.redis.RedisConnect;

/**
 * Reids相关的通用数据服务
 *
 */
public interface RedisService {

	//系统
	RedisConnect R_SYSTEM = new RedisConnect(0);

	//用户
	RedisConnect R_USER = new RedisConnect(1);

	//管理员
	RedisConnect R_ADMIN = new RedisConnect(2);

	//帐户
	RedisConnect R_ACCOUNT = new RedisConnect(3);

	//路由
	RedisConnect R_ROUTE = new RedisConnect(4);

	//消息 key：消息id value{消息内容，用户id，群id}
	RedisConnect R_MESSAGES = new RedisConnect(5);

	//群  list
	RedisConnects R_GROUP=new RedisConnects(6);

	//好友  list
	RedisConnects R_FRIEND=new RedisConnects(7);

	//用户-群关系
	RedisConnects R_REDPACKAGE_INFO=new RedisConnects(8);

	//离线消息
	RedisConnect R_OFF_LINE_MAIL=new RedisConnect(9);

	//群的基本信息
	RedisConnect R_PROPERTY=new RedisConnect(10);

	//用户-群关系
	RedisConnects R_USER_R_GROUP=new RedisConnects(11);

	//房间的基本信息
	RedisConnect R_ROOM_INFO=new RedisConnect(12);




	String SERVERSTATUS="SERVERSTATUS";
	String TOKEN="TOKEN:";

	//userInfo
	String NICKNAME="nickname";
	String PASSWORD="password";
	String SEX="sex";
	String PORTRAIT="portrait";
	String ACCTYPE="accType";
	String SIGNATURE="signature";


	String CODE_NUMBER = "CODE_NUMBER";//手机验证码
	String ACCOUNT="ACCOUNT";//账号
	String PHONE="phone";//手机号


	String FILE="FILE";//聊天资源
	String APP_VERSION="APP_VERSION";//聊天资源

	String MAILLIST="list:";
	String MAILHASH="hash:";


	String WATERPOOL="waterPool";
	String WATERLINE="waterLine";
	String WELFAREAMOUNT="welfareAmount";


	String KILLRATE="KILLRATE";



}
