package com.sencorsta.ids.common.service;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.utils.string.StringUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
* @description: http入参非空验证
* @author TAO
* @date 2019/7/19 14:39
*/
public class ParamsVerify {

    public static String params(JSONObject params, Object... exclude){
        Out.debug("-------------入参非空验证ParamsVerify--------------");
        Set<String> jsz=params.keySet();
        List<Object> list= Arrays.asList(exclude);
        for (String key :jsz){
            if (!list.contains(key)){
                if (StringUtil.isEmpty(params.getString(key))){
                    JSONObject jsonObject=new JSONObject();
                    jsonObject.put("code","-1");
                    jsonObject.put("msg","不好意思"+key+"为空!!!");
                    return String.valueOf(jsonObject);
                }
            }
        }
        return null;
    }


}
