package com.sencorsta.ids.common.information;

import com.sencorsta.ids.core.entity.CodeList;
import com.sencorsta.ids.core.entity.ErrorMsg;

/**
　　* @description: 异常码处理
　　* @author TAO
　　* @date 2019/6/12 17:43
　　*/
public class ErrorCodeList extends CodeList {
	//


	public static final ErrorMsg REQUEST_DOES_NOT_EXIST=new ErrorMsg(1,"请求类型不存在");
	public static final ErrorMsg DEFICIENCY=new ErrorMsg(2,"请提交完整数据");
	public static final ErrorMsg PUBLICKEY_IS_ERROR=new ErrorMsg(3,"publicKey有误");
	public static final ErrorMsg UUID_IS_ERROR=new ErrorMsg(4,"UUID有误");
	public static final ErrorMsg SYSOUT_BUSY=new ErrorMsg(5,"系统繁忙");
	public static final ErrorMsg SERVER_MYSQL_ERROR=new ErrorMsg(6,"数据库异常");
	public static final ErrorMsg SERVER_IO=new ErrorMsg(7,"IO流异常");
	public static final ErrorMsg BOLT_WARNING=new ErrorMsg(8,"断线警告");
	public static final ErrorMsg KICKOFF=new ErrorMsg(9,"离线警告");
	public static final ErrorMsg ZKICKOFF=new ErrorMsg(10,"主动离线");
	public static final ErrorMsg RECONNECTION=new ErrorMsg(11,"二次重连");
	public static final ErrorMsg MASTER_SID_FULL=new ErrorMsg(12,"SID池用完");
	public static final ErrorMsg SERVER_NOT_AVAILABLE =new ErrorMsg(13,"没有可用的服务器");
	public static final ErrorMsg ACC_IS_ERROR=new ErrorMsg(14,"账号中心异常");
	public static final ErrorMsg FREEZE_KICKOFF=new ErrorMsg(15,"冻结下线");




	//http 30001-30100 AC+GATEWAY
	public static final ErrorMsg LOGIN_ACC_NOT_FIND=new ErrorMsg(30001,"没有找到账号");
	public static final ErrorMsg LOGIN_PWD_INCORRECT =new ErrorMsg(30002,"密码错误");
	public static final ErrorMsg REGIST_PSW_NOT_EQUALS =new ErrorMsg(30004,"二次密码输入不一致");
	public static final ErrorMsg SERVER_CODE=new ErrorMsg(30005,"手机验证码异常");
	public static final ErrorMsg REGIST_NUMBER_REGISTED =new ErrorMsg(30006,"此号码已注册过");
	public static final ErrorMsg PWD_IS_NULL =new ErrorMsg(30007,"密码为空");
	public static final ErrorMsg PWD_LENGTH_SHORT =new ErrorMsg(30008,"密码小于6位");
	public static final ErrorMsg PHONE_NUMBER_ERROR =new ErrorMsg(30009,"手机号码有误");
	public static final ErrorMsg PHONE_IS_NULL =new ErrorMsg(30010,"手机号码为空");
	public static final ErrorMsg CODE_IS_NULL =new ErrorMsg(30011,"验证码为空");
	public static final ErrorMsg CODE_PAST =new ErrorMsg(30012,"验证码已过期");
	public static final ErrorMsg CODE_ERROR =new ErrorMsg(30013,"验证码错误");
	public static final ErrorMsg REGISTER_IS_ERROR=new ErrorMsg(30014,"注册失败");
	public static final ErrorMsg WALLETID_INITIALIZE_IS_ERROR = new ErrorMsg(30015, "钱包初始化失败");
	public static final ErrorMsg USERID_IS_NULL =new ErrorMsg(30016,"userId为空");
	public static final ErrorMsg CODE_SEND =new ErrorMsg(30018,"验证码已发送");
	public static final ErrorMsg CODE_OFTEN =new ErrorMsg(30019,"操作频繁");
	public static final ErrorMsg FORGET_IS_ERROR =new ErrorMsg(30020,"密码重置失败");
	public static final ErrorMsg INVITECODE_IS_ERROR =new ErrorMsg(30021,"邀请码错误");
	public static final ErrorMsg PNONE_IS_NULL =new ErrorMsg(30022,"手机号码不存在");
	public static final ErrorMsg OLD_PWA_IS_ERROR =new ErrorMsg(30023,"原始密码错误");
	public static final ErrorMsg UPDATE_PWA_IS_ERROR =new ErrorMsg(30024,"密码修改错误");
	public static final ErrorMsg TIED_CARD_IS_ERROR =new ErrorMsg(30025,"绑卡失败");
	public static final ErrorMsg UNBUNDLE_IS_ERROR =new ErrorMsg(30026,"解绑失败");
	public static final ErrorMsg ACCOUNT_FREEZE =new ErrorMsg(30027,"账号已冻结");




	//http 30101-30200 TALK
	public static final ErrorMsg TAGID_IS_NULL =new ErrorMsg(30101,"目标ID为空");
	public static final ErrorMsg MSG_IS_NULL =new ErrorMsg(30102,"消息为空");
	public static final ErrorMsg TYPE_IS_NULL =new ErrorMsg(30103,"类型为空");
	public static final ErrorMsg GROUPID_IS_NULL = new ErrorMsg(30104, "群聊Id为空");
	public static final ErrorMsg CONTENTTYPEY_IS_NULL =new ErrorMsg(30105,"消息内容类型为空");
	public static final ErrorMsg NO_MSG_RECEPTION=new ErrorMsg(30106,"不是该消息的接收者");
	public static final ErrorMsg I_ADD_IS_ERROR=new ErrorMsg(30107,"无法添加自己");
	public static final ErrorMsg DELETE_FRIENDERROR = new ErrorMsg(30108, "删除好友失败");
	public static final ErrorMsg JURISDICTION_INSUFFICIENT=new ErrorMsg(30109,"权限不足");
	public static final ErrorMsg GROUP_IS_NULL = new ErrorMsg(30120, "群聊不存在");
	public static final ErrorMsg NICKNAME_UPDATE_ERROR=new ErrorMsg(30121,"昵称修改失败");
	public static final ErrorMsg PHONE_IS_EXIST=new ErrorMsg(30122,"账号已存在");
	public static final ErrorMsg PHONE_UPDATE_ERROR=new ErrorMsg(30123,"手机号码修改失败");
	public static final ErrorMsg SEX_UPDATE_ERROR=new ErrorMsg(30124,"性别修改失败");
	public static final ErrorMsg PORTRAIT_UPDATE_ERROR=new ErrorMsg(30125,"头像修改失败");
	public static final ErrorMsg ACC_IS_EXIST=new ErrorMsg(30126,"账号已存在，无法修改");
	public static final ErrorMsg ACC_IS_NO_UPDATE=new ErrorMsg(30127,"账号已经不能修改了");
	public static final ErrorMsg ACC_UPDATE_ERROR=new ErrorMsg(30128,"账号修改失败");
	public static final ErrorMsg PWD_UPDATE_ERROR=new ErrorMsg(30129,"密码修改失败");
	public static final ErrorMsg TD_RELIEVE_FRIEND=new ErrorMsg(30130,"对方已解除好友关系");
	public static final ErrorMsg TD_RELIEVE_FRIEND1=new ErrorMsg(30131,"好友关系已解除");















	//http 30201-30300 RESURCE
	public static final ErrorMsg PICTURE_IS_NULL =new ErrorMsg(30201,"上传资源为空");
	public static final ErrorMsg FILE_IS_NULL =new ErrorMsg(30202,"文件不存在");
	public static final ErrorMsg MD5FILENAME_IS_NULL =new ErrorMsg(30203,"MD5FileName为空");



	//http 30301-30400 GAME
	public static final ErrorMsg  ROOM_IS_NULL= new ErrorMsg(30301, "房间不存在");
	public static final ErrorMsg  GAME_IS_NULL= new ErrorMsg(30302, "游戏不存在");
	public static final ErrorMsg PACKET_IS_NULL = new ErrorMsg(30303, "红包已结束");
	public static final ErrorMsg ROBUSERID_IS_NULL = new ErrorMsg(30304, "抢红包用户ID为空");
	public static final ErrorMsg PLAYER_IS_NO_ROOM = new ErrorMsg(30305, "玩家不再当前房间");
	public static final ErrorMsg WALLET_IS_NULL = new ErrorMsg(30306, "钱包不存在");
	public static final ErrorMsg AMOUNT_NOT_ENOUGH = new ErrorMsg(30307, "余额不够");
	public static final ErrorMsg SEND_PACKAGE_IS_NOT_ROB = new ErrorMsg(30308, "扫雷玩法发包者不能自己抢包");
	public static final ErrorMsg RED_PACKET_IS_GET = new ErrorMsg(30309, "该红包已领取");
	public static final ErrorMsg REDPACKAGE_LIST_ISNULL = new ErrorMsg(30313, "红包LIST为空");
	public static final ErrorMsg REDPACKAGE_IS_END = new ErrorMsg(30314, "红包已结束");
	public static final ErrorMsg TEN_PACKAGE_IS_ERROR = new ErrorMsg(30315, "红包异常，十包连环雷发包数不够");
	public static final ErrorMsg SIX_PACKAGE_IS_ERROR = new ErrorMsg(30316, "红包异常，发包数量不符合规范");
	public static final ErrorMsg RED_REDPACKAGE_IS_WITHDRAW = new ErrorMsg(30317, "当前红包已撤回");
	public static final ErrorMsg RED_PACKAGE_COUNT_ERROR = new ErrorMsg(30318, "红包数量有误");
	public static final ErrorMsg SEND_PACKAGE_IS_NOT_ROB_LONGHU = new ErrorMsg(30319, "龙虎玩法发包者不能自己抢包");
	public static final ErrorMsg SEND_PACKAGE_IS_NOT_ROB_JINQIANG = new ErrorMsg(30320, "禁抢玩法发包者不能自己抢包");
	public static final ErrorMsg SERVICECHARGE_IS_ERROR = new ErrorMsg(30321, "手续费异常");
	public static final ErrorMsg SYSTEMWATERLINE_IS_ERROR = new ErrorMsg(30322, "水线异常");
	public static final ErrorMsg PLAYER_IS_NULL = new ErrorMsg(30323, "玩家不存在");
	public static final ErrorMsg AMOUNT_IS_ERROR = new ErrorMsg(30324, "红包金额不在范围");
	public static final ErrorMsg RUNWATER_NOT_ENOUGH = new ErrorMsg(3032, "今日发包流水不符合要求");
	public static final ErrorMsg ROOM_NO_START = new ErrorMsg(3033, "房间为开启");





	//http 30401-30500 ROBOT
	public static final ErrorMsg NOT_USABLE_SIMULATION = new ErrorMsg(30400, "无可用机器人");
	public static final ErrorMsg USABLE_SIMULATION_INSUFFICIENT = new ErrorMsg(30401, "机器人不够");
	public static final ErrorMsg OUT_SIMULATION_IS_NULL = new ErrorMsg(30402, "当前房间机器人为空，无需离开");
	public static final ErrorMsg MIN_MAX_IS_ERROR = new ErrorMsg(30403, "设置金额MIN-MAX异常");




	//socket 40000
	public static final ErrorMsg LOGIN_TOKEN_INVALID=new ErrorMsg(40001,"TOKEN不正确");
	public static final ErrorMsg TOKEN_INSERT_ERROR=new ErrorMsg(40002,"TOKEN插入失败");
	public static final ErrorMsg TOKEN_FAILURE=new ErrorMsg(40003,"TOKEN过期失效/不存在");
	public static final ErrorMsg TOKEN_IS_NULL=new ErrorMsg(40004,"TOKEN为空");
}
