package com.sencorsta.ids.common.proto.utils;

import com.sencorsta.ids.common.proto.Entity;
import com.sencorsta.ids.core.entity.ErrorMsg;

public class ReturnUtils {
    public static Entity.ReturnMsg ErrorProto(ErrorMsg errorMsg) {
        Entity.ReturnMsg.Builder msg = Entity.ReturnMsg.newBuilder();
        msg.setCode(errorMsg.code);
        msg.setMsg(errorMsg.Msg);
        return msg.build();
    }

    public static Entity.ReturnMsg SuccessProto() {
        Entity.ReturnMsg.Builder msg = Entity.ReturnMsg.newBuilder();
        msg.setCode(0);
        msg.setMsg("Success");
        return msg.build();
    }
}
