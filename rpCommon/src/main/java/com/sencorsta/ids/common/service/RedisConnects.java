package com.sencorsta.ids.common.service;

import com.sencorsta.ids.core.data.redis.RedisConnect;
import com.sencorsta.ids.core.log.Out;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;

import java.util.List;
import java.util.Set;

/**
* @description: redis方法扩充
* @author TAO
* @date 2019/7/15 8:23
*/
public class RedisConnects extends RedisConnect {

    public RedisConnects(int index) {
        this.select(index);
    }

    //list删除指定元素
    public final Long lrem(String key, long count, String value) {
        Jedis redis = this.getRedis();
        Long var5;
        try {
            var5 = redis.lrem(key,count,value);
        } finally {
            this.release(redis);
        }

        return var5;
    }

/*    //zset获取分数区间的值
    public final Set<String> zrange (String key, long start, long stop) {
        Jedis redis = this.getRedis();
        Set<String> var5;
        try {
            var5=redis.zrange(key,start,stop);

        } finally {
            this.release(redis);
        }

        return var5;
    }


    //zset获取指定分数的值
    public final Set<String> zrangezd(String key, long score) {
        long min=score-1;
        long man=score+1;
        Jedis redis = this.getRedis();
        Set<String> var5;
        try {
            var5=redis.zrange(key,min,man);

        } finally {
            this.release(redis);
        }

        return var5;
    }*/





    //zset获取分数区间的值
    public final Set<String> zrangebyscore (String key, long min, long max) {
        Jedis redis = this.getRedis();
        Set<String> var5;
        try {
            var5=redis.zrangeByScore(key,min,max);

        } finally {
            this.release(redis);
        }

        return var5;
    }
    //zset获取指定分数的值
    public final Set<String> zrangebyscorezd(String key, long score) {
        return zrangebyscore(key,score,score);
    }


    //zset删除分数区间的值
    public final long zremrangebyscore (String key, String min, String max) {
        Jedis redis = this.getRedis();
        long var5;
        try {
            var5=redis.zremrangeByScore(key,min,max);
        } finally {
            this.release(redis);
        }

        return var5;////zrange mZySet 0 -1 withscores
    }

    //zset获取指定分数的值
    public final long zremrangebyscorezd(String key, String score) {
        return zremrangebyscore(key,score,score);
    }


    //zset删除分数区间的值
    public final  Set<Tuple> zrangeWithScores (String key, Long min, Long max) {
        Jedis redis = this.getRedis();
        Set<Tuple> var5;
        try {//zrangebyscore
            var5=redis.zrangeWithScores(key,min,max);
        } finally {
            this.release(redis);
        }
        return var5;
    }

    public Set<Tuple> zrangeWithScoresAll(String key) {
        return this.zrangeWithScores(key, 0L, -1L);
    }

}
