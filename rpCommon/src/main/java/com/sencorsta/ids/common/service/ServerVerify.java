package com.sencorsta.ids.common.service;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.configure.SysConfig;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.utils.net.HttpRequester;
import com.sencorsta.utils.net.HttpRespons;
import com.sencorsta.utils.string.RSAEncrypt;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
* @description: 服务器身份验证
* @author TAO
* @date 2019/7/29 20:40
*/
public class ServerVerify {

    public static String Verify() {
        Out.debug("--------------------ServerVerify--------------------");
        HttpRequester req=new HttpRequester();
        HttpRespons res=new HttpRespons();

            //获取公钥
            final String publicKey= SysConfig.getInstance().get("ac.publicKey");
            //获取服务器Type
            String serverType=SysConfig.getInstance().get("server.type");

            //构建错误返回
            JSONObject error = new JSONObject();
            error.put("code", "30031");
            error.put("msg", "publicKey有误");


            //构建http请求入参
            JSONObject jsonObject=new JSONObject();
            try {

                Out.debug("publicKey----------------"+publicKey);
                Out.debug("serverType----------------"+serverType);

                //将serverType用publicKey加密
                String encode = RSAEncrypt.encrypt(serverType, publicKey);//给子系统--公钥

                //封装data
                jsonObject.put("data", URLEncoder.encode(encode, "utf-8") +"".trim());
                jsonObject.put("type",serverType);

                Out.debug("DisparkServerVerify-----------------------"+SysConfig.getInstance().get("ac.DisparkServerVerify"));

                //发送http请求
                res=req.sendPost(SysConfig.getInstance().get("ac.DisparkServerVerify"),jsonObject);

                return res.getContent();
            } catch (UnsupportedEncodingException e) {
                return String.valueOf(error);
            } catch (Exception e) {
                return String.valueOf(error);
            }

    }


}
