package com.sencorsta.ids.common.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.application.proxy.ProxyClient;
import com.sencorsta.ids.core.configure.GlobalConfigure;
import com.sencorsta.ids.core.data.mysql.DBService;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;

public class MysqlService extends DBService {



    private static MysqlService instance=null;
    public static MysqlService getInstance(){
        if (instance==null){
            instance=new MysqlService();
            instance.DATA_BASE_SERVER_NAME="rpDataBase";
            return instance;
        }
        return instance;
    }

}
