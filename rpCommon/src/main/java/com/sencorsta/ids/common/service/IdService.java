package com.sencorsta.ids.common.service;

import com.sencorsta.ids.core.log.Out;
import com.sencorsta.utils.random.SnowflakeIdWorker;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class IdService implements RedisService {

    public static SnowflakeIdWorker idWorker=new SnowflakeIdWorker(0,0);
    public static String newUUID(){
        //生成用户id的
        Long UUID = idWorker.nextId();
        Out.info("生成新的UUID：", UUID.toString());
        return UUID.toString();
    }

    //生成用户Id
    public static String newUserId(){
        //生成用户id的
        Long userId = R_SYSTEM.incr("userId", 1);
        String id = String.valueOf(userId.longValue() + 1000000);
        if (R_USER.exists(id)) {
            return newUserId();
        }
        return id;
    }

    //生成群id的
    public static String newGroupId(){
        Long groupId = R_SYSTEM.incr("groupId", 1);
        String id = String.valueOf(groupId.longValue());
        if (R_GROUP.exists(id)) {
            Out.info("已经有存在的GroupId");
            return newGroupId();
        }
        Out.info("生成新的GroupId：", id);
        return id;
    }

    //生成消息id的
    public static String newMsgId(){
        Long msgId = R_SYSTEM.incr("msgId", 1);
        String id = String.valueOf(msgId.longValue());
        if (R_MESSAGES.exists(id)) {
            return newMsgId();
        }
        return id;
    }

    //生成红包id
    public static String newRpId(){
        Long msgId = R_SYSTEM.incr("rpId", 1);
        String id = String.valueOf(msgId.longValue());
        return id;
    }

    //生成房间Id
    public static String newRoomId(){
        Long msgId = R_SYSTEM.incr("roomId", 1);
        String id = String.valueOf(msgId.longValue());
        if (R_ROOM_INFO.exists(id)) {
            return newRoomId();
        }
        return id;
    }

    //生成交易Id
    public static String newTraderId(){
        //生成新的钱包Id
        Long UUID = new SnowflakeIdWorker(0,0).nextId();
        return UUID.toString();
    }






}
