package com.sencorsta.ids.common.service;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.configure.SysConfig;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.utils.string.RSAEncrypt;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
* @description: 服务器key
* @author TAO
* @date 2019/7/29 20:40
*/
public class ServerKey {

    public static JSONObject Verify() {
        Out.debug("--------------------ServerKey--------------------");

        //获取公钥
        final String publicKey= SysConfig.getInstance().get("ac.publicKey");
        //获取服务器Type
        String serverType=SysConfig.getInstance().get("server.type");

        Out.debug("publicKey----------------"+publicKey);
        Out.debug("serverType----------------"+serverType);

        //构建return的JSONObject对象
        JSONObject jsonObject=new JSONObject();
        try {
            //将serverType用publicKey加密
            String encode = RSAEncrypt.encrypt(serverType, publicKey);//给子系统--公钥

            //封装data
            jsonObject.put("data", URLEncoder.encode(encode, "utf-8") +"".trim());
            jsonObject.put("type",serverType);

        } catch (UnsupportedEncodingException e) {
            return null;
        } catch (Exception e) {
            return null;
        }

        return jsonObject;
    }


}
