package com.sencorsta.ids.common.service;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.application.proxy.ProxyClient;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.utils.string.StringUtil;

import java.util.Map;

public class MsgService implements RedisService {

    private static void push(RpcMessage msg, boolean isSaved, boolean isUnique, String key) {
        String SID=R_ROUTE.hget(msg.userId,"proxy");
        if (!StringUtil.isEmpty(SID)){//玩家在线
            Out.debug("userId======================>" + msg.userId);
            Out.debug("SID======================>" +SID);
            pushBySID(msg,SID);
        }else{//玩家不在线
            if (isSaved) {
                JSONObject mail = new JSONObject();
                mail.put("path", msg.method);
                mail.put("data", msg.data);
                if (isUnique) {
                    if (StringUtil.isEmpty(key) == false) {
                        //唯一存储
                        R_OFF_LINE_MAIL.hset(MAILHASH + msg.userId, key, mail.toJSONString());
                    } else {
                        Out.error("唯一存储失败,key未指定");
                    }
                } else {
                    //列表存储
                    R_OFF_LINE_MAIL.lpush(MAILLIST + msg.userId, mail.toJSONString());
                }

            } else {
                Out.debug("isSaved:", isSaved, " 无需推送");
            }
        }
    }

    //推送 只给在线用户推送
    public static void pushOnlyOnline(RpcMessage msg) {
        push(msg, false, false, null);
    }

    public static void pushBySID(RpcMessage msg,String SID) {
        if (StringUtil.isEmpty(SID)){
            Out.warn("SID不存在！");
            return;
        }
        RpcMessage newMsg=new RpcMessage(msg.header.type);
        newMsg.data=msg.data;
        newMsg.userId=msg.userId;
        newMsg.method=msg.method;
        newMsg.serializeType=msg.serializeType;
        ProxyClient.sendBySID(newMsg,"proxy" , SID);
    }

    //推送 若不在线 放入邮箱 不唯一,允许重复
    public static void pushAndSaved(RpcMessage msg) {
        push(msg, true, false, null);
    }

    //推送 若不在线 放入邮箱 保持唯一
    public static void pushAndSavedUnique(RpcMessage msg, String uniKey) {
        push(msg, true, true, uniKey);
    }

}
