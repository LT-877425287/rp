package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

class GetRun extends Thread {
	public String host;
	public int port;

	public GetRun(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public void run() {
		try {

			String result = TestProxy.sendGet(TestProxy.url, "x=530982", host, port);
			System.out.println("OK --->" + host + ":" + port + "    " + result);
		} catch (Exception e) {
			// e.printStackTrace();
			System.out.println("error --->" + host + ":" + port);
		}
	}
}

public class TestProxy {
//	static String host = "39.137.69.10";
//	static int port = 8080;
	// static String url = "http://93w.club/portal.php";http://823w.xyz/portal.php
	static String url = "http://823w.xyz/portal.php";
	static JSONObject object;

	public static void main(String[] args) throws Exception {
		// four();
		getIP();
		JSONArray arr = object.getJSONArray("data");
		// System.out.println(object.getJSONArray("data").toJSONString());
		for (int i = 0; i < arr.size(); i++) {

			if (i > 50) {
				break;
			}

			JSONObject obj = arr.getJSONObject(i);
			// System.out.println(obj.getString("ip")+":"+obj.getString("port"));
			new GetRun(obj.getString("ip"), obj.getIntValue("port")).start();

			Thread.sleep(new Random().nextInt(3000));
		}
		System.out.println("完毕!!!!!!!!");
	}

	public static String sendGet(String url, String param, String host, int port) throws Exception {

		String result = "";
		String urlName = url + "?" + param;
		URL realURL = new URL(urlName);

		InetSocketAddress addr = new InetSocketAddress(host, port);
		Proxy proxy = new Proxy(Proxy.Type.HTTP, addr); // http 代理
		URLConnection conn = realURL.openConnection(proxy);

		conn.setRequestProperty("accept", "*/*");
		conn.setRequestProperty("connection", "Keep-Alive");
		conn.setRequestProperty("user-agent",
				"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36");
		conn.connect();
		Map<String, List<String>> map = conn.getHeaderFields();
		for (String s : map.keySet()) {
			System.out.println(s + "-->" + map.get(s));
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
		String line;
		while ((line = in.readLine()) != null) {
			result += "\n" + line;
		}
		return result;
	}

	public static String sendPost(String url, String param) {
		String result = "";
		try {
			URL realUrl = new URL(url);
			URLConnection conn = realUrl.openConnection();
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36");
			// post设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			PrintWriter out = new PrintWriter(conn.getOutputStream());
			out.print(param);
			out.flush();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result += "\n" + line;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	static void show(InputStream in) throws IOException {
		Scanner cin = new Scanner(in);
		StringBuilder builder = new StringBuilder();
		while (cin.hasNext()) {
			builder.append(cin.nextLine());
		}
		cin.close();
		Pattern pattern = Pattern.compile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}");
		Matcher matcher = pattern.matcher(builder.toString());
		matcher.find();
		System.out.println(matcher.group());
	}

	static void getIP() {
		try {
			// doctype=xml/json/jsonp
			URL url = new URL(
					"http://api.ip.data5u.com/api/get.shtml?order=9aac0025e813e5a2df352e36ead9b036&num=100&carrier=0&protocol=0&an1=1&an2=2&an3=3&sp1=1&sp2=2&sp3=3&sort=1&system=1&distinct=0&rettype=0&seprator=%0D%0A");
			URLConnection connection = url.openConnection();
			InputStream in = connection.getInputStream();
			InputStreamReader isr = new InputStreamReader(in, "utf-8");
			BufferedReader br = new BufferedReader(isr);
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
			isr.close();
			in.close();
			object = JSONObject.parseObject(sb.toString());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
