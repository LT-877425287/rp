package com.sencorsta.ids.game.openHandler;

import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.IdService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.game.db.RpMassage;
import com.sencorsta.ids.game.proto.GamePushRedPackageByProto;
import com.sencorsta.ids.game.proto.GameSendLongHuByProto;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.LongHuPackage;
import com.sencorsta.ids.game.world.room.LongHuRoom;
import io.netty.channel.Channel;


/**
 * @author TAO
 * @description: 发龙虎玩法的包
 * @date 2019/11/14 19:43
 */
@ClientEvent("SendLongHu")
public class SendLongHu extends OpenMessageHandler implements RedisService {

    @Override
    public RpcMessage request(Channel channel, JSONObject jsonObject, String s, byte[] bytes, byte[] protobuf) {
        Out.debug("执行方法: ---->SendSaoLei--------- 发扫雷玩法的包:");
        GameSendLongHuByProto.SendLongHuRes.Builder res=GameSendLongHuByProto.SendLongHuRes.newBuilder();
        GamePushRedPackageByProto.PushRedPackage.Builder push = GamePushRedPackageByProto.PushRedPackage.newBuilder();

        try {
            GameSendLongHuByProto.SendLongHuReq req=GameSendLongHuByProto.SendLongHuReq.parseFrom(protobuf);

            int gameId = req.getGameId();//得到加入的房间类型
            int roomId =req.getRoomId();//得到加入的房间房间Id
            long sendAmount =req.getSendAmount();//得到金额
            int count =req.getCount();//发包数
            int detain=req.getDetain();//龙虎类型  1龙 2.虎

            //得到龙虎房间
            LongHuRoom longHuRoom = (LongHuRoom) World.getInstance().games.get(gameId).rooms.get(roomId);
            if (longHuRoom == null) {
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.ROOM_IS_NULL));
                return responseProto(res);
            }

            if (longHuRoom.status!=2){
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.ROOM_NO_START));
                return responseProto(res);
            }

            if (longHuRoom.minAmount!=0&&longHuRoom.minAmount!=0) {
                if (sendAmount < longHuRoom.minAmount || sendAmount > longHuRoom.maxAmount) {
                    res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.AMOUNT_IS_ERROR));
                    return responseProto(res);
                }
            }

            //得到在当前房间发包的押金
            long sendDeposit = (long) longHuRoom.computeSendDeposit(count, sendAmount);
            Out.debug("龙虎sendDeposit==>" + sendDeposit);

            Player player=World.getInstance().allPlayers.get(userId);//得到发包玩家

            if (player.amount< sendDeposit) {//余额不足
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.AMOUNT_NOT_ENOUGH));
                return responseProto(res);
            }


            String rpId = IdService.newRpId();//红包Id.
            LongHuPackage longHuPackage = new LongHuPackage(userId, rpId, count, sendAmount, longHuRoom,sendDeposit,detain,player.type==2);//创建一个龙虎玩法的红包
            longHuRoom.rpPackage.put(rpId, longHuPackage);//将龙虎房间的包容器中添加当前包
            longHuPackage.playerMap.put(userId,player);

            //走到这代表包已经发出=======================
            JSONObject recode=new JSONObject();
            recode.put("sendAmount",-sendAmount);
            longHuPackage.moneyRecode.put(userId,recode);

            player.onSend(rpId,-sendDeposit,sendDeposit);

            push.setGameId(gameId);
            push.setRoomId(roomId);
            push.setRpId(rpId);

            //将红包记录插到MySql库里
            RpMassage.insertRpMessage2(rpId,roomId,gameId,1);//--------------------------------------------------------------------
            RpcMessage pushMsg=pushProto("", "push.RedPackage", push);
            longHuRoom.broadcast(pushMsg,"proxy");
            res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SUCCESS));
            return responseProto(res);
        }catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SERVER_ERROR));
            return responseProto(res);
        }
    }


}
