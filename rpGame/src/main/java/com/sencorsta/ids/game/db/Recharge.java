package com.sencorsta.ids.game.db;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.MysqlServiceS;
import com.sencorsta.ids.core.log.Out;

/**
* @description: 充值
* @author TAO
* @date 2019/11/23 22:37
*/
public class Recharge {
    public static long getTodayRecharge(String userId){
        String sql="SELECT SUM(`amount`) amount FROM `recharge` WHERE TO_DAYS(`create_time`) = TO_DAYS(NOW()) AND `status`=2 AND `userId`="+userId;
        JSONArray result= MysqlServiceS.getInstance().select(sql);
        Out.debug("result==>",result);
        if (result.size()>0){
            JSONObject jsz=JSONObject.parseObject(String.valueOf(result.get(0)));
            if (jsz.keySet().size()>0){
                return jsz.getLong("amount");
            }
        }
        return 0;
    }
}
