package com.sencorsta.ids.game.http.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.db.RpMassage;
import com.sencorsta.ids.game.util.GetRedPackageInfo;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.room.*;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.ArrayList;

/**
* @description: 获取当前房间的红包结合每次20条
* @author TAO
* @date 2019/11/22 16:26
*/
public class GetRoomRedPackageList extends HttpHandler {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("--------------------------获取当前房间的红包结合每次20条-------------------------------");
        int gameId=params.getInteger("gameId");
        int roomId=params.getInteger("roomId");
        int count=params.getInteger("count");

        JSONArray result= RpMassage.getRpIdList(gameId,roomId,count);
        if (result==null){//没有一个红包
            return error(ErrorCodeList.REDPACKAGE_LIST_ISNULL);
        }
        Room room=World.getInstance().games.get(gameId).rooms.get(roomId);//得到当前房间

        ArrayList<JSONObject> data=new ArrayList<>();
        for(var item:result){
            JSONObject current= (JSONObject) item;
            switch (gameId){
                case 1:
                    JSONObject jszSaoLei=GetRedPackageInfo.GetSaoLeiRedPackage(current.getString("rpId"),(SaoLeiRoom) room,current.getInteger("status"));
                    if (jszSaoLei.keySet().size()==0) {
                        continue;
                    }
                    data.add(jszSaoLei);
                    break;
                case 2:
                    JSONObject jszNiuNiu=GetRedPackageInfo.GetNiuNiuRedPackage(current.getString("rpId"),room,current.getInteger("status"));
                    if (jszNiuNiu.keySet().size()==0) {
                        continue;
                    }
                    data.add(jszNiuNiu);
                    break;
                case 3:
                    JSONObject jszJinQiang=GetRedPackageInfo.GetJinQiangRedPackage(current.getString("rpId"),(JinQiangRoom) room,current.getInteger("status"));
                    if (jszJinQiang.keySet().size()==0) {
                        continue;
                    }
                    data.add(jszJinQiang);
                    break;
                case 4:
                    JSONObject jszLongHu=GetRedPackageInfo.GetLongHuRedPackage(current.getString("rpId"),(LongHuRoom) room,current.getInteger("status"));
                    if (jszLongHu.keySet().size()==0) {
                        continue;
                    }
                    data.add(jszLongHu);
                    break;
                case 5:
                    JSONObject jszWelfare=GetRedPackageInfo.GetWelfareRedPackage(current.getString("rpId"), room,current.getInteger("status"));
                    if (jszWelfare.keySet().size()==0) {
                        continue;
                    }
                    data.add(jszWelfare);
                    break;
                case 6:
                    JSONObject jszJielong=GetRedPackageInfo.GetJieLongPackage(current.getString("rpId"), room,current.getInteger("status"));
                    if (jszJielong.keySet().size()==0) {
                        continue;
                    }
                    data.add(jszJielong);
                    break;
            }
        }




        Out.info(JSON.toJSON(data));

        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/GetRoomRedPackageList";
    }
}
