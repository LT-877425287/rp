// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: GamePushRoomStatusByProto.proto

package com.sencorsta.ids.game.proto;

public final class GamePushRoomStatusByProto {
  private GamePushRoomStatusByProto() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface PushRoomStatusOrBuilder extends
      // @@protoc_insertion_point(interface_extends:proto.PushRoomStatus)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <pre>
     *游戏id
     * </pre>
     *
     * <code>int32 gameId = 1;</code>
     */
    int getGameId();

    /**
     * <pre>
     *房间Id
     * </pre>
     *
     * <code>int32 roomId = 2;</code>
     */
    int getRoomId();

    /**
     * <pre>
     *房间状态
     * </pre>
     *
     * <code>int32 status = 3;</code>
     */
    int getStatus();
  }
  /**
   * Protobuf type {@code proto.PushRoomStatus}
   */
  public  static final class PushRoomStatus extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:proto.PushRoomStatus)
      PushRoomStatusOrBuilder {
  private static final long serialVersionUID = 0L;
    // Use PushRoomStatus.newBuilder() to construct.
    private PushRoomStatus(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private PushRoomStatus() {
    }

    @Override
    @SuppressWarnings({"unused"})
    protected Object newInstance(
        UnusedPrivateParameter unused) {
      return new PushRoomStatus();
    }

    @Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return this.unknownFields;
    }
    private PushRoomStatus(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      if (extensionRegistry == null) {
        throw new NullPointerException();
      }
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 8: {

              gameId_ = input.readInt32();
              break;
            }
            case 16: {

              roomId_ = input.readInt32();
              break;
            }
            case 24: {

              status_ = input.readInt32();
              break;
            }
            default: {
              if (!parseUnknownField(
                  input, unknownFields, extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return GamePushRoomStatusByProto.internal_static_proto_PushRoomStatus_descriptor;
    }

    @Override
    protected FieldAccessorTable
        internalGetFieldAccessorTable() {
      return GamePushRoomStatusByProto.internal_static_proto_PushRoomStatus_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              PushRoomStatus.class, Builder.class);
    }

    public static final int GAMEID_FIELD_NUMBER = 1;
    private int gameId_;
    /**
     * <pre>
     *游戏id
     * </pre>
     *
     * <code>int32 gameId = 1;</code>
     */
    public int getGameId() {
      return gameId_;
    }

    public static final int ROOMID_FIELD_NUMBER = 2;
    private int roomId_;
    /**
     * <pre>
     *房间Id
     * </pre>
     *
     * <code>int32 roomId = 2;</code>
     */
    public int getRoomId() {
      return roomId_;
    }

    public static final int STATUS_FIELD_NUMBER = 3;
    private int status_;
    /**
     * <pre>
     *房间状态
     * </pre>
     *
     * <code>int32 status = 3;</code>
     */
    public int getStatus() {
      return status_;
    }

    private byte memoizedIsInitialized = -1;
    @Override
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    @Override
    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (gameId_ != 0) {
        output.writeInt32(1, gameId_);
      }
      if (roomId_ != 0) {
        output.writeInt32(2, roomId_);
      }
      if (status_ != 0) {
        output.writeInt32(3, status_);
      }
      unknownFields.writeTo(output);
    }

    @Override
    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (gameId_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(1, gameId_);
      }
      if (roomId_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(2, roomId_);
      }
      if (status_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(3, status_);
      }
      size += unknownFields.getSerializedSize();
      memoizedSize = size;
      return size;
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof PushRoomStatus)) {
        return super.equals(obj);
      }
      PushRoomStatus other = (PushRoomStatus) obj;

      if (getGameId()
          != other.getGameId()) return false;
      if (getRoomId()
          != other.getRoomId()) return false;
      if (getStatus()
          != other.getStatus()) return false;
      if (!unknownFields.equals(other.unknownFields)) return false;
      return true;
    }

    @Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptor().hashCode();
      hash = (37 * hash) + GAMEID_FIELD_NUMBER;
      hash = (53 * hash) + getGameId();
      hash = (37 * hash) + ROOMID_FIELD_NUMBER;
      hash = (53 * hash) + getRoomId();
      hash = (37 * hash) + STATUS_FIELD_NUMBER;
      hash = (53 * hash) + getStatus();
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static PushRoomStatus parseFrom(
        java.nio.ByteBuffer data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static PushRoomStatus parseFrom(
        java.nio.ByteBuffer data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static PushRoomStatus parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static PushRoomStatus parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static PushRoomStatus parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static PushRoomStatus parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static PushRoomStatus parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static PushRoomStatus parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static PushRoomStatus parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static PushRoomStatus parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static PushRoomStatus parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static PushRoomStatus parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    @Override
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(PushRoomStatus prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    @Override
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @Override
    protected Builder newBuilderForType(
        BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code proto.PushRoomStatus}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:proto.PushRoomStatus)
        PushRoomStatusOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return GamePushRoomStatusByProto.internal_static_proto_PushRoomStatus_descriptor;
      }

      @Override
      protected FieldAccessorTable
          internalGetFieldAccessorTable() {
        return GamePushRoomStatusByProto.internal_static_proto_PushRoomStatus_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                PushRoomStatus.class, Builder.class);
      }

      // Construct using com.sencorsta.ids.game.proto.GamePushRoomStatusByProto.PushRoomStatus.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      @Override
      public Builder clear() {
        super.clear();
        gameId_ = 0;

        roomId_ = 0;

        status_ = 0;

        return this;
      }

      @Override
      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return GamePushRoomStatusByProto.internal_static_proto_PushRoomStatus_descriptor;
      }

      @Override
      public PushRoomStatus getDefaultInstanceForType() {
        return PushRoomStatus.getDefaultInstance();
      }

      @Override
      public PushRoomStatus build() {
        PushRoomStatus result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      @Override
      public PushRoomStatus buildPartial() {
        PushRoomStatus result = new PushRoomStatus(this);
        result.gameId_ = gameId_;
        result.roomId_ = roomId_;
        result.status_ = status_;
        onBuilt();
        return result;
      }

      @Override
      public Builder clone() {
        return super.clone();
      }
      @Override
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return super.setField(field, value);
      }
      @Override
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return super.clearField(field);
      }
      @Override
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return super.clearOneof(oneof);
      }
      @Override
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, Object value) {
        return super.setRepeatedField(field, index, value);
      }
      @Override
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return super.addRepeatedField(field, value);
      }
      @Override
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof PushRoomStatus) {
          return mergeFrom((PushRoomStatus)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(PushRoomStatus other) {
        if (other == PushRoomStatus.getDefaultInstance()) return this;
        if (other.getGameId() != 0) {
          setGameId(other.getGameId());
        }
        if (other.getRoomId() != 0) {
          setRoomId(other.getRoomId());
        }
        if (other.getStatus() != 0) {
          setStatus(other.getStatus());
        }
        this.mergeUnknownFields(other.unknownFields);
        onChanged();
        return this;
      }

      @Override
      public final boolean isInitialized() {
        return true;
      }

      @Override
      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        PushRoomStatus parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (PushRoomStatus) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private int gameId_ ;
      /**
       * <pre>
       *游戏id
       * </pre>
       *
       * <code>int32 gameId = 1;</code>
       */
      public int getGameId() {
        return gameId_;
      }
      /**
       * <pre>
       *游戏id
       * </pre>
       *
       * <code>int32 gameId = 1;</code>
       */
      public Builder setGameId(int value) {

        gameId_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *游戏id
       * </pre>
       *
       * <code>int32 gameId = 1;</code>
       */
      public Builder clearGameId() {

        gameId_ = 0;
        onChanged();
        return this;
      }

      private int roomId_ ;
      /**
       * <pre>
       *房间Id
       * </pre>
       *
       * <code>int32 roomId = 2;</code>
       */
      public int getRoomId() {
        return roomId_;
      }
      /**
       * <pre>
       *房间Id
       * </pre>
       *
       * <code>int32 roomId = 2;</code>
       */
      public Builder setRoomId(int value) {

        roomId_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *房间Id
       * </pre>
       *
       * <code>int32 roomId = 2;</code>
       */
      public Builder clearRoomId() {

        roomId_ = 0;
        onChanged();
        return this;
      }

      private int status_ ;
      /**
       * <pre>
       *房间状态
       * </pre>
       *
       * <code>int32 status = 3;</code>
       */
      public int getStatus() {
        return status_;
      }
      /**
       * <pre>
       *房间状态
       * </pre>
       *
       * <code>int32 status = 3;</code>
       */
      public Builder setStatus(int value) {

        status_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       *房间状态
       * </pre>
       *
       * <code>int32 status = 3;</code>
       */
      public Builder clearStatus() {

        status_ = 0;
        onChanged();
        return this;
      }
      @Override
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.setUnknownFields(unknownFields);
      }

      @Override
      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.mergeUnknownFields(unknownFields);
      }


      // @@protoc_insertion_point(builder_scope:proto.PushRoomStatus)
    }

    // @@protoc_insertion_point(class_scope:proto.PushRoomStatus)
    private static final PushRoomStatus DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new PushRoomStatus();
    }

    public static PushRoomStatus getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<PushRoomStatus>
        PARSER = new com.google.protobuf.AbstractParser<PushRoomStatus>() {
      @Override
      public PushRoomStatus parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new PushRoomStatus(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<PushRoomStatus> parser() {
      return PARSER;
    }

    @Override
    public com.google.protobuf.Parser<PushRoomStatus> getParserForType() {
      return PARSER;
    }

    @Override
    public PushRoomStatus getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_proto_PushRoomStatus_descriptor;
  private static final
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_proto_PushRoomStatus_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    String[] descriptorData = {
      "\n\037GamePushRoomStatusByProto.proto\022\005proto" +
      "\032\014Entity.proto\"@\n\016PushRoomStatus\022\016\n\006game" +
      "Id\030\001 \001(\005\022\016\n\006roomId\030\002 \001(\005\022\016\n\006status\030\003 \001(\005" +
      "B\036\n\034com.sencorsta.ids.game.protob\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          com.sencorsta.ids.common.proto.Entity.getDescriptor(),
        });
    internal_static_proto_PushRoomStatus_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_proto_PushRoomStatus_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_proto_PushRoomStatus_descriptor,
        new String[] { "GameId", "RoomId", "Status", });
    com.sencorsta.ids.common.proto.Entity.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
