package com.sencorsta.ids.game.world.room;

import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.RedPackage;
import com.sencorsta.ids.game.world.pak.SaoLeiPackage;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
* @description: 扫雷房间
* @author TAO
* @date 2019/11/19 22:44
*/
public class SaoLeiRoom extends Room{

    public double magnification;//禁抢玩法固定倍率

    public Map<Long ,Long> reward=new ConcurrentHashMap<>();

    public SaoLeiRoom(String icon,int roomId,String roomName,String announcement,String notice,String regulation,String playingMethodDescribe,int type,double magnification,String main,int isSys,long minAmount,long maxAmount,long sMinAmount,long sMaxAmount,int status){
        super(icon,roomId,roomName,announcement,notice,regulation,playingMethodDescribe,main,isSys,minAmount,maxAmount,sMinAmount,sMaxAmount,status);
        this.magnification=magnification;
        this.main= new Player(Integer.parseInt(R_USER.hget(main, "type")),R_USER.hget(main, "nickname"),R_USER.hget(main, "portrait"),main,null);

        Map<Long,Long> reward=new ConcurrentHashMap<>();
        if (type==1){
            Out.debug("-------------------无奖励-------------------");
            reward=new ConcurrentHashMap<>();
        }else{
            Out.debug("-------------------有奖励-------------------");
            //顺子奖励
            reward.put(1234L,1888L);
            reward.put(2345L,2888L);
            reward.put(3456L,3888L);
            reward.put(4567L,5888L);
            reward.put(5678L,6888L);
            reward.put(6789L,7888L);
            reward.put(12345L,66666L);
            reward.put(23456L,88888L);


            //小豹子奖励
            reward.put(111L,588L);
            reward.put(222L,588L);
            reward.put(333L,588L);
            reward.put(444L,588L);
            reward.put(555L,588L);
            reward.put(666L,588L);
            reward.put(777L,588L);
            reward.put(888L,588L);
            reward.put(999L,588L);


            //大豹子奖励
            reward.put(1111L,2888L);
            reward.put(2222L,3888L);
            reward.put(3333L,3888L);
            reward.put(4444L,5888L);
            reward.put(5555L,6888L);
            reward.put(6666L,7888L);
            reward.put(7777L,8888L);
            reward.put(8888L,9999L);
            reward.put(9999L,19999L);
            reward.put(11111L,66666L);
            reward.put(22222L,88888L);

            //特殊奖励
            reward.put(520L,1888L);
            reward.put(1314L,2888L);
            reward.put(2019L,3888L);
        }
        this.reward=reward;
    }

    //得到当前房间的最高奖励
    public long getMaxReward(){
        Map<Long ,Long> reward=this.reward;
        if (reward.keySet().size()!=0){//存在奖励模式
            Collection<Long> c =reward.values();
            Object[] obj = c.toArray();
            Arrays.sort(obj);//排序顺序
            return Long.parseLong(obj[obj.length-1].toString());
        }
        return 0;//没有奖励
    }

    //计算出当前房间发包金额的押金
    public double computeSendDeposit(int count,long amount){
        Out.debug("count==>"+count);
        Out.debug("amount==>"+amount);
        Out.debug("magnification==>"+magnification);
        Out.debug("MaxMagnification==>"+getMaxReward());
        return ((count-1)*amount*magnification+getMaxReward()*(count-1));
    }


    @Override
    public void update() {
        for (RedPackage saoLeiPackage : rpPackage.values()) {
            saoLeiPackage.update();
        }
    }


}
