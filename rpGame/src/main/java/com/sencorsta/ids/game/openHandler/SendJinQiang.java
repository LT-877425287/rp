package com.sencorsta.ids.game.openHandler;

import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.IdService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.game.db.RpMassage;
import com.sencorsta.ids.game.proto.GamePushRedPackageByProto;
import com.sencorsta.ids.game.proto.GameSendJinQiangByProto;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.JinQiangPackage;
import com.sencorsta.ids.game.world.room.JinQiangRoom;
import io.netty.channel.Channel;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author TAO
 * @description: 发禁抢玩法的包
 * @date 2019/11/21 14:37
 */
@ClientEvent("SendJinQiang")
public class SendJinQiang extends OpenMessageHandler implements RedisService {

    @Override
    public RpcMessage request(Channel channel, JSONObject jsonObject, String s, byte[] bytes, byte[] protobuf) {
        Out.debug("执行方法: ---->SendJinQiang--------- 发禁抢玩法的包:");
        GameSendJinQiangByProto.SendJinQiangRes.Builder res = GameSendJinQiangByProto.SendJinQiangRes.newBuilder();

        GamePushRedPackageByProto.PushRedPackage.Builder push = GamePushRedPackageByProto.PushRedPackage.newBuilder();
        try {
            GameSendJinQiangByProto.SendJinQiangReq req=GameSendJinQiangByProto.SendJinQiangReq.parseFrom(protobuf);

            int gameId = req.getGameId();//得到加入的房间类型
            int roomId =req.getRoomId();//得到加入的房间房间Id
            long sendAmount =req.getSendAmount();//得到金额
            int count =req.getCount();//发包数
            List<Integer> thunderNumber =req.getThunderNumberList();//雷号

            thunderNumber = thunderNumber.stream().distinct().collect(Collectors.toList());

            int redType = req.getRedType();//类型

            //发包是否符合规范
            if (!isOk(redType,count,thunderNumber.size())){
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SIX_PACKAGE_IS_ERROR));
                return responseProto(res);
            }

            //得到房间--判断房间是否存在
            JinQiangRoom jinQiangRoom = (JinQiangRoom) World.getInstance().games.get(gameId).rooms.get(roomId);
            if (jinQiangRoom == null) {
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.ROOM_IS_NULL));
                return responseProto(res);
            }

            if (jinQiangRoom.status!=2){
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.ROOM_NO_START));
                return responseProto(res);
            }

            if (jinQiangRoom.minAmount!=0&&jinQiangRoom.minAmount!=0){
                if (sendAmount<jinQiangRoom.minAmount||sendAmount>jinQiangRoom.maxAmount){
                    res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.AMOUNT_IS_ERROR));
                    return responseProto(res);
                }
            }





            int rpType= JinQiangPackage.getType(count,thunderNumber,redType);//得到当前发送的红包类型


            Player player=World.getInstance().allPlayers.get(userId);//得到发包玩家
            if (player.amount< sendAmount) {//余额不足
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.AMOUNT_NOT_ENOUGH));
                return responseProto(res);
            }

            String rpId = IdService.newRpId();//红包Id
            JinQiangPackage pack = new JinQiangPackage(userId, rpId, count, sendAmount, jinQiangRoom, thunderNumber,rpType,player.type==2);//创建一个扫雷玩法的红包
            jinQiangRoom.rpPackage.put(rpId, pack);//将禁抢房间的包容器中添加当前包
            pack.playerMap.put(userId,player);

            //走到这代表包已经发出=======================
            JSONObject recode=new JSONObject();
            recode.put("sendAmount",-sendAmount);
            pack.moneyRecode.put(userId,recode);

            player.onSend(rpId,-sendAmount,pack.sendDeposit);


            push.setGameId(gameId);
            push.setRoomId(roomId);
            push.setRpId(rpId);

            //将红包记录插到MySql库里
            RpMassage.insertRpMessage2(rpId,roomId,gameId,1);//-------------------------------------------------------------------------
            RpcMessage pushMsg=pushProto("", "push.RedPackage", push);
            jinQiangRoom.broadcast(pushMsg,"proxy");
            res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SUCCESS));
            return responseProto(res);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SERVER_ERROR));
            return responseProto(res);
        }
    }


    public boolean isOk(int redType, int count, int size) {
        if (redType == 0) {
            if (count == 5) {
                if (size >= 1 && size <= 5) {
                    return true;
                } else {
                    return false;
                }
            } else if (count == 6) {
                if (size >= 1 && size <= 6) {
                    return true;
                } else {
                    return false;
                }
            } else if (count == 7) {
                if (size >= 1 && size <= 7) {
                    return true;
                } else {
                    return false;
                }
            } else if (count == 9) {
                if (size >= 1 && size <= 7) {
                    return true;
                } else {
                    return false;
                }
            } else if (count == 10) {
                if (size >= 3 && size <= 8) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            if (count != 6) {
                return false;
            } else {
                if (size >= 2 && size <= 5) {
                    return true;
                } else {
                    return false;
                }
            }
        }


    }


}
