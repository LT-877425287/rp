package com.sencorsta.ids.game;

import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.application.Application;
import com.sencorsta.ids.game.util.Init;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.game.*;

public class rpGame extends Application implements RedisService {

    static {
        instance = new rpGame();
    }

    public static rpGame getInstance() {
        return (rpGame) instance;
    }

    //玩家强制离线触发

    @Override
    public void onUserLeave(String userId) {
        World.getInstance().leaveWorld(userId);
    }


    @Override
    public void onStarted() {

        GameSaoLei gameSaoLei = new GameSaoLei();
        World.getInstance().games.put(World.SAOLEI, gameSaoLei);
        GameNiuNiu gameNiuNiu = new GameNiuNiu();
        World.getInstance().games.put(World.NIUNIU, gameNiuNiu);
        GameJinQiang gameJinQiang = new GameJinQiang();
        World.getInstance().games.put(World.JINGQIANG, gameJinQiang);
        GameLonHu gameLonHu = new GameLonHu();
        World.getInstance().games.put(World.LONGHU, gameLonHu);
        GameWelfare gameWelfare = new GameWelfare();
        World.getInstance().games.put(World.WELFARE, gameWelfare);
        GameJieLong gameJieLong = new GameJieLong();
        World.getInstance().games.put(World.JIELONG, gameJieLong);

        if (!R_SYSTEM.exists(WATERLINE)) {
            R_SYSTEM.set(WATERLINE, "0");
        }
        if (!R_SYSTEM.exists(WATERPOOL)) {
            R_SYSTEM.set(WATERPOOL, "0");
        }
        if (!R_SYSTEM.exists(WELFAREAMOUNT)) {
            R_SYSTEM.set(WELFAREAMOUNT, "0");
        }

        MysqlService.getInstance().waitUtilLoad();
        Init.Initialization();
    }


}
