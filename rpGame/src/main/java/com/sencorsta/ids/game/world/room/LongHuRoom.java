package com.sencorsta.ids.game.world.room;

import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.game.world.pak.LongHuPackage;
import com.sencorsta.ids.game.world.pak.RedPackage;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
* @description: 牛牛房间
* @author TAO
* @date 2019/11/19 10:31
*/
public class LongHuRoom extends Room{

    public double magnification;//倍率


    public LongHuRoom(String icon, int roomId, String roomName, String announcement, String notice, String regulation, String playingMethodDescribe, int type,String main,int isSys,long minAmount,long maxAmount,long sMinAmount,long sMaxAmount,int status){
        super(icon,roomId,roomName,announcement,notice,regulation,playingMethodDescribe,main,isSys,minAmount,maxAmount,sMinAmount,sMaxAmount,status);
        switch (type){
            case 1:
                magnification=1;
                break;
            case 2:
                magnification=2;
                break;
            case 3:
                magnification=5;

                break;
        }
    }

    //获取当前房间的倍率
    public double getMaxMagnification(){
        return magnification;
    }

    //计算出当前房间发包金额的押金
    public double computeSendDeposit(int count,long amount){
        Out.debug("count==>"+count);
        Out.debug("amount==>"+amount);
        Out.debug("MaxMagnification==>"+getMaxMagnification());
        return count*amount*getMaxMagnification();
    }

    @Override
    public void update() {
        for (RedPackage longHuPackage : rpPackage.values()) {
            longHuPackage.update();
        }
    }
}
