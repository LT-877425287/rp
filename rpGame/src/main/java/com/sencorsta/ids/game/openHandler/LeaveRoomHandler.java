package com.sencorsta.ids.game.openHandler;


import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.game.proto.GameLeaveRoomByProto;
import com.sencorsta.ids.game.world.bean.World;
import io.netty.channel.Channel;



/**
* @description: 离开房间
* @author TAO
* @date 2019/11/22 14:00
*/

@ClientEvent("LeaveRoom")
public class LeaveRoomHandler extends OpenMessageHandler implements RedisService {

    @Override
    public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {
        Out.debug("---------离开房间--------LeaveRoom------------");

        GameLeaveRoomByProto.LeaveRoomRes.Builder res=GameLeaveRoomByProto.LeaveRoomRes.newBuilder();
            World.getInstance().leaveWorld(userId);
            res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SUCCESS));
            return responseProto(res);

    }
}
