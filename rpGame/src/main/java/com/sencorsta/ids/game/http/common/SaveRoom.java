package com.sencorsta.ids.game.http.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;

import com.sencorsta.ids.game.db.RoomInfo;
import com.sencorsta.ids.game.util.Init;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.room.*;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @author TAO
 * @description: 保存房间
 * @date 2019/11/19 10:45
 */
public class SaveRoom extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("--------------保存房间----------SaveRoom---------------");

        int gameId=params.getInteger("gameId");
        int roomId=params.getInteger("roomId");

        if (World.getInstance().games.containsKey(gameId)){
            if (World.getInstance().games.get(gameId).rooms.containsKey(roomId)){
                Room room=World.getInstance().games.get(gameId).rooms.get(roomId);
                if (room.rpPackage.keySet().size()!=0){
                    return error(54005,"房间红包未结算完，请稍后再试");
                }
            }
        }

        JSONObject result = RoomInfo.getRoomIdInfoByRoomID(roomId);
        if (result==null){
            return error(ErrorCodeList.ROOM_IS_NULL);
        }
        Init.reloadRoom(result);

        return success();
    }



    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/SaveRoom";
    }
}

