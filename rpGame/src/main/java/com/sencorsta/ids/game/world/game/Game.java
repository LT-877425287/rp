package com.sencorsta.ids.game.world.game;

import com.sencorsta.ids.core.application.zone.ZoneService;
import com.sencorsta.ids.game.world.room.Room;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
* @description: 游戏基类
* @author TAO
* @date 2019/11/19 19:24
*/
public abstract class Game extends ZoneService {

    public Map<Integer, Room> rooms = new ConcurrentHashMap<>();


}
