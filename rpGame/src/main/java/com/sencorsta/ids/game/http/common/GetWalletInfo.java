package com.sencorsta.ids.game.http.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.db.Recharge;
import com.sencorsta.ids.game.db.TradeRecord;
import com.sencorsta.ids.game.db.Wallet;
import com.sencorsta.ids.game.db.WithdrawDeposit;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 获取钱包详情
* @author TAO
* @date 2019/11/23 20:23
*/
public class GetWalletInfo extends HttpHandler {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-----------------获取钱包详情-----------------");
        String userId=params.getString("userId");
        Out.trace("userId",userId);
        long amount= Wallet.getAmount(userId,1);
        long recharge= Recharge.getTodayRecharge(userId);
        long withdrawal= WithdrawDeposit.getTodayWithdrawDeposit(userId);
        long runwater= TradeRecord.getRunWater(userId);
        long profit= TradeRecord.getProfit(userId);

        JSONObject data=new JSONObject();
        data.put("amount",amount);
        data.put("recharge",recharge);
        data.put("withdrawal",withdrawal);
        data.put("runwater",runwater);
        data.put("profit",profit);

        Out.info(data);
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/GetWalletInfo";
    }
}
