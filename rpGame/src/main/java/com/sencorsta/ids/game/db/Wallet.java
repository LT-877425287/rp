package com.sencorsta.ids.game.db;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.MysqlServiceS;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.game.world.bean.Player;

import java.util.List;

/**
 * @author TAO
 * @description: 钱包
 * @date 2019/11/20 14:15
 */
public class Wallet implements RedisService {

    //获取余额
    public static long getAmount(String userId, int type) {//1 直接金额  2.算冻结金额
        String sql;
        if (type == 1) {
            sql = "SELECT `amount` FROM `wallet` WHERE `userId`=" + userId;
        } else {
            sql = "SELECT `amount`-`freeze` amount FROM `wallet` WHERE `userId`=" + userId;
        }

        JSONArray jsonArray = MysqlServiceS.getInstance().select(sql);
        if (jsonArray.size() != 0) {
            return JSONObject.parseObject(String.valueOf(jsonArray.get(0))).getLong("amount");
        }
        return 0;
    }

    //获取余额
    public static JSONObject getAmountAndFreeze(String userId) {//1 直接金额  2.算冻结金额
        String sql = "SELECT `amount`,`freeze` FROM `wallet` WHERE `userId`=" + userId;
        JSONArray jsonArray = MysqlServiceS.getInstance().select(sql);
        if (jsonArray.size() != 0) {
            return JSONObject.parseObject(String.valueOf(jsonArray.get(0)));
        }
        return null;
    }





    //扣钱--冻结
    public static void DeductMoney(Player player, long amount, long freeze) {
        if (player.type == 2) {
            R_SYSTEM.reduce(WATERPOOL, amount);
        }
    }

    //加钱--解冻
    public static void addMoney(Player player, long amount,long freeze) {
        if (player.type == 2) {
            R_SYSTEM.incr(WATERPOOL, amount);
        }

        String sql = "UPDATE `wallet` SET `amount`=`amount`+? WHERE `userId`=?";
        Object[] args = new Object[]{
                amount, player.userId
        };
        MysqlService.getInstance().updateAsyn(sql, args);
    }

    public static void addMoneySP(String userId, long amount) {
        String sql = "UPDATE `wallet` SET `amount`=`amount`+? WHERE `userId`=?";
        Object[] args = new Object[]{
                amount, userId
        };
        MysqlService.getInstance().updateAsyn(sql, args);
    }

    public static JSONObject addMoneySql(Player player, long amount,long freeze,int roomId) {
        if (player.type == 2) {
            R_SYSTEM.incr(WATERPOOL+"-"+roomId, amount);
        }

        String sql = "UPDATE `wallet` SET `amount`=`amount`+? WHERE `userId`=?";
        Object[] args = new Object[]{
                amount, player.userId
        };
        JSONObject res=new JSONObject();
        res.put("SQL",sql);
        res.put("args",args);
        return res;
    }

    public static JSONObject addMoneySqlWelfare(Player player, long amount,int roomId) {
        String sql = "UPDATE `wallet` SET `amount`=`amount`+? WHERE `userId`=?";
        Object[] args = new Object[]{
                amount, player.userId
        };
        JSONObject res=new JSONObject();
        res.put("SQL",sql);
        res.put("args",args);
        return res;
    }


}
