package com.sencorsta.ids.game.world.room;

import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.application.zone.Zone;
import com.sencorsta.ids.core.application.zone.ZoneUser;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.LongHuPackage;
import com.sencorsta.ids.game.world.pak.RedPackage;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author TAO
 * @description: 房间基类
 * @date 2019/11/19 10:25
 */
public abstract class Room extends Zone implements RedisService {

    public String icon;//房间图标

    public int roomId;//房间ID

    public int gameId;//游戏ID

    public String roomName;//房间名

    public String announcement;//公告

    public String notice;//须知

    public String regulation;//群规

    public String playingMethodDescribe;//玩法

    public Player main;//群主

    public int isSys;//是否是系统

    public String mainUserId;//房主Id

    public long minAmount;//最小金额

    public long maxAmount;//最大金额

    public Map<String, RedPackage> rpPackage = new ConcurrentHashMap<>();//房间中的红包

    public long sMinAmount;//机器人最小金额

    public long sMaxAmount;//机器人最大金额

    public int status;//房间状态

    public boolean isStop=false;
    public boolean isStoped=false;

    public int sequence=0;

    public Room(String icon, int roomId, String roomName, String announcement, String notice, String regulation, String playingMethodDescribe, String main, int isSys,long minAmount,long maxAmount,long sMinAmount,long sMaxAmount,int status) {
        this.icon = icon;
        this.roomId = roomId;
        this.roomName = roomName;
        this.announcement = announcement;
        this.notice = notice;
        this.regulation = regulation;
        this.playingMethodDescribe = playingMethodDescribe;
        this.mainUserId = main;
        this.isSys = isSys;
        this.minAmount=minAmount;
        this.maxAmount=maxAmount;
        this.sMinAmount=sMinAmount;
        this.sMaxAmount=sMaxAmount;
        this.status=status;

    }

    public void enter(Player player) {
        super.enter(player);
        player.room = this;
    }

    public void leave(Player player) {
        player.room = null;
        super.leave(player);
    }
}
