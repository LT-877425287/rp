package com.sencorsta.ids.game.http.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.world.bean.World;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 改变系统手续费
* @author TAO
* @date 2019/12/16 13:32
*/
public class ChanageServiceCharge extends HttpHandler {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("----------------改变系统手续费---ChanageServiceCharge-------------------");
        double serviceCharge;
        try {
            serviceCharge=params.getDouble("serviceCharge");
        }catch (Exception e){
            return error(ErrorCodeList.SERVICECHARGE_IS_ERROR);
        }

        World.getInstance().serviceCharge=serviceCharge;
        return success();
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/ChanageServiceCharge";
    }
}
