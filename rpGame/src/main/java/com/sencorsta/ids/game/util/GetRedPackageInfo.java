package com.sencorsta.ids.game.util;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.game.world.pak.JinQiangPackage;
import com.sencorsta.ids.game.world.pak.LongHuPackage;
import com.sencorsta.ids.game.world.pak.RedPackage;
import com.sencorsta.ids.game.world.pak.SaoLeiPackage;
import com.sencorsta.ids.game.world.room.*;

import java.util.Map;

/**
 * @author TAO
 * @description: 获取红包详情
 * @date 2019/11/22 17:15
 */
public class GetRedPackageInfo implements RedisService {
//    getState//1 未领取 2 已领取 3红包派发玩  4时间到  5 余额不足 6红包撤回
//    msgType// 1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息  7.龙虎红包  8.龙虎结算
    //获取扫雷红包详情
    public static JSONObject GetSaoLeiRedPackage(String rpId, SaoLeiRoom saoLeiRoom, int status) {
        JSONObject data = new JSONObject();
        Map<String, String> redisPackage = R_REDPACKAGE_INFO.hgetAll(rpId);//Redis中的红包
        Out.debug(redisPackage);
        SaoLeiPackage memoryPackage = (SaoLeiPackage)saoLeiRoom.rpPackage.get(rpId);//内存中的红包

        if (memoryPackage != null) {//内存中不为空
            String sendUserId = memoryPackage.sendUserId;
            String nickname = R_USER.hget(sendUserId, "nickname");//得到发包者昵称
            String portrait = R_USER.hget(sendUserId, "portrait");//得到发包者头像
            String amount = String.valueOf(memoryPackage.amount);
            amount = amount.substring(0, amount.length() - 2) + "." + amount.substring(amount.length() - 2);
            data.put("msgId", memoryPackage.rpId);//红包ID
            data.put("tagType", memoryPackage.sendUserId);//发送者Id
            data.put("msgType", "2");//游戏Id
            data.put("roomId", memoryPackage.roomId);//房间Id
            data.put("name", nickname);//姓名
            data.put("portrait", portrait);//头像
            data.put("content", amount + "-" + memoryPackage.thunderNumber);//"8-7"
            data.put("getState", 1);//1 未领取 2 已领取
            data.put("thunderNumber", memoryPackage.thunderNumber);//雷号

        } else {
            if (redisPackage.keySet().size() != 0) {
                String sendUserId = redisPackage.get("sendUserId");
                String nickname = R_USER.hget(sendUserId, "nickname");//得到发包者昵称
                String portrait = R_USER.hget(sendUserId, "portrait");//得到发包者头像

                String amount = redisPackage.get("amount");
                amount = amount.substring(0, amount.length() - 2) + "." + amount.substring(amount.length() - 2);

                int mediumMineNumber = Integer.parseInt(redisPackage.get("mediumMineNumber"));//中雷
                data.put("thunderNumber", redisPackage.get("thunderNumber"));//雷号

                Out.debug("sendUserId", redisPackage.get("sendUserId"));
                data.put("tagType", redisPackage.get("sendUserId"));//发送者Id
                data.put("msgId", rpId);//红包ID
                data.put("msgType", "2");//游戏Id
                data.put("roomId", redisPackage.get("roomId"));//房间Id
                data.put("name", nickname);//姓名
                data.put("portrait", portrait);//头像
                data.put("content", amount + "-" + redisPackage.get("thunderNumber"));//"8-7"
                data.put("getState", 2);//1 未领取 2 已领取 3.已截止
                data.put("mediumMineNumber", mediumMineNumber);//中雷数

                if (status == 2) {
                    data.put("getState", 3);//1 未领取 2 已领取 3.已截止
                }
            }
        }

        return data;
    }


    public static String formatAmount(String amount){
        return amount.substring(0, amount.length() - 2) + "." + amount.substring(amount.length() - 2);
    }

    //获取牛牛红包详情
    public static JSONObject GetNiuNiuRedPackage(String rpId, Room room, int status) {
        NiuniuRoom roomNiuniu = (NiuniuRoom) room;
        JSONObject data = new JSONObject();
        RedPackage memoryPackage = roomNiuniu.rpPackage.get(rpId);//内存中的红包

        //内存中不为空（刚发完包的推送消息会到这里拿红包消息）
        if (memoryPackage != null) {
            String sendUserId = memoryPackage.sendUserId;
            String nickname = R_USER.hget(sendUserId, "nickname");//得到发包者昵称
            String portrait = R_USER.hget(sendUserId, "portrait");//得到发包者头像
            String amount = formatAmount(String.valueOf(memoryPackage.amount));//格式化发送红包的金额

            //基本信息
            data.put("msgId", memoryPackage.rpId);//红包ID
            data.put("tagType", memoryPackage.sendUserId);//发送者Id
            data.put("msgType", "3");//1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息  7.龙虎红包  8.龙虎结算
            data.put("roomId", memoryPackage.roomId);//房间Id
            data.put("name", nickname);//发包者昵称
            data.put("portrait", portrait);//发包者头像
            data.put("content", amount + "-" + memoryPackage.sumCount);//红包显示信息
            data.put("getState", 1);//1 未领取 2 已领取 3红包派发玩  4时间到  5 余额不足 6红包撤回

        }else{//内存中为空要么是真的结算完了----要么是数据丢失
            Map<String, String> redisPackage = R_REDPACKAGE_INFO.hgetAll(rpId);//Redis中的红包
            Out.debug(redisPackage);

            if (redisPackage.keySet().size() != 0) {//红包数据确实存在
                String sendUserId = redisPackage.get("sendUserId");
                String nickname = R_USER.hget(sendUserId, "nickname");//得到发包者昵称
                String portrait = R_USER.hget(sendUserId, "portrait");//得到发包者头像
                String amount =formatAmount( redisPackage.get("amount"));
                int sumCount = Integer.parseInt(redisPackage.get("sumCount"));//总包数

                //基本信息
                data.put("msgId", rpId);//红包ID
                data.put("tagType", sendUserId);//发送者Id
                data.put("msgType", "3");//1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息  7.龙虎红包  8.龙虎结算
                data.put("roomId", redisPackage.get("roomId"));//房间Id
                data.put("name", nickname);//姓名
                data.put("portrait", portrait);//头像
                data.put("content", amount + "-" + sumCount);//"8-7"
                data.put("getState", 1);//1 未领取 2 已领取 3.已截止

                //结算信息
                if (status==2){//1发出状态  2.结算完  状态等于2时代表已经结算完成  需要改变状态返给前端显示--并添加结算信息
                    //改变显示信息模板
                    data.put("msgType", "4");//1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息  7.龙虎红包  8.龙虎结算
                    int rpStatus=Integer.valueOf(redisPackage.get("status"));//得到红包完成状态  0:发出 1:完成  2.撤回

                    if (rpStatus==2){//红包没一个人抢（撤回）
                        //改变红包状态
                        data.put("getState", 6);//1 未领取 2 已领取 3红包派发玩  4时间到  5 余额不足 6红包撤回
                        data.put("killType", 0);//1通杀  2.通赔
                        data.put("bankerWin", 0);//庄家赢
                        data.put("sentWin", 0);//闲家赢
                        data.put("bankerBoints", 0);//庄家点数
                    }else{//正常结算

                        //改变红包状态
                        data.put("getState", 3);//1 未领取 2 已领取 3红包派发玩  4时间到  5 余额不足 6红包撤回

                        int victoryNumber = Integer.parseInt(redisPackage.get("victoryNumber"));//庄家赢的包数
                        int count = Integer.parseInt(redisPackage.get("count"));//剩余包数

                        //添加结算信息
                        int killType = 0;//1通杀  2.通赔

                        if (sumCount-1==count){//只有发包者抢了
                            killType=0;
                        }else{
                            if (victoryNumber==0){//庄一家都没赢
                                killType = 2;//2.通赔
                            }else if(victoryNumber==sumCount-1-count){
                                killType = 1;//1通杀
                            }
                        }
                        data.put("killType", killType);//1通杀  2.通赔
                        data.put("bankerWin", victoryNumber);//庄家赢
                        data.put("sentWin", sumCount - count - victoryNumber-1);//闲家赢
                        data.put("bankerBoints", redisPackage.get("bankerNiuNumber"));//庄家点数
                    }
                }
            }
        }
        return data;
    }



    //获取禁抢红包详情
    public static JSONObject GetJinQiangRedPackage(String rpId, JinQiangRoom jinQiangRoom, int status) {
        JSONObject data = new JSONObject();
        Map<String, String> redisPackage = R_REDPACKAGE_INFO.hgetAll(rpId);//Redis中的红包
        Out.debug(redisPackage);
        JinQiangPackage memoryPackage = (JinQiangPackage)jinQiangRoom.rpPackage.get(rpId);//内存中的红包

        if (memoryPackage != null) {//内存中不为空
            String sendUserId = memoryPackage.sendUserId;
            String nickname = R_USER.hget(sendUserId, "nickname");//得到发包者昵称
            String portrait = R_USER.hget(sendUserId, "portrait");//得到发包者头像
            String amount = String.valueOf(memoryPackage.amount);
            amount = amount.substring(0, amount.length() - 2) + "." + amount.substring(amount.length() - 2);
            data.put("msgId", memoryPackage.rpId);//红包ID
            data.put("tagType", memoryPackage.sendUserId);//发送者Id
            data.put("msgType", "5");//1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息  7.龙虎红包  8.龙虎结算
            data.put("roomId", memoryPackage.roomId);//房间Id
            data.put("name", nickname);//姓名
            data.put("portrait", portrait);//头像

            data.put("content",  amount+ "-" + memoryPackage.thunderNumber);//"8-7"
            data.put("getState", 1);//1 未领取 2 已领取 3红包派发玩  4时间到  5 余额不足 6红包撤回
            data.put("rayList", memoryPackage.thunderNumber);//雷号集合
            data.put("redType", memoryPackage.redType);//0 默认  666为六不中
            data.put("sendCount", amount);//发包总金额
            data.put("redCount", memoryPackage.sumCount);//发包数量
        } else {
            if (redisPackage.keySet().size() != 0) {
                String sendUserId = redisPackage.get("sendUserId");
                String nickname = R_USER.hget(sendUserId, "nickname");//得到发包者昵称
                String portrait = R_USER.hget(sendUserId, "portrait");//得到发包者头像
                String amount = redisPackage.get("amount");
                amount = amount.substring(0, amount.length() - 2) + "." + amount.substring(amount.length() - 2);
                data.put("tagType", redisPackage.get("sendUserId"));//发送者Id
                data.put("msgId", rpId);//红包ID
                data.put("msgType", "5");//1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息  7.龙虎红包  8.龙虎结算
                data.put("roomId", redisPackage.get("roomId"));//房间Id
                data.put("name", nickname);//姓名
                data.put("portrait", portrait);//头像
                data.put("rayList", redisPackage.get("thunderNumber"));//埋雷数
                data.put("tag", redisPackage.get("redType"));//红包玩法--//0 默认  666为六不中
                data.put("sendCount", amount);//发包金额
                data.put("redCount", redisPackage.get("sumCount"));//发包数量
                data.put("content", amount + "-" +  redisPackage.get("thunderNumber"));//"8-7"
                data.put("getState", 3);//1 未领取 2 已领取 3红包派发玩  4时间到  5 余额不足 6红包撤回
                if (status == 2) {
                    data.put("msgType", "6");//1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息  7.龙虎红包  8.龙虎结算
                    int mediumMineNumber = Integer.parseInt(redisPackage.get("mediumMineNumber"));//中雷
                    data.put("rayCount", mediumMineNumber);//中雷数

                    if (redisPackage.get("rpResult")==null){
                        data.put("getState", 6);//1 未领取 2 已领取 3红包派发玩  4时间到  5 余额不足 6红包撤回
                    }else{
                        data.put("getState", 3);//1 未领取 2 已领取 3红包派发玩  4时间到  5 余额不足 6红包撤回
                    }
                    String bonus = redisPackage.get("bonus");
                    if (!"0".equals(bonus)){
                        bonus = bonus.substring(0, bonus.length() - 2) + "." + bonus.substring(bonus.length() - 2);
                    }

                    data.put("bonus", bonus);//奖金金额
                    data.put("rate", redisPackage.get("rate"));//红包赔率
                }
            }
        }
        return data;
    }


    //获取龙虎红包详情
    public static JSONObject GetLongHuRedPackage(String rpId, LongHuRoom longHuRoom, int status) {
        JSONObject data = new JSONObject();
        Map<String, String> redisPackage = R_REDPACKAGE_INFO.hgetAll(rpId);//Redis中的红包
        Out.info(redisPackage);
        LongHuPackage memoryPackage = (LongHuPackage)longHuRoom.rpPackage.get(rpId);//内存中的红包

        if (memoryPackage != null) {//内存中不为空
            String sendUserId = memoryPackage.sendUserId;
            String nickname = R_USER.hget(sendUserId, "nickname");//得到发包者昵称
            String portrait = R_USER.hget(sendUserId, "portrait");//得到发包者头像
            String amount = String.valueOf(memoryPackage.amount);
            amount = amount.substring(0, amount.length() - 2) + "." + amount.substring(amount.length() - 2);
            data.put("msgId", memoryPackage.rpId);//红包ID
            data.put("tagType", memoryPackage.sendUserId);//发送者Id
            data.put("msgType", "7"); //1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息
            data.put("roomId", memoryPackage.roomId);//房间Id
            data.put("name", nickname);//姓名
            data.put("portrait", portrait);//头像
            data.put("content", amount );//"8-7"  memoryPackage.detain发包者押的龙赢--虎赢标识
            data.put("getState", 1);//1 未领取 2 已领取 3红包派发玩  4时间到  5 余额不足 6红包撤回
            data.put("detain", memoryPackage.detain);//发包者押的龙赢--虎赢标识
        } else {
            if (redisPackage.keySet().size() != 0) {
                String sendUserId = redisPackage.get("sendUserId");
                String nickname = R_USER.hget(sendUserId, "nickname");//得到发包者昵称
                String portrait = R_USER.hget(sendUserId, "portrait");//得到发包者头像

                String amount = redisPackage.get("amount");
                amount = amount.substring(0, amount.length() - 2) + "." + amount.substring(amount.length() - 2);

                data.put("tagType", redisPackage.get("sendUserId"));//发送者Id
                data.put("msgId", rpId);//红包ID
                data.put("msgType", "7"); //1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息  7.龙虎红包  8.龙虎结算
                data.put("roomId", redisPackage.get("roomId"));//房间Id
                data.put("name", nickname);//姓名
                data.put("portrait", portrait);//头像
                data.put("content", amount );//"8-7"   type发包者押的龙赢--虎赢标识
                data.put("getState", 2);//1 未领取 2 已领取 3红包派发玩  4时间到  5 余额不足 6红包撤回
                data.put("detain", Integer.parseInt(redisPackage.get("detain")));//发包者押的龙赢--虎赢标识


                int sumCount = Integer.parseInt(redisPackage.get("sumCount"));
                int count = Integer.parseInt(redisPackage.get("count"));
                int victoryNumber = Integer.parseInt(redisPackage.get("victoryNumber"));


                if (status == 2) {
                    int wol= Integer.parseInt(redisPackage.get("wol"));
                    int killType = 0;//1通杀  2.通赔
                    if (wol!=0){
                        if (victoryNumber==0&&sumCount!=count){//庄一家都没赢
                            killType = 2;//2.通赔
                        }else if(victoryNumber==sumCount-1-count){
                            killType = 1;//1通杀
                        }
                    }
                    killType=0;
                    Out.debug("--------------------------");
                    Out.info(killType);
                    Out.debug("--------------------------");

                    int longyNumber= Integer.parseInt(redisPackage.get("longyNumber"));//得到龙的数量
                    data.put("dragon", longyNumber);//龙的人数
                    data.put("tiger", sumCount-longyNumber-count);//虎的人数
                    data.put("killType", killType);//1通杀  2.通赔
                    data.put("wol",wol);//龙虎输赢结果
                    data.put("getState", 3);//1 未领取 2 已领取 3红包派发玩  4时间到  5 余额不足 6红包撤回
                    data.put("msgType", "8"); //1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息  7.龙虎红包  8.龙虎结算
                }
            }
        }

        return data;
    }


    //获取福利红包详情
    public static JSONObject GetWelfareRedPackage(String rpId, Room room, int status) {
        WelfareRoom welfareRoom = (WelfareRoom) room;
        JSONObject data = new JSONObject();
        RedPackage memoryPackage = welfareRoom.rpPackage.get(rpId);//内存中的红包

        //内存中不为空（刚发完包的推送消息会到这里拿红包消息）
        if (memoryPackage != null) {
            String sendUserId = memoryPackage.sendUserId;
            String nickname = R_USER.hget(sendUserId, "nickname");//得到发包者昵称
            String portrait = R_USER.hget(sendUserId, "portrait");//得到发包者头像
            String amount = formatAmount(String.valueOf(memoryPackage.amount));//格式化发送红包的金额

            //基本信息
            data.put("msgId", memoryPackage.rpId);//红包ID
            data.put("tagType", memoryPackage.sendUserId);//发送者Id
            data.put("msgType", "9");//1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息  7.龙虎红包  8.龙虎结算
            data.put("roomId", memoryPackage.roomId);//房间Id
            data.put("name", nickname);//发包者昵称
            data.put("portrait", portrait);//发包者头像
            data.put("content", amount + "-" + memoryPackage.sumCount);//红包显示信息
            data.put("getState", 1);//1 未领取 2 已领取 3红包派发玩  4时间到  5 余额不足 6红包撤回

        }else{//内存中为空要么是真的结算完了----要么是数据丢失
            Map<String, String> redisPackage = R_REDPACKAGE_INFO.hgetAll(rpId);//Redis中的红包
            Out.debug(redisPackage);

            if (redisPackage.keySet().size() != 0) {//红包数据确实存在
                String sendUserId = redisPackage.get("sendUserId");
                String nickname = R_USER.hget(sendUserId, "nickname");//得到发包者昵称
                String portrait = R_USER.hget(sendUserId, "portrait");//得到发包者头像
                String amount =formatAmount( redisPackage.get("amount"));
                int sumCount = Integer.parseInt(redisPackage.get("sumCount"));//总包数

                //基本信息
                data.put("msgId", rpId);//红包ID
                data.put("tagType", sendUserId);//发送者Id
                data.put("msgType", "9");//1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息  7.龙虎红包  8.龙虎结算
                data.put("roomId", redisPackage.get("roomId"));//房间Id
                data.put("name", nickname);//姓名
                data.put("portrait", portrait);//头像
                data.put("content", amount + "-" + sumCount);//"8-7"
                data.put("getState", 3);//1 未领取 2 已领取 3.已截止
            }
        }
        return data;
    }

    //获取福利红包详情
    public static JSONObject GetJieLongPackage(String rpId, Room room, int status) {
        JieLongRoom jieLongRoom = (JieLongRoom) room;
        JSONObject data = new JSONObject();
        RedPackage memoryPackage = jieLongRoom.rpPackage.get(rpId);//内存中的红包

        //内存中不为空（刚发完包的推送消息会到这里拿红包消息）
        if (memoryPackage != null) {
            String sendUserId = memoryPackage.sendUserId;
            String nickname = R_USER.hget(sendUserId, "nickname");//得到发包者昵称
            String portrait = R_USER.hget(sendUserId, "portrait");//得到发包者头像
            String amount = formatAmount(String.valueOf(memoryPackage.amount));//格式化发送红包的金额

            //基本信息
            data.put("msgId", memoryPackage.rpId);//红包ID
            data.put("tagType", memoryPackage.sendUserId);//发送者Id
            data.put("msgType", "10");//1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息  7.龙虎红包  8.龙虎结算
            data.put("roomId", memoryPackage.roomId);//房间Id
            data.put("name", nickname);//发包者昵称
            data.put("portrait", portrait);//发包者头像
            data.put("content", amount + "-" + memoryPackage.sumCount);//红包显示信息
            data.put("getState", 1);//1 未领取 2 已领取 3红包派发玩  4时间到  5 余额不足 6红包撤回

        }else{//内存中为空要么是真的结算完了----要么是数据丢失
            Map<String, String> redisPackage = R_REDPACKAGE_INFO.hgetAll(rpId);//Redis中的红包
            Out.debug(redisPackage);

            if (redisPackage.keySet().size() != 0) {//红包数据确实存在
                String sendUserId = redisPackage.get("sendUserId");
                String nickname = R_USER.hget(sendUserId, "nickname");//得到发包者昵称
                String portrait = R_USER.hget(sendUserId, "portrait");//得到发包者头像
                String amount =formatAmount( redisPackage.get("amount"));
                int sumCount = Integer.parseInt(redisPackage.get("sumCount"));//总包数

                //基本信息
                data.put("msgId", rpId);//红包ID
                data.put("tagType", sendUserId);//发送者Id
                data.put("msgType", "10");//1 系统消息，2 扫雷红包 ，3 牛牛红包 ，4 牛牛系统 5 禁枪红包 6 播报消息  7.龙虎红包  8.龙虎结算
                data.put("roomId", redisPackage.get("roomId"));//房间Id
                data.put("name", nickname);//姓名
                data.put("portrait", portrait);//头像
                data.put("content", amount + "-" + sumCount);//"8-7"
                data.put("getState", 3);//1 未领取 2 已领取 3.已截止
            }
        }
        return data;
    }




}
