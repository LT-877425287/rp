package com.sencorsta.ids.game.http.longhu;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.util.GetRedPackageInfo;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.LongHuPackage;
import com.sencorsta.ids.game.world.room.LongHuRoom;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @author TAO
 * @description: 获取龙虎红包详情
 * @date 2019/11/17 17:21
 */
public class GetLongHuRedPackage extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("---------------------GetLongHuRedPackage------GetLongHuRedPackage---------------------");

        int roomId=params.getInteger("roomId");
        String rpId=params.getString("rpId");

        LongHuRoom longHuRoom=(LongHuRoom) World.getInstance().games.get(World.LONGHU).rooms.get(roomId);//得到当前房间
        if (longHuRoom==null){
            return error(ErrorCodeList.ROOM_IS_NULL);
        }

        LongHuPackage longHuPackage= (LongHuPackage) longHuRoom.rpPackage.get(rpId);
        JSONObject result;
        if (longHuPackage!=null){
            result=GetRedPackageInfo.GetLongHuRedPackage(rpId,longHuRoom,1);//状态 1.未领取  2.已结算
        }else{
            result=GetRedPackageInfo.GetLongHuRedPackage(rpId,longHuRoom,2);//状态 1.未领取  2.已结算
        }

        if (result==null){
            return error(ErrorCodeList.PACKET_IS_NULL);
        }

        return success(result);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }


    @Override
    public String getPath() {
        return "/Game/GetLongHuRedPackage";
    }
}
