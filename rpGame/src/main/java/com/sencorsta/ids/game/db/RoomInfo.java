package com.sencorsta.ids.game.db;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.MysqlServiceS;
import com.sencorsta.ids.core.log.Out;

/**
* @description: 房间
* @author TAO
* @date 2019/12/15 16:22
*/
public class RoomInfo {

    public static JSONArray getRoomInfo(){
        String sql="select `gameId`,`roomId`,`icon`,`roomName`,`announcement`,`notice`,`regulation`,`playingMethodDescribe`,`type`,`main`,`isSys`,`passworld`,`status`,`magnification`,`minAmount`,`maxAmount`,`sMinAmount`,`sMaxAmount`,`extData`,`sequence` from `roomInfo` where `status`=2";
        JSONArray result= MysqlServiceS.getInstance().select(sql);
        if (result.size()>0){
            return result;
        }
        return null;
    }
    public static JSONObject getRoomIdInfo(int id){
        String sql="select `gameId`,`roomId`,`icon`,`roomName`,`announcement`,`notice`,`regulation`,`playingMethodDescribe`,`type`,`main`,`isSys`,`passworld`,`status`,`magnification`,`minAmount`,`maxAmount`,`sMinAmount`,`sMaxAmount`,`extData`,`sequence` from `roomInfo` where `status`=2 and `id`=?";
        Object[] args=new Object[]{
                id
        };
        JSONArray result= MysqlServiceS.getInstance().select(sql,args);
        if (result.size()>0){
            return JSONObject.parseObject(String.valueOf(result.get(0)));
        }
        return null;
    }
    public static JSONObject getRoomIdInfoByRoomID(int roomId){
        String sql="select `gameId`,`roomId`,`icon`,`roomName`,`announcement`,`notice`,`regulation`,`playingMethodDescribe`,`type`,`main`,`isSys`,`passworld`,`status`,`magnification`,`minAmount`,`maxAmount`,`sMinAmount`,`sMaxAmount`,`extData`,`sequence` from `roomInfo` where `status`=2 and `roomId`=?";
        Object[] args=new Object[]{
                roomId
        };
        JSONArray result= MysqlServiceS.getInstance().select(sql,args);
        if (result.size()>0){
            return JSONObject.parseObject(String.valueOf(result.get(0)));
        }
        return null;
    }

}
