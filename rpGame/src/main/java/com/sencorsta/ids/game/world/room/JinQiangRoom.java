package com.sencorsta.ids.game.world.room;

import com.alibaba.fastjson.JSON;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.JinQiangPackage;
import com.sencorsta.ids.game.world.pak.RedPackage;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author TAO
 * @description: 禁枪房间
 * @date 2019/11/19 22:43
 */
public class JinQiangRoom extends Room {

    public Map<Integer, Map<Integer, Double>> magnification;//禁抢玩法固定倍率


    public JinQiangRoom(String icon, int roomId, String roomName, String announcement, String notice, String regulation, String playingMethodDescribe, int type,String main,int isSys,long minAmount,long maxAmount,long sMinAmount,long sMaxAmount,int status) {
        super(icon, roomId, roomName, announcement, notice, regulation, playingMethodDescribe,main,isSys,minAmount,maxAmount,sMinAmount,sMaxAmount,status);
        Map<Integer, Map<Integer, Double>> magnification = new HashMap<>();
            Out.debug("-------------------有倍率-------------------");
            Map<Integer, Double> five = new HashMap<>();//五单包
            five.put(1, 2D);
            five.put(2, 3.5D);
            five.put(3, 5D);
            five.put(4, 20D);
            five.put(5, 25D);

            Map<Integer, Double> five2 = new HashMap<>();//五连包
            five2.put(2, 5D);
            five2.put(3, 21D);
            five2.put(4, 40D);
            five2.put(5, 60D);

            Map<Integer, Double> six = new HashMap<>();//六单包
            six.put(1, 1.6D);
            six.put(2, 3.2D);
            six.put(3, 6D);
            six.put(4, 10D);
            six.put(5, 20D);
            six.put(6, 30D);

            Map<Integer, Double> six2 = new HashMap<>();//六连包
            six2.put(2, 4D);
            six2.put(3, 13.5D);
            six2.put(4, 30D);
            six2.put(5, 50D);
            six2.put(6, 80D);


            Map<Integer, Double> seven = new HashMap<>();//七单包
            seven.put(1, 1.25D);
            seven.put(2, 2.5D);
            seven.put(3, 3.75D);
            seven.put(4, 5D);
            seven.put(5, 7D);
            seven.put(6, 10D);
            seven.put(7, 10D);

            Map<Integer, Double> seven2 = new HashMap<>();//七连包
            seven2.put(2, 2.5D);
            seven2.put(3, 6D);
            seven2.put(4, 13D);
            seven2.put(5, 20D);
            seven2.put(6, 35D);
            seven2.put(7, 55D);


            Map<Integer, Double> nine = new HashMap<>();//九单包
            nine.put(1, 1D);
            nine.put(2, 2D);
            nine.put(3, 3D);
            nine.put(4, 5D);
            nine.put(5, 7D);
            nine.put(6, 11D);
            nine.put(7, 20D);
            nine.put(8, 20D);
            nine.put(9, 20D);

            Map<Integer, Double> nine2 = new HashMap<>();//九连包
            nine2.put(2, 2D);
            nine2.put(3, 4D);
            nine2.put(4, 9D);
            nine2.put(5, 18D);
            nine2.put(6, 28D);
            nine2.put(7, 45D);
            nine2.put(8, 45D);
            nine2.put(9, 45D);

            Map<Integer, Double> ten = new HashMap<>();//十连包
            ten.put(3, 3D);
            ten.put(4, 7D);
            ten.put(5, 11D);
            ten.put(6, 18D);
            ten.put(7, 30D);
            ten.put(8, 42D);
            ten.put(9, 42D);
            ten.put(10, 42D);

            Map<Integer, Double> sixNo = new HashMap<>();//六不中
            sixNo.put(2, 4D);
            sixNo.put(3, 8D);
            sixNo.put(4, 13D);
            sixNo.put(5, 22D);

            magnification.put(5, five);
            magnification.put(55, five2);
            magnification.put(6, six);
            magnification.put(66, six2);
            magnification.put(666, sixNo);
            magnification.put(7, seven);
            magnification.put(77, seven2);
            magnification.put(9, nine);
            magnification.put(99, nine2);
            magnification.put(10, ten);


        //}
        this.magnification = magnification;
    }

    //得到当前房间抢包者的最高赔率
    public double getMagnification(int type) {
        Map<Integer, Map<Integer, Double>> magnification = this.magnification;
        Map<Integer, Double> currentMagnification = magnification.get(type);//得到当前玩法的倍率
        if (currentMagnification.keySet().size() != 0) {//倍率存在
            Collection<Double> c = currentMagnification.values();
            Object[] obj = c.toArray();
            Arrays.sort(obj);//排序顺序
            return Double.parseDouble(obj[obj.length - 1].toString());
        }
        return 1;
    }


    //根据红包类型得到赔率列表
    public Map<Integer, Double> getCurrentMagnification(JinQiangRoom jinQiangRoom, int type) {
        Out.debug("type==>", type);
        Out.debug(JSON.toJSON(jinQiangRoom.magnification));
        return jinQiangRoom.magnification.get(type);
    }

    @Override
    public void update() {
        for (RedPackage jinQiangPackage : rpPackage.values()) {
            jinQiangPackage.update();
        }
    }

}
