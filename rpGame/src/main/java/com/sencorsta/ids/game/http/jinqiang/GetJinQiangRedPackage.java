package com.sencorsta.ids.game.http.jinqiang;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.util.GetRedPackageInfo;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.JinQiangPackage;
import com.sencorsta.ids.game.world.room.JinQiangRoom;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @author TAO
 * @description: 获取禁抢红包详情
 * @date 2019/11/17 17:21
 */
public class GetJinQiangRedPackage extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("---------------------获取禁抢红包详情------GetJinQiangRedPackage---------------------");

        int roomId=params.getInteger("roomId");
        String rpId=params.getString("rpId");

        JinQiangRoom jinQiangRoom=(JinQiangRoom) World.getInstance().games.get(World.JINGQIANG).rooms.get(roomId);//得到当前房间
        if (jinQiangRoom==null){
            return error(ErrorCodeList.ROOM_IS_NULL);
        }

        JinQiangPackage jinQiangPackage= (JinQiangPackage) jinQiangRoom.rpPackage.get(rpId);
        JSONObject result;
        if (jinQiangPackage!=null){
            result=GetRedPackageInfo.GetJinQiangRedPackage(rpId,jinQiangRoom,1);//状态 1.未领取  2.已结算

        }else{
            result=GetRedPackageInfo.GetJinQiangRedPackage(rpId,jinQiangRoom,2);//状态 1.未领取  2.已结算
        }

        if (result==null){
            return error(ErrorCodeList.PACKET_IS_NULL);
        }

        return success(result);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }


    @Override
    public String getPath() {
        return "/Game/GetJinQiangRedPackage";
    }
}
