package com.sencorsta.ids.game.http.saolei;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.util.GetRedPackageInfo;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.SaoLeiPackage;
import com.sencorsta.ids.game.world.room.SaoLeiRoom;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;


/**
 * @author TAO
 * @description: 获取扫雷红包详情
 * @date 2019/11/17 17:21
 */
public class GetSaoLeiRedPackage extends HttpHandler {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("---------------------获取扫雷红包详情------GetSaoLeiRedPackage---------------------");
        int roomId=params.getInteger("roomId");
        String rpId=params.getString("rpId");

        SaoLeiRoom saoLeiRoom=(SaoLeiRoom) World.getInstance().games.get(World.SAOLEI).rooms.get(roomId);//得到当前房间
        if (saoLeiRoom==null){
            return error(ErrorCodeList.ROOM_IS_NULL);
        }
        SaoLeiPackage saoLeiPackage= (SaoLeiPackage) saoLeiRoom.rpPackage.get(rpId);//得到内存中的扫雷红包
        JSONObject result;
        if (saoLeiPackage!=null){
            result= GetRedPackageInfo.GetSaoLeiRedPackage(rpId,saoLeiRoom,1);//状态 1.未领取  2.已结算
        }else{
            result=GetRedPackageInfo.GetSaoLeiRedPackage(rpId,saoLeiRoom,2);//状态 1.未领取  2.已结算
        }
        if (result==null){
            return error(ErrorCodeList.PACKET_IS_NULL);
        }
        return success(result);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/GetSaoLeiRedPackage";
    }
}
