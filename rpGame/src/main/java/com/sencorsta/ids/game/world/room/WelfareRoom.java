package com.sencorsta.ids.game.world.room;

import com.sencorsta.ids.common.service.IdService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.game.db.RpMassage;
import com.sencorsta.ids.game.proto.GamePushRedPackageByProto;
import com.sencorsta.ids.game.util.PushAndSaved;
import com.sencorsta.ids.game.world.pak.RedPackage;
import com.sencorsta.ids.game.world.pak.WelfarePackage;
import com.sencorsta.utils.date.DateUtil;
import com.sencorsta.utils.string.StringUtil;

import java.util.*;
import java.util.concurrent.*;


/**
 * @author TAO
 * @description: 福利玩法
 * @date 2020/1/5 18:57
 */
public class WelfareRoom extends Room {

    //minAmount ==发包间隔
    //maxAmount ==发包金额
    //sMinAmount ==每日开始时间
    //sMaxAmount ==每日结束时间
    //magnification ==发包个数
    //passworld ==最小抢包流水


    public long SEND_INTERVAL;//发包间隔时间--分钟
    public long SEND_AMOUNT;//发包金额
    public long FIRST_SEND_INTERVAL;//发包开始时间
    public long LAST_SEND_INTERVAL;//发包停止时间
    public int SEND_COUNT;//发包个数
    public long ROB_RUNWATER;//最小抢包流水



    public WelfareRoom(String icon, int roomId, String roomName, String announcement, String notice, String regulation, String playingMethodDescribe, double sendCount, String main, int isSys, long minAmount, long maxAmount, long sMinAmount, long sMaxAmount, String passworld, int status) {
        super(icon, roomId, roomName, announcement, notice, regulation, playingMethodDescribe, main, isSys, minAmount, maxAmount, sMinAmount, sMaxAmount, status);


        this.SEND_INTERVAL = minAmount;//发包间隔时间
        this.SEND_AMOUNT = maxAmount;//发包金额
        this.FIRST_SEND_INTERVAL = sMinAmount;//每日开始时间
        this.LAST_SEND_INTERVAL = sMaxAmount;//每日结束时间
        this.SEND_COUNT = (int) sendCount;//发包个数

        if (StringUtil.isEmpty(passworld)) {
            this.ROB_RUNWATER = 0;//最小抢包流水
        } else {
            this.ROB_RUNWATER = Long.parseLong(passworld);
        }
        {
            ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
            Calendar calendar = Calendar.getInstance();
            executor.scheduleWithFixedDelay(() -> {
                int nowMinit = calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE);
                if (nowMinit >= FIRST_SEND_INTERVAL * 60 && nowMinit < LAST_SEND_INTERVAL * 60) {// 判断时间是否大于开始时间 以及小于结束时间
                    sendPackage();
                }
                if (nowMinit >= LAST_SEND_INTERVAL * 60) {
                    executor.shutdown();
                }
                //},0,SEND_INTERVAL*60,TimeUnit.MINUTES);
            }, 60 - calendar.get(Calendar.SECOND), SEND_INTERVAL * 60, TimeUnit.SECONDS);
        }
        {
            ScheduledExecutorService executorDay = Executors.newScheduledThreadPool(1);
            executorDay.scheduleWithFixedDelay(() -> {
                try {
                    ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
                    Calendar calendar = Calendar.getInstance();
                    executor.scheduleWithFixedDelay(() -> {
                        int nowMinit = calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE);
                        if (nowMinit >= FIRST_SEND_INTERVAL * 60 && nowMinit < LAST_SEND_INTERVAL * 60) {// 判断时间是否大于开始时间 以及小于结束时间
                            sendPackage();
                        }
                        if (nowMinit >= LAST_SEND_INTERVAL * 60) {
                            executor.shutdown();
                        }
                        //},0,SEND_INTERVAL*60,TimeUnit.MINUTES);
                    }, 60 - calendar.get(Calendar.SECOND), SEND_INTERVAL * 60, TimeUnit.SECONDS);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, 24 - Calendar.getInstance().HOUR_OF_DAY, 24, TimeUnit.HOURS);
        }
    }

    @Override
    public void update() {

        for (RedPackage welfarePackage : rpPackage.values()) {
            welfarePackage.update();
        }

    }


    public void sendPackage() {
        try {
            Out.debug("开始发送福利包----");

            String rpId = IdService.newRpId();//红包Id

            WelfarePackage welfarePackage = new WelfarePackage(mainUserId, rpId, SEND_COUNT, SEND_AMOUNT, this, ROB_RUNWATER, roomId, true);//创建一个扫雷玩法的红包
            this.rpPackage.put(rpId, welfarePackage);//将扫雷房间的包容器中添加当前包


            RpMassage.insertRpMessage2(rpId, roomId, gameId, 1);
            GamePushRedPackageByProto.PushRedPackage.Builder push = GamePushRedPackageByProto.PushRedPackage.newBuilder();
            push.setGameId(gameId);
            push.setRoomId(roomId);
            push.setRpId(rpId);

            RpcMessage pushMsg = PushAndSaved.pushProto("", "push.RedPackage", push);
            this.broadcast(pushMsg, "proxy");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}


//        long NOW_TIME=new Date().getTime();//得到当前当前时间
//        //得到今天的第一时刻
//        // long TODAY_TIME= DateUtil.getNextDayMillisecond()-24*S;
//        long TODAY_TIME=1578294000000l;//得到21点时间戳
//
//        //得到今天的开始时间
//        long BEGIN_TIME=TODAY_TIME+S*FIRST_SEND_INTERVAL;
//
//        //得到今天结束时间
//        long FINISH_TIME=TODAY_TIME+S*LAST_SEND_INTERVAL;
//
//
//        //if (NOW_TIME>=BEGIN_TIME && NOW_TIME<=FINISH_TIME+1000){//满足发包条件  当前时间>开始发包时间    &&  当前发包时间<结束发包时间
//        if (NOW_TIME>=BEGIN_TIME ){//满足发包条件  当前时间>开始发包时间    &&  当前发包时间<结束发包时间(10*60*1000//最后一个包的误差范围)
//            //下次发包时间
//            Out.debug("NOW_TIME==>",NOW_TIME,"_---SEND_PACKAGE_TIME==>",SEND_PACKAGE_TIME);
//            if (NOW_TIME>=SEND_PACKAGE_TIME){
//                //发红包
//                Out.debug("发包");
//
//                String rpId = IdService.newRpId();//红包Id
//
//                WelfarePackage welfarePackage = new WelfarePackage(mainUserId, rpId, SEND_COUNT, SEND_AMOUNT,this,ROB_RUNWATER,roomId,true);//创建一个扫雷玩法的红包
//                this.rpPackage.put(rpId, welfarePackage);//将扫雷房间的包容器中添加当前包
//
//
//                RpMassage.insertRpMessage2(rpId,roomId,gameId,1);
//                GamePushRedPackageByProto.PushRedPackage.Builder push = GamePushRedPackageByProto.PushRedPackage.newBuilder();
//                push.setGameId(gameId);
//                push.setRoomId(roomId);
//                push.setRpId(rpId);
//
//                RpcMessage pushMsg= PushAndSaved.pushProto("", "push.RedPackage", push);
//                this.broadcast(pushMsg,"proxy");
//
//
//
//
//
//                SEND_PACKAGE_TIME=  SEND_INTERVAL*S+NOW_TIME;
//                if(SEND_PACKAGE_TIME>NOW_TIME){
//
//                }
//            }
//        }
