package com.sencorsta.ids.game.db;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.game.world.pak.RedPackage;

public class SettlementResult {

    public static JSONObject makeResult(RedPackage redPackage, JSONObject jsonObject, String userId,int gameId){
        String sql="insert into `settlement_result`(`userId`,`rpId`,`gameId`,`roomId`,`sendAmount`,`sendAmountback`,`robMoney`,`resultMoney`,`drawWater`,`bonus`,`total`)" +
                "values(?,?,?,?,?,?,?,?,?,?,?)";
        Object[] args=new Object[]{
                userId,
                redPackage.rpId,
                gameId,
                redPackage.roomId,
                jsonObject.getLongValue("sendAmount"),
                jsonObject.getLongValue("sendAmountback"),
                jsonObject.getLongValue("robMoney"),
                jsonObject.getLongValue("resultMoney"),
                jsonObject.getLongValue("drawWater"),
                jsonObject.getLongValue("bonus"),
                jsonObject.getLongValue("total"),
        };
        JSONObject res=new JSONObject();
        res.put("SQL",sql);
        res.put("args",args);
        return res;
    }

    public static void doKyResult(String userId,long total){
        if (total==0){
            return;
        }
        String sql="insert into `the_3rd_result`(`userId`,`platform`,`totalAmount`)" +
                "values(?,?,?)";
        Object[] args=new Object[]{
                userId,
                "ky",
                total,
        };
        MysqlService.getInstance().updateAsyn(sql,args);
    }

}
