package com.sencorsta.ids.game.http.longhu;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.LongHuPackage;
import com.sencorsta.ids.game.world.room.LongHuRoom;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.Map;

/**
* @description: 获取龙虎红包结算结果
* @author TAO
* @date 2019/11/22 22:22
*/
public class GetLongHuRedPackageResult extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-------------------获取龙虎红包结算结果----GetLongHuRedPackageResult-----------------");
        String rpId=params.getString("rpId");
        int roomId=params.getInteger("roomId");

        LongHuRoom longHuRoom=(LongHuRoom) World.getInstance().games.get(World.LONGHU).rooms.get(roomId);//得到当前房间
        if (longHuRoom==null){
            return error(ErrorCodeList.ROOM_IS_NULL);
        }
        JSONObject data=new JSONObject();
        Map<String,String> redisPackage=R_REDPACKAGE_INFO.hgetAll(rpId);//Redis中的红包
        LongHuPackage memoryPackage= (LongHuPackage) longHuRoom.rpPackage.get(rpId);//内存中的红包
        if (memoryPackage!=null){//内存中不为空
            data.put("roomId",memoryPackage.roomId);//房间Id
            data.put("time",memoryPackage.time);//剩余时间
            data.put("sumCount",memoryPackage.sumCount);//总共多少
            data.put("amount",memoryPackage.amount);//总共金额
            data.put("type",1);//状态
            data.put("detain", memoryPackage.detain);//发包者押的龙赢--虎赢标识

        }else{
            if (redisPackage.keySet().size()!=0){
                String sendUserId=redisPackage.get("sendUserId");
                int sumCount= Integer.parseInt(redisPackage.get("sumCount"));
                data.put("roomId",redisPackage.get("roomId"));//房间Id
                data.put("sendUserId",redisPackage.get("sendUserId"));//发送者Id
                Out.debug("sendUserId-->",sendUserId);
                data.put("count",redisPackage.get("count"));//剩余多少
                data.put("sumCount",redisPackage.get("sumCount"));//总共多少
                data.put("amount",redisPackage.get("amount"));//总共金额
                data.put("balance",redisPackage.get("balance"));//剩余金额
                data.put("detain", Integer.parseInt(redisPackage.get("detain")));//发包者押的龙赢--虎赢标识
                data.put("wol", redisPackage.get("wol"));//龙虎输赢结果

                JSONArray jsonArray=new JSONArray();
                JSONObject rpResult =JSONObject.parseObject(redisPackage.get("rpResult"));
                if (rpResult!=null){
                    for (var userId:rpResult.keySet()){
                        jsonArray.add(rpResult.get(userId));
                    }
                }
                data.put("rpResult", jsonArray);//结算记录
                data.put("type",2);//状态
            }
        }
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }




    @Override
    public String getPath() {
        return "/Game/GetLongHuRedPackageResult";
    }
}
