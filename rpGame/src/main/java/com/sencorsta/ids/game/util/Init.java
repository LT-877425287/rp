package com.sencorsta.ids.game.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.MysqlServiceS;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.game.db.RoomInfo;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.room.*;
import com.sencorsta.utils.string.StringUtil;

/**
 * @author TAO
 * @description: TODO
 * @date 2019/11/13 18:43
 */
public class Init implements RedisService {
    public static void Initialization() {


        int gameId;//游戏ID
        int roomId;//房间ID
        String icon;
        String roomName;//房间名
        String announcement;//公告
        String notice;//须知
        String regulation;//群规
        String playingMethodDescribe;//玩法
        int type;//房间规则类型
        double magnification;//赔率
        String main;//房主
        int isSys;//是否是系统
        long minAmount;//最小金额
        long maxAmount;//最大金额
        long sMinAmount;//机器人最小金额
        long sMaxAmount;//机器人最大金额
        String passworld;//房间密码--最小流水
        int status;//房间密码--最小流水

        MysqlServiceS.getInstance().waitUtilLoad();
        MysqlService.getInstance().waitUtilLoad();
        JSONArray result = RoomInfo.getRoomInfo();


        if (result != null) {
            for (var item : result) {
                JSONObject jsz = JSONObject.parseObject(String.valueOf(item));
                reloadRoom(jsz);
            }
        }

    }

    public static void reloadRoom(JSONObject result) {
        if (result != null) {

            int gameId = result.getInteger("gameId");
            int roomId = result.getInteger("roomId");
            String icon = result.getString("icon");
            String roomName = result.getString("roomName");
            String announcement = result.getString("announcement");
            String notice = result.getString("notice");
            String regulation = result.getString("regulation");
            String playingMethodDescribe = result.getString("playingMethodDescribe");
            int type = result.getInteger("type");
            double magnification = result.getDouble("magnification");
            String main = result.getString("main");
            int isSys = result.getInteger("isSys");
            long minAmount = result.getLongValue("minAmount");//最小金额
            long maxAmount = result.getLongValue("maxAmount");//最大金额
            long sMinAmount = result.getLongValue("sMinAmount");//机器人最小金额
            long sMaxAmount = result.getLongValue("sMaxAmount");//机器人最大金额
            String passworld = result.getString("passworld");//房间密码--最小流水
            int status = result.getInteger("status");
            int sequence = result.getInteger("sequence");


            World.getInstance().SYSWATERPOOL_ROOM.put(roomId + "", 0l);
            World.getInstance().SYSWATERLINE_ROOM.put(roomId + "", 0l);
            if (!R_SYSTEM.exists(WATERPOOL + "-" + roomId)) {
                R_SYSTEM.put(WATERPOOL + "-" + roomId, "0");
            }
            if (!R_SYSTEM.exists(WATERLINE + "-" + roomId)) {
                R_SYSTEM.put(WATERLINE + "-" + roomId, "0");
            }
            World.getInstance().SYSKILLRATE_ROOM.put(roomId + "", 0l);
            if (!R_SYSTEM.exists(KILLRATE + "-" + roomId)) {
                R_SYSTEM.put(KILLRATE + "-" + roomId, "0");
            }

            switch (gameId) {
                case World.SAOLEI:
                    SaoLeiRoom saoLeiRoom = new SaoLeiRoom(icon, roomId, roomName, announcement, notice, regulation, playingMethodDescribe, type, magnification, main, isSys, minAmount, maxAmount, sMinAmount, sMaxAmount, status);
                    saoLeiRoom.gameId = World.SAOLEI;
                    saoLeiRoom.sequence=sequence;
                    World.getInstance().games.get(World.SAOLEI).rooms.put(roomId, saoLeiRoom);
                    World.getInstance().games.get(World.SAOLEI).newZone(saoLeiRoom);
                    break;
                case World.NIUNIU:
                    NiuniuRoom niuniuRoom = new NiuniuRoom(icon, roomId, roomName, announcement, notice, regulation, playingMethodDescribe, type, main, isSys, minAmount, maxAmount, sMinAmount, sMaxAmount, status);//初始化当前游戏的房间
                    niuniuRoom.gameId = World.NIUNIU;
                    niuniuRoom.sequence=sequence;
                    World.getInstance().games.get(World.NIUNIU).rooms.put(roomId, niuniuRoom);
                    World.getInstance().games.get(World.NIUNIU).newZone(niuniuRoom);
                    break;
                case World.JINGQIANG:
                    JinQiangRoom jinQiangRoom = new JinQiangRoom(icon, roomId, roomName, announcement, notice, regulation, playingMethodDescribe, type, main, isSys, minAmount, maxAmount, sMinAmount, sMaxAmount, status);//初始化当前游戏的房间
                    jinQiangRoom.gameId = World.JINGQIANG;
                    jinQiangRoom.sequence=sequence;
                    World.getInstance().games.get(World.JINGQIANG).rooms.put(roomId, jinQiangRoom);
                    World.getInstance().games.get(World.JINGQIANG).newZone(jinQiangRoom);
                    break;
                case World.LONGHU:
                    LongHuRoom longHuRoom = new LongHuRoom(icon, roomId, roomName, announcement, notice, regulation, playingMethodDescribe, type, main, isSys, minAmount, maxAmount, sMinAmount, sMaxAmount, status);//初始化当前游戏的房间
                    longHuRoom.gameId = World.LONGHU;
                    longHuRoom.sequence=sequence;
                    World.getInstance().games.get(World.LONGHU).rooms.put(roomId, longHuRoom);
                    World.getInstance().games.get(World.LONGHU).newZone(longHuRoom);
                    break;
                case World.WELFARE:
                    WelfareRoom welfareRoom = new WelfareRoom(icon, roomId, roomName, announcement, notice, regulation, playingMethodDescribe, magnification, main, isSys, minAmount, maxAmount, sMinAmount, sMaxAmount, passworld, status);//初始化当前游戏的房间
                    welfareRoom.gameId = World.WELFARE;
                    welfareRoom.sequence=sequence;
                    World.getInstance().games.get(World.WELFARE).rooms.put(roomId, welfareRoom);
                    World.getInstance().games.get(World.WELFARE).newZone(welfareRoom);
                    break;
                case World.JIELONG:
                    JieLongRoom jieLongRoom = new JieLongRoom(icon, roomId, roomName, announcement, notice, regulation, playingMethodDescribe, type, main, isSys, minAmount, maxAmount, sMinAmount, sMaxAmount, status);//初始化当前游戏的房间
                    jieLongRoom.gameId = World.JIELONG;
                    jieLongRoom.sequence=sequence;
                    String extData = result.getString("extData");
                    if (StringUtil.isNotEmpty(extData)){
                        JSONObject jsonExtData=JSONObject.parseObject(extData);
                        jieLongRoom.fixedAmount=jsonExtData.getLongValue("fixedAmount");
                        jieLongRoom.fixedCount=jsonExtData.getIntValue("fixedCount");
                    }
                    World.getInstance().games.get(World.JIELONG).rooms.put(roomId, jieLongRoom);
                    World.getInstance().games.get(World.JIELONG).newZone(jieLongRoom);

                    break;
            }
        }else {
            Out.warn("reloadRoom参数为空！");
        }
    }
}
