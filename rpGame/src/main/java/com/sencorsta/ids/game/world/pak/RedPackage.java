package com.sencorsta.ids.game.world.pak;


import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.game.util.MoneyPiece;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.bean.World;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: 红包基类
 * @author TAO
 * @date 2019/11/14 19:28
 */
public abstract class RedPackage{
    public String sendUserId;//发红包者

    public int roomId ;//房间Id
    public String rpId;//红包ID
    public int count;//红包余数
    public int sumCount;//红包总个数
    public long amount;//红包金额
    public long balance;//红包余额
    public List<MoneyPiece> moneyPieces;//金额分配
    public JSONObject rpResult=new JSONObject();//红包结果
    public int time=120;//红包时间
    public int status=0;//红包状态  0:发出 1:完成  2.撤回
    public long robDeposit;//押金
    public long sendDeposit;//发包者冻结资金
    public Map<String,Long> amountMap=new HashMap<>();
    public Map<String, Player> playerMap=new HashMap<>();
    public boolean isRobedBySend = false;//判断是否抢过
    public boolean isRobotPack = false;//判断是否抢过
    public boolean isRobotRobMustWin = false;//机器人是否必赢
    public boolean isHumanRobMustWin = false;//人类是否必赢

    public JSONObject moneyRecode=new JSONObject();//金额变动


    public RedPackage(String sendUserId, String rpId, int count, long amount, long robDeposit, int roomId,boolean isRobotPack){
        this.sendUserId=sendUserId;
        this.rpId=rpId;
        this.count=count;
        this.sumCount=count;
        this.amount=amount;
        this.balance=amount;
        this.robDeposit=robDeposit;
        this.roomId=roomId;
        this.isRobotPack=isRobotPack;
    }


    public int getPlayerType(String userId){
        return World.getInstance().allPlayers.get(userId).type;
    }



    public int getMaxScore(){
        int score=Integer.MIN_VALUE;
        int index=0;
        for (int i = 0; i < moneyPieces.size() ; i++) {
            if (moneyPieces.get(i).isRobed()||moneyPieces.get(i).isSend()==true){
                continue;
            }
            int jScore=moneyPieces.get(i).getScore();
            if (jScore>score){
                score=jScore;
                index=i;
            }
        }
        return index;
    }

    public int getMinScore(){
        int score=Integer.MAX_VALUE;
        int index=0;
        for (int i = 0; i < moneyPieces.size() ; i++) {
            if (moneyPieces.get(i).isRobed()||moneyPieces.get(i).isSend()==true){
                continue;
            }
            int jScore=moneyPieces.get(i).getScore();
            if (jScore<score){
                score=jScore;
                index=i;
            }
        }
        return index;
    }


    public int getNextIndex(){
        int index=0;
        for (int i = 0; i < moneyPieces.size() ; i++) {
            if (moneyPieces.get(i).isRobed()==false&&moneyPieces.get(i).isSend()==false){
                return i;
            }
        }
        return index;
    }

    public int getSendIndex(){
        int index=0;
        for (int i = 0; i < moneyPieces.size() ; i++) {
            if (moneyPieces.get(i).isSend()==true){
                return i;
            }
        }
        return index;
    }

    public int getWinIndex(){
        int index=-1;
        for (int i = 0; i < moneyPieces.size() ; i++) {
            if (moneyPieces.get(i).isWin()==true&&moneyPieces.get(i).isSend()==false&&moneyPieces.get(i).isRobed()==false){
                return i;
            }
        }
        return index;
    }
    public int getLoseIndex(){
        int index=-1;
        for (int i = 0; i < moneyPieces.size() ; i++) {
            if (moneyPieces.get(i).isWin()==false&&moneyPieces.get(i).isSend()==false&&moneyPieces.get(i).isRobed()==false){
                return i;
            }
        }
        return index;
    }

    @Override
    public String toString() {
        return "RedPackage{" +
                "sendUserId='" + sendUserId + '\'' +
                ", roomId=" + roomId +
                ", rpId='" + rpId + '\'' +
                ", count=" + count +
                ", sumCount=" + sumCount +
                ", amount=" + amount +
                ", balance=" + balance +
                ", moneyPieces=" + JSONObject.toJSONString(moneyPieces) +
                ", rpResult=" + rpResult +
                ", time=" + time +
                ", status=" + status +
                ", robDeposit=" + robDeposit +
                ", sendDeposit=" + sendDeposit +
                ", amountMap=" + amountMap +
                ", playerMap=" + playerMap +
                '}';
    }

    public static long getWinAmount(long leisureAmount, Player player) {
        if (player.type==2){
            return 0;
        }
        if (leisureAmount <= 0) {
            return 0;
        }
        double serviceCharge = World.getInstance().serviceCharge;
        if (serviceCharge == 0) {//没有手续费
            return (long) leisureAmount;
        }
        return (long) (serviceCharge * leisureAmount);
    }

    public abstract void update();
    public abstract void analysis();
    public abstract void rob(Player player);
}
