package com.sencorsta.ids.game.http.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.room.Room;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 获取房间详情
* @author TAO
* @date 2019/11/19 15:01
*/
public class GetRoomInfo extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("--------------------获取房间详情---------GetRoomInfo-----------------");
        int roomId=params.getInteger("roomId");//得到到房间Id
        int gameId=params.getInteger("gameId");//得到到房间类型

        Room room= World.getInstance().games.get(gameId).rooms.get(roomId);
        Out.debug(JSON.toJSON(room));
        return success(JSONObject.toJSON(room));
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/GetRoomInfo";
    }
}
