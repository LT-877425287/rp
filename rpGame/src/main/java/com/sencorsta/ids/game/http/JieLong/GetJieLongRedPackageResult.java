package com.sencorsta.ids.game.http.JieLong;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.JieLongPackage;
import com.sencorsta.ids.game.world.pak.WelfarePackage;
import com.sencorsta.ids.game.world.room.JieLongRoom;
import com.sencorsta.ids.game.world.room.WelfareRoom;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.Map;

/**
* @description: 获取福利红包结算结果
* @author TAO
* @date 2019/11/26 16:22
*/
public class GetJieLongRedPackageResult extends HttpHandler implements RedisService {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-------------------获取福利红包结算结果----GetWelfareRedPackageResult-----------------");
        String rpId=params.getString("rpId");
        int roomId=params.getInteger("roomId");

        JieLongRoom welfareRoom=(JieLongRoom) World.getInstance().games.get(World.JIELONG).rooms.get(roomId);//得到当前房间
        if (welfareRoom==null){
            return error(ErrorCodeList.ROOM_IS_NULL);
        }
        JSONObject data=new JSONObject();
        Map<String,String> redisPackage=R_REDPACKAGE_INFO.hgetAll(rpId);//Redis中的红包
        JieLongPackage memoryPackage= (JieLongPackage)welfareRoom.rpPackage.get(rpId);//内存中的红包
        if (memoryPackage!=null){//内存中不为空
            data.put("roomId",memoryPackage.roomId);//房间Id
            data.put("time",memoryPackage.time);//剩余时间
            data.put("sumCount",memoryPackage.sumCount);//总共多少
            data.put("amount",memoryPackage.amount);//总共金额
            data.put("type",1);//状态
        }else{
            if (redisPackage.keySet().size()!=0){
                data.put("roomId",redisPackage.get("roomId"));//房间Id
                data.put("sendUserId",redisPackage.get("sendUserId"));//发送者Id
                data.put("count",redisPackage.get("count"));//剩余多少
                data.put("sumCount",redisPackage.get("sumCount"));//总共多少
                data.put("amount",redisPackage.get("amount"));//总共金额
                data.put("balance",redisPackage.get("balance"));//剩余金额

                JSONArray jsonArray=new JSONArray();
                JSONObject rpResult =JSONObject.parseObject(redisPackage.get("rpResult"));
                for (var userId:rpResult.keySet()){
                    jsonArray.add(rpResult.get(userId));
                }
                data.put("rpResult", jsonArray);//结算记录
                data.put("type",2);//状态
            }
        }
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/GetJieLongRedPackageResult";
    }
}
