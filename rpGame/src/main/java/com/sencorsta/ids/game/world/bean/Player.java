package com.sencorsta.ids.game.world.bean;

import com.sencorsta.ids.common.service.MsgService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.application.proxy.ProxyClient;
import com.sencorsta.ids.core.application.zone.ZoneUser;
import com.sencorsta.ids.core.configure.GlobalConfigure;
import com.sencorsta.ids.core.configure.SysConfig;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.game.world.room.Room;

import java.util.HashSet;
import java.util.Set;

import static com.sencorsta.ids.game.util.PushAndSaved.pushProto;

/**
 * @author TAO
 * @description: 玩家类
 * @date 2019/11/19 18:33
 */
public class Player extends ZoneUser implements RedisService {

    public int gameId;//游戏Id

    public Room room;//玩家所在额房间

    public int type;//1.普通用户  2.机器人

    public  String nickname;//昵称

    public  String portrait;//头像

    public long amount;//余额
    public long freeze;//余额

    public String SID;//SID

    private Set<String> rpSet=new HashSet<>();


    public Player(){}


    @Override
    public void subscribe(String SubscribeId) {
        ProxyClient.subscribe("proxy", SysConfig.getInstance().get("server.type"),SubscribeId,userId);
    }

    @Override
    public void unSubscribe(String SubscribeId) {
        ProxyClient.unSubscribe("proxy",SysConfig.getInstance().get("server.type"),SubscribeId,userId);
    }

    @Override
    public void sendMsg(RpcMessage msg) {
        MsgService.pushBySID(msg,SID);

        //MsgService.pushOnlyOnline(msg);
    }


    public Player(int type ,String nickname,String portrait,String userId,String SID){
        this.type=type;
        this.nickname=nickname;
        this.portrait=portrait;
        super.userId=userId;
        this.SID=SID;
    }


    public void onSettlement(String rpid,long changeMoney,long freezeMoney) {
        this.amount+=changeMoney;
        this.freeze+=freezeMoney;
        this.rpSet.remove(rpid);
        World.getInstance().checkUser(userId);
    }

    public void onSend(String rpid,long changeMoney,long freezeMoney) {
        this.amount+=changeMoney;
        this.freeze+=freezeMoney;
        this.rpSet.add(rpid);
    }

    public void onRob(String rpid,long changeMoney,long freezeMoney) {
        this.amount+=changeMoney;
        this.freeze+=freezeMoney;
        this.rpSet.add(rpid);
    }


    public int rpSetSize() {
       return rpSet.size();
    }
}
