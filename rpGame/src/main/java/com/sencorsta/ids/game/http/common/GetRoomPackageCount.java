package com.sencorsta.ids.game.http.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.world.bean.World;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 获取当前房间中正在结算的红包个数
* @author TAO
* @date 2020/1/6 5:31
*/
public class GetRoomPackageCount extends HttpHandler {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("----------------------获取当前房间中正在结算的红包个数-------GetRoomPackageCount----------------------");
        int gameId=params.getInteger("gameId");
        int roomId=params.getInteger("roomId");


        JSONObject data=new JSONObject();
        if (!World.getInstance().games.containsKey(gameId)){
            data.put("count",0);
        }
        if (!World.getInstance().games.get(gameId).rooms.containsKey(roomId)){
            data.put("count",0);
        }
        try{
            data.put("count",World.getInstance().games.get(gameId).rooms.get(roomId).rpPackage.keySet().size());
        }catch (Exception e){
            data.put("count",0);
        }
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/GetRoomPackageCount";
    }
}
