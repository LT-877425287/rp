package com.sencorsta.ids.game.openHandler;


import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.game.proto.GameJoinRoomByProto;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.room.Room;
import io.netty.channel.Channel;


/**
 * @author TAO
 * @description: 加入房间
 * @date 2019/11/22 14:00
 */

@ClientEvent("zero")
public class zeroHandler extends OpenMessageHandler implements RedisService {

    @Override
    public RpcMessage request(Channel channel, JSONObject json, String content, byte[] data, byte[] protobuf) {
        if (World.getInstance().allPlayers.keySet().contains(userId)){
            return response("0");
        }else {
            return response("-1");
        }
    }
}
