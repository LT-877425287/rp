package com.sencorsta.ids.game.openHandler;

import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.IdService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.entity.ClientEvent;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.game.db.RpMassage;
import com.sencorsta.ids.game.proto.GamePushRedPackageByProto;
import com.sencorsta.ids.game.proto.GameSendNiuNiuByProto;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.NiuNiuPackage;
import com.sencorsta.ids.game.world.room.NiuniuRoom;
import io.netty.channel.Channel;


/**
 * @author TAO
 * @description: 发牛牛玩法的包
 * @date 2019/11/14 19:43
 */

@ClientEvent("SendNiuNiu")
public class SendNiuNiu extends OpenMessageHandler implements RedisService {

    @Override
    public RpcMessage request(Channel channel, JSONObject jsonObject, String s, byte[] bytes, byte[] protobuf) {
        Out.debug("执行方法: ---->SendNiuNiu--------- 发牛牛玩法的包:" );
        GameSendNiuNiuByProto.SendNiuNiuRes.Builder res=GameSendNiuNiuByProto.SendNiuNiuRes.newBuilder();
        GamePushRedPackageByProto.PushRedPackage.Builder push = GamePushRedPackageByProto.PushRedPackage.newBuilder();

        try {
            GameSendNiuNiuByProto.SendNiuNiuReq req=GameSendNiuNiuByProto.SendNiuNiuReq.parseFrom(protobuf);

            int gameId = req.getGameId();//得到加入的房间类型
            int roomId =req.getRoomId();//得到加入的房间房间Id
            long sendAmount =req.getSendAmount();//得到金额
            int count =req.getCount();//发包数

            if (count <5||count>15) {
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.RED_PACKAGE_COUNT_ERROR));
                return responseProto(res);
            }

            //得到发包的房间
            NiuniuRoom roomNiuniu = (NiuniuRoom) World.getInstance().games.get(gameId).rooms.get(roomId);
            if (roomNiuniu == null) {
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.ROOM_IS_NULL));
                return responseProto(res);
            }

            if (roomNiuniu.status!=2){
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.ROOM_NO_START));
                return responseProto(res);
            }
            if (roomNiuniu.minAmount!=0&&roomNiuniu.minAmount!=0) {
                if (sendAmount < roomNiuniu.minAmount || sendAmount > roomNiuniu.maxAmount) {
                    res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.AMOUNT_IS_ERROR));
                    return responseProto(res);
                }
            }

            Player player=World.getInstance().allPlayers.get(userId);//得到发包玩家

            long sendDeposit = (long) roomNiuniu.computeSendDeposit(count, sendAmount);//得到在当前房间发包的押金
            Out.debug("发牛牛包的押金sendDeposit==>" + sendDeposit);


            if (player.amount < sendDeposit) {//余额不足
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.AMOUNT_NOT_ENOUGH));
                return responseProto(res);
            }

            String rpId = IdService.newRpId();//红包Id
            NiuNiuPackage niuNiuPackage = new NiuNiuPackage(userId, rpId, count, sendAmount, roomNiuniu,sendDeposit,player.type==2);//创建一个牛牛玩法的红包
            roomNiuniu.rpPackage.put(rpId, niuNiuPackage);//将牛牛房间的包容器中添加当前包
            niuNiuPackage.playerMap.put(userId,player);


            //走到这代表包已经发出=======================
            JSONObject recode=new JSONObject();
            recode.put("sendAmount",-sendAmount);
            niuNiuPackage.moneyRecode.put(userId,recode);

            player.onSend(rpId,-(sendAmount+sendDeposit),sendDeposit);

            push.setGameId(gameId);
            push.setRoomId(roomId);
            push.setRpId(rpId);

            //将红包记录插到MySql库里
            RpMassage.insertRpMessage2(rpId,roomId,gameId,1);//-------------------------------------------------------
            niuNiuPackage.rob2(player);//让发包者去抢
            RpcMessage pushMsg=pushProto("", "push.RedPackage", push);
            roomNiuniu.broadcast(pushMsg,"proxy");
            res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SUCCESS));
            return responseProto(res);
        }catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SERVER_ERROR));
            return responseProto(res);
        }

    }
}




