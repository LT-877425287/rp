package com.sencorsta.ids.game.openHandler;


import com.alibaba.fastjson.JSONObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.proto.utils.ReturnUtils;
import com.sencorsta.ids.common.service.IdService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.entity.ClientEvent;

import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.OpenMessageHandler;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.game.db.RpMassage;
import com.sencorsta.ids.game.proto.GamePushRedPackageByProto;
import com.sencorsta.ids.game.proto.GameSendSaoLeiByProto;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.SaoLeiPackage;
import com.sencorsta.ids.game.world.room.SaoLeiRoom;
import io.netty.channel.Channel;


/**
 * @author TAO
 * @description: 发扫雷玩法的包
 * @date 2019/11/21 14:51
 */
@ClientEvent("SendSaoLei")
public class SendSaoLei extends OpenMessageHandler implements RedisService {

    @Override
    public RpcMessage request(Channel channel, JSONObject jsonObject, String s, byte[] bytes, byte[] protobuf) {
        Out.debug("执行方法: ---->SendSaoLei--------- 发扫雷玩法的包:" );
        GameSendSaoLeiByProto.SendSaoLeiRes.Builder res=GameSendSaoLeiByProto.SendSaoLeiRes.newBuilder();
        GamePushRedPackageByProto.PushRedPackage.Builder push = GamePushRedPackageByProto.PushRedPackage.newBuilder();

        try {
            GameSendSaoLeiByProto.SendSaoLeiReq req=GameSendSaoLeiByProto.SendSaoLeiReq.parseFrom(protobuf);


            int gameId = req.getGameId();//得到加入的房间类型
            int roomId =req.getRoomId();//得到加入的房间房间Id
            long sendAmount =req.getSendAmount();//得到金额
            int count =req.getCount();//发包数
            count=7;
            int thunderNumber =req.getThunderNumber();//雷号

            SaoLeiRoom saoLeiRoom = (SaoLeiRoom) World.getInstance().games.get(gameId).rooms.get(roomId);
            if (saoLeiRoom == null) {
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.ROOM_IS_NULL));
                return responseProto(res);
            }

            if (saoLeiRoom.status!=2){
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.ROOM_NO_START));
                return responseProto(res);
            }

            if (saoLeiRoom.minAmount!=0&&saoLeiRoom.minAmount!=0) {
                if (sendAmount < saoLeiRoom.minAmount || sendAmount > saoLeiRoom.maxAmount) {
                    res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.AMOUNT_IS_ERROR));
                    return responseProto(res);
                }
            }

            Player player=World.getInstance().allPlayers.get(userId);//得到发包玩家

            //判断余额是否充足
            long sendDeposit = (long) saoLeiRoom.computeSendDeposit(count, sendAmount);//得到在当前房间发包的押金
            Out.debug("发扫雷包的押金sendDeposit==>" + sendDeposit);
            if (player.amount < sendDeposit) {//余额不足
                res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.AMOUNT_NOT_ENOUGH));
                return responseProto(res);
            }

            String rpId = IdService.newRpId();//红包Id
            SaoLeiPackage saoLeiPackage = new SaoLeiPackage(userId, rpId, count, sendAmount, saoLeiRoom, thunderNumber,sendDeposit,player.type==2);//创建一个扫雷玩法的红包
            saoLeiRoom.rpPackage.put(rpId, saoLeiPackage);//将扫雷房间的包容器中添加当前包
            saoLeiPackage.playerMap.put(userId,player);

            //走到这代表包已经发出=======================
            JSONObject recode=new JSONObject();
            recode.put("sendAmount",-sendAmount);
            saoLeiPackage.moneyRecode.put(userId,recode);

            player.onSend(rpId,-(sendAmount+sendDeposit),sendAmount);

            push.setGameId(gameId);
            push.setRoomId(roomId);
            push.setRpId(rpId);

            //将红包记录插到MySql库里
            RpMassage.insertRpMessage2(rpId,roomId,gameId,1);//-----------------------------------------------------------
            //找到房主--让其第一个抢包

            //房主抢---
            saoLeiPackage.rob(saoLeiRoom.main);
            RpcMessage pushMsg=pushProto("", "push.RedPackage", push);
            saoLeiRoom.broadcast(pushMsg,"proxy");
            res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SUCCESS));
            return responseProto(res);
        }catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            res.setMsg(ReturnUtils.ErrorProto(ErrorCodeList.SERVER_ERROR));
            return responseProto(res);
        }
    }
}
