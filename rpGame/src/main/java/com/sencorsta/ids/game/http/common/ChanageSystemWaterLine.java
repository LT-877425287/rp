package com.sencorsta.ids.game.http.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 改变系统水线
* @author TAO
* @date 2019/12/16 13:32
*/
public class ChanageSystemWaterLine extends HttpHandler implements RedisService {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("----------------改变系统水线---ChanageSystemWaterLine-------------------");
        long SYSWATERLINE;
        String roomId=params.getString("roomId");
        if (StringUtil.isEmpty(roomId)){
            return error(ErrorCodeList.SERVICECHARGE_IS_ERROR);
        }
        try {
            SYSWATERLINE=params.getLong("SYSWATERLINE");
        }catch (Exception e){
            return error(ErrorCodeList.SERVICECHARGE_IS_ERROR);
        }
        if (SYSWATERLINE==0){
            return error(ErrorCodeList.SYSTEMWATERLINE_IS_ERROR);
        }
        R_SYSTEM.set(WATERLINE+"-"+roomId, String.valueOf(SYSWATERLINE));

        JSONObject data=new JSONObject();
        data.put("SYSWATERLINE",SYSWATERLINE);
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/ChanageSystemWaterLine";
    }
}
