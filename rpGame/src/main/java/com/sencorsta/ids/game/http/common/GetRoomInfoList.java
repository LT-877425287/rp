package com.sencorsta.ids.game.http.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.room.Room;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
* @description: 得到同一个Game中的所有房间
* @author TAO
* @date 2019/11/19 15:17
*/
public class GetRoomInfoList extends HttpHandler {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("----------------获取Game中的房间集合-------GetRoomInfoList------------------");
        int gameId=params.getInteger("gameId");
        List<JSONObject> roomInfoList=new ArrayList<JSONObject>();//房间详情List

        Map<Integer, Room> rooms= World.getInstance().games.get(gameId).rooms;
        JSONObject roomInfo=null;
        for (var room:rooms.keySet()){
            roomInfo=new JSONObject();
            roomInfo.put("icon",rooms.get(room).icon);
            roomInfo.put("roomId",rooms.get(room).roomId);
            roomInfo.put("roomName",rooms.get(room).roomName);
            roomInfo.put("announcement",rooms.get(room).announcement);
            roomInfo.put("status",rooms.get(room).status);
            roomInfo.put("sequence",rooms.get(room).sequence);
            roomInfoList.add(roomInfo);
        }

        JSONObject data=new JSONObject();
        data.put("roomIdList",roomInfoList);

        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/GetRoomInfoList";
    }
}
