package com.sencorsta.ids.game.db;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.IdService;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.MysqlServiceS;
import com.sencorsta.ids.core.log.Out;

/**
 * @author TAO
 * @description: 交易流水
 * @date 2019/11/23 14:10
 */
public class TradeRecord {

    //drawWater：系统抽水
    //amount：实际金额
    //生成交易流水   // 1.发包扣钱  2.结算扣钱  3.加钱（不参与手续费） 4.加钱（参与手续费） 5.撤回
    public static void generateRecord(String userId, String rpId, long amount, long drawWater, int tradeWay, int gameId, int roomId) {
        String sql = "INSERT INTO `trade_record`(`recordId`,`userId`,`rpId`,`amount`,`drawWater`,`tradeWay`,`gameId`,`roomId`) VALUE(?,?,?,?,?,?,?,?)";
        Object[] args = new Object[]{
                IdService.newTraderId(), userId, rpId, amount, drawWater, tradeWay, gameId, roomId
        };
        MysqlService.getInstance().insertAsyn(sql, args);
    }

    //得到今天盈利
    public static long getProfit(String userId) {
        String sql = "select sum(`total`) amount from `settlement_result` where TO_DAYS(`createTime`) = TO_DAYS(NOW()) and `userId`=? ";
        Object[] args = new Object[]{
                userId
        };
        JSONArray result = MysqlServiceS.getInstance().select(sql, args);
        Out.debug("result==>", result);
        if (result.size() > 0) {
            JSONObject jsz = JSONObject.parseObject(String.valueOf(result.get(0)));
            if (jsz.keySet().size() > 0) {
                return jsz.getLong("amount");
            }
        }
        return 0;
    }

    public static long getRunWater(String userId){
        String sql="SELECT IFNULL(SUM(ABS(`total`)), 0) runWater  FROM `settlement_result` WHERE  TO_DAYS(`createTime`) = TO_DAYS(NOW()) AND `userId`="+userId;
        JSONArray result = MysqlServiceS.getInstance().select(sql);
        if (result.size() > 0) {
            JSONObject jsz = JSONObject.parseObject(String.valueOf(result.get(0)));
            if (jsz.keySet().size() > 0) {
                return jsz.getLong("runWater");
            }
        }
        return 0;


    }






}

