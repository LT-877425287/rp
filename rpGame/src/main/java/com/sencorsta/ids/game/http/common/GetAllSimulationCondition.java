package com.sencorsta.ids.game.http.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.bean.World;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.Map;
import java.util.Set;

/**
* @description: 获取机器人各游戏房间占比情况占比情况
* @author TAO
* @date 2019/12/6 14:35
*/
public class GetAllSimulationCondition extends HttpHandler {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-----------------------------------获取机器人各游戏房间占比情况占比情况----------------------------------------");

        int gameId=params.getInteger("gameId");
        int roomId=params.getInteger("roomId");

        Set<String> userList = World.getInstance().games.get(gameId).rooms.get(roomId).zoneUsers.keySet();
        Map<String, Player> allPlayers=World.getInstance().allPlayers;

        int simCount=0;
        int playCount=0;

        for (var user:userList){
            try {
                if (allPlayers.get(user).type==1){
                    playCount++;
                    continue;
                }
                simCount++;
            }catch (Exception e){
                continue;
            }
        }
        JSONObject data=new JSONObject();
        data.put("simCount",simCount);
        data.put("playCount",playCount);
        data.put("sumCount",userList.size());
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/GetAllSimulationCondition";
    }
}
