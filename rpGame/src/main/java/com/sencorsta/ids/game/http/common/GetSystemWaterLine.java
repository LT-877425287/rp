package com.sencorsta.ids.game.http.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.world.bean.World;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 获取系统设置水线
* @author TAO
* @date 2019/12/19 13:07
*/
public class GetSystemWaterLine extends HttpHandler {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("----------------获取系统设置水线------GetSystemWaterLine-------------");
        JSONObject data=new JSONObject();
        data.put("SYSWATERLINE",World.getInstance().SYSWATERLINE_ROOM);
        data.put("SYSWATERPOOL",World.getInstance().SYSWATERPOOL_ROOM);
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/GetSystemWaterLine";
    }
}
