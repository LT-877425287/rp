package com.sencorsta.ids.game.http.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.world.bean.World;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.Set;

/**
* @description: 获取房间中的用户集合
* @author TAO
* @date 2019/12/4 18:57
*/
public class GetRoomUserList extends HttpHandler {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-------------------------获取房间中的用户集合----GetRoomUserList-------------------------");
        int roomId=params.getInteger("roomId");
        int gameId=params.getInteger("gameId");

        Set<String> userList = World.getInstance().games.get(gameId).rooms.get(roomId).zoneUsers.keySet();
        JSONObject data=new JSONObject();
        data.put("userList",userList);
        data.put("size",userList.size());

        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/GetRoomUserList";
    }
}
