package com.sencorsta.ids.game.http.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.world.bean.World;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 获取系统手续费
* @author TAO
* @date 2019/12/16 13:37
*/
public class GetServiceCharge extends HttpHandler {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("----------------------GetServiceCharge----获取系统手续费---------------------");
        JSONObject data=new JSONObject();
        data.put("serviceCharge",World.getInstance().serviceCharge);
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/GetServiceCharge";
    }
}
