package com.sencorsta.ids.game.world.room;

import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.game.world.pak.NiuNiuPackage;
import com.sencorsta.ids.game.world.pak.RedPackage;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
* @description: 牛牛房间
* @author TAO
* @date 2019/11/19 10:31
*/
public class NiuniuRoom extends Room{

    public Map<Integer ,Double> magnification=new ConcurrentHashMap<>();//倍率


    public NiuniuRoom(String icon,int roomId,String roomName,String announcement,String notice,String regulation,String playingMethodDescribe,int type,String main,int isSys,long minAmount,long maxAmount,long sMinAmount,long sMaxAmount,int status){
        super(icon,roomId,roomName,announcement,notice,regulation,playingMethodDescribe,main,isSys,minAmount,maxAmount,sMinAmount,sMaxAmount,status);
        Map<Integer,Double> magnification=new ConcurrentHashMap<>();
        if (type==1){
            magnification=new ConcurrentHashMap<>();
        }else{
            magnification.put(1,1D);
            magnification.put(2,1D);
            magnification.put(3,1D);
            magnification.put(4,1D);
            magnification.put(5,1D);
            magnification.put(6,1D);
            magnification.put(7,1D);
            magnification.put(8,2D);
            magnification.put(9,2D);
            magnification.put(10,3D);
        }
        this.magnification=magnification;
    }

    //获取当前房间的最高倍率
    public double getMaxMagnification(){
        Map<Integer,Double> multiplyingPower=this.magnification;
        Out.debug("multiplyingPower==>"+multiplyingPower);
        if (multiplyingPower.keySet().size()!=0){//倍率存在
            Collection<Double> c =multiplyingPower.values();
            Object[] obj = c.toArray();
            Arrays.sort(obj);//排序顺序
            return Double.parseDouble(obj[obj.length-1].toString());
        }
        return 1;//没有倍率
    }

    //计算出当前房间发包金额的押金
    public double computeSendDeposit(int count,long amount){
        Out.debug("count==>"+count);
        Out.debug("amount==>"+amount);
        Out.debug("MaxMagnification==>"+getMaxMagnification());
        return (count-1)*amount*getMaxMagnification();
    }

    @Override
    public void update() {
        for (RedPackage niuniu : rpPackage.values()) {
            niuniu.update();
        }
    }

}
