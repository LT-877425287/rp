package com.sencorsta.ids.game.http.JieLong;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.util.GetRedPackageInfo;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.JieLongPackage;
import com.sencorsta.ids.game.world.pak.WelfarePackage;
import com.sencorsta.ids.game.world.room.JieLongRoom;
import com.sencorsta.ids.game.world.room.WelfareRoom;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;


/**
 * @author TAO
 * @description: 获取福利红包详情
 * @date 2019/11/17 17:21
 */
public class GetJieLongRedPackage extends HttpHandler {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("---------------------获取福利红包详情------GetWelfareRedPackage---------------------");
        int roomId=params.getInteger("roomId");
        String rpId=params.getString("rpId");

        JieLongRoom welfareRoom=(JieLongRoom) World.getInstance().games.get(World.JIELONG).rooms.get(roomId);//得到当前房间
        if (welfareRoom==null){
            return error(ErrorCodeList.ROOM_IS_NULL);
        }
        JieLongPackage welfarePackage= (JieLongPackage) welfareRoom.rpPackage.get(rpId);//得到内存中的扫雷红包
        JSONObject result;
        if (welfarePackage!=null){
            result= GetRedPackageInfo.GetJieLongPackage(rpId,welfareRoom,1);//状态 1.未领取  2.已结算
        }else{
            result=GetRedPackageInfo.GetJieLongPackage(rpId,welfareRoom,2);//状态 1.未领取  2.已结算
        }
        if (result==null){
            return error(ErrorCodeList.PACKET_IS_NULL);
        }
        return success(result);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Game/GetJieLongRedPackage";
    }
}
