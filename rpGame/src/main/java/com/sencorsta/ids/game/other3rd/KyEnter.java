package com.sencorsta.ids.game.other3rd;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.db.RpMassage;
import com.sencorsta.ids.game.db.SettlementResult;
import com.sencorsta.ids.game.db.Wallet;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.room.Room;
import com.sencorsta.utils.string.CodeUtil;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.io.IOException;

/**
* @description: 获取房间详情
* @author TAO
* @date 2019/11/19 15:01
*/
public class KyEnter extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        try {
            String token=params.getString("token");
            Out.info(token);
            if(StringUtil.isEmpty(token)){
                return error(ErrorCodeList.TOKEN_IS_NULL);
            }
            try {
                JSONObject jsonToken = JSON.parseObject(CodeUtil.decode(token));
                String UUID = jsonToken.getString("UUID");
                String userId = jsonToken.getString("userId");
                String ACCODE = jsonToken.getString("ACCODE");
                Out.info("userId:"+userId);
                //判断有没有这个人
                if (StringUtil.isEmpty(userId)) {
                    return error(ErrorCodeList.LOGIN_TOKEN_INVALID);
                }

                //判断token是否存在
                if (!R_SYSTEM.exists(TOKEN + ACCODE)) {
                    return error(ErrorCodeList.TOKEN_FAILURE);
                }

                String tempData = null;
                try {
                    tempData = CodeUtil.decode(ACCODE);
                } catch (IOException e) {
                    return error(ErrorCodeList.LOGIN_TOKEN_INVALID);
                }
                JSONObject deData = JSON.parseObject(tempData);

                if (!UUID.equals(deData.getString("UUID"))) {
                    return error(ErrorCodeList.UUID_IS_ERROR);
                }

                String account=userId;
                long money=0;

//                //查询玩家的钱
//                long kyMoney=KyService.getBalance(userId);

                //判断玩家在不在游戏中
                if(World.getInstance().allPlayers.containsKey(account)){
                    //在游戏中就把钱扣了 直接上分
                    Player player=World.getInstance().allPlayers.get(account);
                    money=player.amount;
                    player.amount=0;
                }else {
                    //玩家不在游戏中就查一下玩家的钱
                    JSONObject amountAndFreeze = Wallet.getAmountAndFreeze(userId);
                    long amount = amountAndFreeze.getLong("amount");
                    money=amount;
                }
                Wallet.addMoneySP(account,-money);
                String res=KyService.Login(account,money);
                if (res.equals("error")){
                    return error(500,"服务器异常");
                }
                return redirectGET(res);
            }catch (IOException e) {
                e.printStackTrace();
                return error(ErrorCodeList.LOGIN_TOKEN_INVALID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return error(500,"服务器异常");
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/3rd/KyEnter";
    }
}
