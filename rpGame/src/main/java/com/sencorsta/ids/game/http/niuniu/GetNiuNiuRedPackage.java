package com.sencorsta.ids.game.http.niuniu;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.util.GetRedPackageInfo;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.pak.NiuNiuPackage;
import com.sencorsta.ids.game.world.room.NiuniuRoom;


import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @author TAO
 * @description: 获取牛牛红包详情
 * @date 2019/11/17 17:21
 */
public class GetNiuNiuRedPackage extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("---------------------获取牛牛红包详情------GetNiuNiuRedPackage---------------------");

        int roomId=params.getInteger("roomId");
        String rpId=params.getString("rpId");

        NiuniuRoom roomNiuniu=(NiuniuRoom) World.getInstance().games.get(World.NIUNIU).rooms.get(roomId);//得到当前房间
        if (roomNiuniu==null){
            return error(ErrorCodeList.ROOM_IS_NULL);
        }

        NiuNiuPackage niuNiuPackage= (NiuNiuPackage) roomNiuniu.rpPackage.get(rpId);
        JSONObject result;
        if (niuNiuPackage!=null){
            result=GetRedPackageInfo.GetNiuNiuRedPackage(rpId,roomNiuniu,1);//状态 1.未领取  2.已结算
        }else{
            result=GetRedPackageInfo.GetNiuNiuRedPackage(rpId,roomNiuniu,2);//状态 1.未领取  2.已结算
        }

        if (result==null){
            return error(ErrorCodeList.PACKET_IS_NULL);
        }

        return success(result);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }


    @Override
    public String getPath() {
        return "/Game/GetNiuNiuRedPackage";
    }
}
