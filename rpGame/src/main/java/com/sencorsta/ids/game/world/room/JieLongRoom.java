package com.sencorsta.ids.game.world.room;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.IdService;
import com.sencorsta.ids.core.configure.TypeProtocol;
import com.sencorsta.ids.core.configure.TypeSerialize;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.game.db.RpMassage;
import com.sencorsta.ids.game.proto.GamePushRedPackageByProto;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.pak.JieLongPackage;
import com.sencorsta.ids.game.world.pak.NiuNiuPackage;
import com.sencorsta.ids.game.world.pak.RedPackage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
* @description: 接龙房间
* @author TAO
* @date 2019/11/19 10:31
*/
public class JieLongRoom extends Room{

    public long fixedAmount;
    public int fixedCount;

    public JieLongRoom(String icon, int roomId, String roomName, String announcement, String notice, String regulation, String playingMethodDescribe, int type, String main, int isSys, long minAmount, long maxAmount, long sMinAmount, long sMaxAmount, int status){
        super(icon,roomId,roomName,announcement,notice,regulation,playingMethodDescribe,main,isSys,minAmount,maxAmount,sMinAmount,sMaxAmount,status);
        this.main= new Player(Integer.parseInt(R_USER.hget(main, "type")),R_USER.hget(main, "nickname"),R_USER.hget(main, "portrait"),main,null);
        fixedAmount=20000;
        fixedCount=10;
    }

    public static void main(String[] args) {
        JSONObject json=new JSONObject();
        json.put("fixedAmount",20000);
        json.put("fixedCount",10);
        System.out.println(json);
    }

    //获取当前房间的最高倍率
    public double getMaxMagnification(){
        return 1;//没有倍率
    }

    //计算出当前房间发包金额的押金
    public double computeSendDeposit(int count,long amount){
        return amount*getMaxMagnification();
    }

    //计算出当前房间发包金额的押金
    public double computeRobDeposit(int count,long amount){
        return amount*getMaxMagnification();
    }

    @Override
    public void update() {
        //如果没有包了 房主自己发一个包
        if (rpPackage.keySet().size()==0&&isStop==false){
            //房主发包
            Player player=main;
            sendPack(player);
        }


        for (RedPackage niuniu : rpPackage.values()) {
            niuniu.update();
        }
    }

    public void sendPack(Player player){
        Out.debug("开始发接龙包。。");
        String rpId = IdService.newRpId();//红包Id
        String userId=player.userId;
        long sendAmount=fixedAmount;
        long sendDeposit=0;

        JieLongPackage jieLongPackage = new JieLongPackage(player.userId, rpId, fixedCount, sendAmount, this,sendDeposit,player.type==2);//创建一个牛牛玩法的红包
        rpPackage.put(rpId, jieLongPackage);//将牛牛房间的包容器中添加当前包
        jieLongPackage.playerMap.put(player.userId,player);
        //走到这代表包已经发出=======================

        JSONObject recode=new JSONObject();
        recode.put("sendAmount",-sendAmount);
        jieLongPackage.moneyRecode.put(userId,recode);
        player.onSend(rpId,-(sendAmount+sendDeposit),sendDeposit);

        GamePushRedPackageByProto.PushRedPackage.Builder push = GamePushRedPackageByProto.PushRedPackage.newBuilder();

        push.setGameId(gameId);
        push.setRoomId(roomId);
        push.setRpId(rpId);

        //将红包记录插到MySql库里
        RpMassage.insertRpMessage2(rpId,roomId,gameId,1);//-------------------------------------------------------
        RpcMessage pushMsg=pushProto("", "push.RedPackage", push);
        broadcast(pushMsg,"proxy");
    }

    public RpcMessage pushProto(String userId, String method, com.google.protobuf.GeneratedMessageV3.Builder res) {
        RpcMessage response=pushBytes(userId,method,protpToByte(res), TypeSerialize.TYPE_PROTOBUF);
        response.method=method;
        response.userId=userId;
        return response;
    }

    public RpcMessage pushBytes(String userId,String method,byte[] data, short typeSerialize) {
        RpcMessage puls=new RpcMessage();
        puls.header.type= TypeProtocol.TYPE_RPC_RES;
        puls.method=method;
        puls.serializeType= typeSerialize;
        puls.data=data;
        puls.userId=userId;
        return puls;
    }

    public byte[] protpToByte(com.google.protobuf.GeneratedMessageV3.Builder res) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            res.build().writeTo(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] byteArray = output.toByteArray();
        return byteArray;
    }
}
