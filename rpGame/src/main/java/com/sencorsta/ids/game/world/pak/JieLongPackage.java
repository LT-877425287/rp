package com.sencorsta.ids.game.world.pak;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.MsgService;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.configure.TypeProtocol;
import com.sencorsta.ids.core.configure.TypeSerialize;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import com.sencorsta.ids.game.db.RpMassage;
import com.sencorsta.ids.game.db.SettlementResult;
import com.sencorsta.ids.game.db.Wallet;
import com.sencorsta.ids.game.proto.GamePushRedPackageByProto;
import com.sencorsta.ids.game.proto.GamePushRobMoneyByProto;
import com.sencorsta.ids.game.util.PacketMoney;
import com.sencorsta.ids.game.util.PushAndSaved;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.ids.game.world.room.JieLongRoom;
import com.sencorsta.ids.game.world.room.NiuniuRoom;
import com.sencorsta.utils.date.DateUtil;
import com.sencorsta.utils.string.StringUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author TAO
 * @description: 牛牛玩法
 * @date 2019/11/14 19:40
 */
public class JieLongPackage extends RedPackage implements RedisService {

    public int bankerNiuNumber;//庄家点数

    public int victoryNumber = 0;//胜率


    public JieLongRoom jieLongRoom;


    public JieLongPackage(String sendUserId, String rpId, int count, long amount, JieLongRoom jieLongRoom, long sendDeposit, boolean isRobotPack) {
        super(sendUserId, rpId, count, amount, computeRobDeposit(amount, jieLongRoom), jieLongRoom.roomId, isRobotPack);//初始化父类公共属性
        this.moneyPieces = PacketMoney.getPacketMoney(PacketMoney.getRandomWeightsList(amount, count), amount, count, 1);
        this.sendDeposit = sendDeposit;//为发包者初始化冻结资金\
        this.jieLongRoom = jieLongRoom;
        analysis();
    }

    //得到抢红包的人的押金multiplyingPower
    public static long computeRobDeposit(long amount, JieLongRoom roomNiuniu) {
        double maxMagnification = roomNiuniu.getMaxMagnification();//最高倍率
        return (long) (amount * maxMagnification);//为押金赋值
    }

    //抢红包
    public void rob(Player player) {
        Out.debug("-----------接龙抢红包--update-----------");

        if (count == 0 || status != 0) {//判断红包数量
            Out.debug("红包已抢完");
            return;
        }

        if (rpResult.containsKey(player.userId)) {
            Out.debug("该红包你以领取过");
            return;
        }

        Long robAmount;//抢到的红包金额
        int niuNiuNumber;//牛数

        //不加控制默认
        int index = getNextIndex();
        if (isRobotRobMustWin) {
            if (player.type == 2) {
                if (count<=1){
                    //机器人不抢最后一个包
                    Out.debug("机器人不抢最后一个包");
                    return;
                }
                Out.debug("机器人拿最大的包");
                index = getMaxScore();
            } else {
                Out.debug("玩家拿最小的包");
                index = getMinScore();
            }
        }

        robAmount = moneyPieces.get(index).getMoney();//直接将剩余红包金额给最后一个人
        niuNiuNumber = moneyPieces.get(index).getNiuNumber();

        playerMap.put(player.userId, player);
        moneyPieces.get(index).setRobed(true);

        Out.debug("-----------接龙抢红包--index:" + (sumCount - count));
//        JSONObject rpResule = rpResult;//得到当前红包记录
        //将抢的红包和用户对应起来
        JSONObject result = new JSONObject();
        result.put("robUserId", player.userId);
        result.put("amount", robAmount);
        result.put("niuNumber", niuNiuNumber);
        result.put("time", DateUtil.getDateTime());
        result.put("nickname", player.nickname);
        result.put("portrait", player.portrait);
        result.put("index", sumCount - count);
        rpResult.put(player.userId, result);

        balance = balance - robAmount;

        player.onRob(rpId, -robDeposit, robDeposit);

        GamePushRobMoneyByProto.PushRobMoney.Builder push = GamePushRobMoneyByProto.PushRobMoney.newBuilder();
        push.setGameId(World.JIELONG);
        push.setRoomId(roomId);
        push.setRpId(rpId);
        push.setMoney(robAmount);
        Out.debug("接龙即将为==》", player.userId, "__推送__");
        //MsgService.pushOnlyOnline(PushAndSaved.pushProto(player.userId, "push.RobMoney", push));
        MsgService.pushBySID(PushAndSaved.pushProto(player.userId, "push.RobMoney", push), player.SID);

        if (!moneyRecode.containsKey(player.userId)) {//参与抢包用户添加recode的--防止报空
            JSONObject recode = new JSONObject();
            moneyRecode.put(player.userId, recode);
        }
        count = count - 1;

    }


    public void update() {
        //得到当前红包的数量
        this.time -= 1;//当前包的时间减1秒
        Out.debug("当前接龙红包Id==>", rpId, "__房间为==>", jieLongRoom.roomId, "  count==>", count, "  time==>", time);

        if (status == 0) {
            if (time == 0) {//当前包的死期已到，他妈的干掉
                if (rpResult == null) {//红包结算记录为空，代表
                    Out.debug("当前包无人问津，自动撤回");
                    status = 2;
                    Settlement();//结算
                    return;
                }
                Out.debug("当前包的死期已到，开始清算结果");
                status = 1;
                Settlement();//结算
                return;
            }

            if (count == 0) {//红包抢完了
                Out.debug("当前红包已抢完", "红包Id", rpId);
                status = 1;
                Settlement();//结算
                return;
            }
        }
    }

    @Override
    public void analysis() {
        //判断是否是机器人发包
        boolean isMustWin = World.getInstance().isMustWinbyRoom(roomId);
        isRobotRobMustWin = isMustWin;
    }

    //通知结算
    public void Settlement() {
        try {
            JieLongRoom jieLongRoom = (JieLongRoom) World.getInstance().games.get(World.JIELONG).rooms.get(roomId);//得到当前房间

            Out.debug("------------------------通知结算接龙包----rpResult----------------------------");
            Out.debug("接龙红包详情：", this.toString());
            Out.debug("接龙红包记录：", rpResult);
            String bankerUserId = sendUserId;//得到庄家

            JSONObject jsz = (JSONObject) rpResult.get(sendUserId);


            Player playerMin;
            long bankWinMoney = 0;
            for (var userId : rpResult.keySet()) {

                JSONObject userRobInfo = (JSONObject) rpResult.get(userId);//得到的userId的结算记录
                long leisureAmount = userRobInfo.getLong("amount");//得到当前用户抢到的钱

                JSONObject recod = moneyRecode.getJSONObject(userId);//得到当前玩家的账单记录
                recod.put("robMoney", leisureAmount);//将抢到的钱存到当前玩家的账单记录


            }
            moneyRecode.getJSONObject(sendUserId).put("sendAmountback", balance);//发包者剩余的钱
            moneyRecode.getJSONObject(sendUserId).put("resultMoney", bankWinMoney);
            JSONArray array = new JSONArray();
            for (String userId : moneyRecode.keySet()) {
                JSONObject resultItem = moneyRecode.getJSONObject(userId);
                long total = 0;
                total += resultItem.getLongValue("sendAmount") + resultItem.getLongValue("sendAmountback") + resultItem.getLongValue("robMoney") + resultItem.getLongValue("resultMoney");
                long drawWater = getWinAmount(total, playerMap.get(userId));//手续费
                if (moneyRecode.keySet().size() == 1 && userId.equals(sendUserId)) {
                    //只有发包者自己的话不算手续费
                    drawWater = 0;
                }
                resultItem.put("drawWater", drawWater);

                long totalFreeze = 0;
                long finalMoney = total - drawWater;

                if (userId.equals(sendUserId)) {
                    totalFreeze = sendDeposit + robDeposit;
                } else {
                    totalFreeze = robDeposit;
                }
                resultItem.put("total", finalMoney);

                playerMap.get(userId).onSettlement(rpId, finalMoney + totalFreeze, -totalFreeze);
                array.add(Wallet.addMoneySql(playerMap.get(userId), finalMoney, 0, roomId));
                array.add(SettlementResult.makeResult(this, resultItem, userId, World.JIELONG));//插入流水账单
            }
            array.add(RpMassage.insertRpMessage(rpId, roomId, World.JIELONG, 2));
            MysqlService.getInstance().executeMultipleAsyn(array);

            //Redis 红包详情入库
            joinRedis();

            GamePushRedPackageByProto.PushRedPackage.Builder push = GamePushRedPackageByProto.PushRedPackage.newBuilder();
            push.setGameId(World.JIELONG);
            push.setRoomId(roomId);
            push.setRpId(rpId);


            Out.debug("接龙红包详情：", this.toString());
            Out.debug("接龙红包记录：", rpResult);
            World.getInstance().games.get(World.JIELONG).rooms.get(roomId).broadcast(PushAndSaved.pushProto("", "push.RedPackage", push), "proxy");
            for (Player current : playerMap.values()) {
                MsgService.pushBySID(PushAndSaved.pushProto(current.userId, "push.RedPackageResult", push), current.SID);
            }

            //找到最小金额的人再发一个包
            Player loser = findLoser();
            if (loser != null) {
                jieLongRoom.sendPack(findLoser());
            }
            jieLongRoom.rpPackage.remove(rpId);//内存中移除红包

        } catch (Exception e) {
            Out.error(" 接龙结算报错");
            Out.error(this.toString());
            e.printStackTrace();
        }
    }

    //Redis 红包详情入库
    public void joinRedis() {
        //将红包记录插到MySql库里
        RpMassage.insertRpMessage(rpId, roomId, World.JIELONG, 2);
        Map<String, String> rpInfo = new HashMap<>();
        rpInfo.put("sendUserId", sendUserId);//发红包者
        rpInfo.put("gameId", String.valueOf(World.JIELONG));//房间Id
        rpInfo.put("roomId", String.valueOf(roomId));//房间Id
        rpInfo.put("RpId", rpId);//红包Id
        rpInfo.put("count", String.valueOf(count));//红包余数
        rpInfo.put("sumCount", String.valueOf(sumCount));//红包总个数
        rpInfo.put("amount", String.valueOf(amount));//红包金额
        rpInfo.put("balance", String.valueOf(balance));//红包余额
        rpInfo.put("moneyPieces", JSON.toJSONString(moneyPieces));//金额分配
        rpInfo.put("rpResult", String.valueOf(rpResult));//红包结果
        rpInfo.put("time", String.valueOf(time));//红包时间
        rpInfo.put("status", String.valueOf(status));//红包状态  0:发出 1:完成  2.撤回
        rpInfo.put("robDeposit", String.valueOf(robDeposit));//押金
        rpInfo.put("victoryNumber", String.valueOf(victoryNumber));//胜率
        rpInfo.put("bankerNiuNumber", String.valueOf(bankerNiuNumber));//庄家牛数
        R_REDPACKAGE_INFO.hmset(rpId, rpInfo);
    }

    public Player findLoser() {
        long score = Long.MAX_VALUE;
        String loseUserid = "";
        int loseIndex = 0;

        for (var userId : rpResult.keySet()) {

            JSONObject userRobInfo = (JSONObject) rpResult.get(userId);//得到的userId的结算记录
            long leisureAmount = userRobInfo.getLong("amount");//得到当前用户抢到的钱
            int robIndex = userRobInfo.getInteger("index");
            if (leisureAmount < score) {
                score = leisureAmount;
                loseUserid = userId;
            } else if (leisureAmount == score) {
                if (robIndex > loseIndex) {
                    score = leisureAmount;
                    loseUserid = userId;
                }
            }
        }
        if (StringUtil.isEmpty(loseUserid)) {
            return null;
        }
        return playerMap.get(loseUserid);
    }


}
