package com.sencorsta.ids.game.db;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.common.service.MysqlServiceS;

/**
* @description: 红包消息
* @author TAO
* @date 2019/11/22 16:55
*/
public class RpMassage {

    //获取红包消息列表  -20条
    public static JSONArray getRpIdList(int gameId,int roomId,int count){

        String sql="SELECT a.`rpId` rpId ,a.`status` status FROM (SELECT `rpId`,`status`,`create_time` FROM `rpMessage` WHERE `roomId`=? AND `gameId` =? ORDER BY `create_time` DESC LIMIT ?,10) a " +
                "ORDER BY a.`create_time`";
        Object[] args=new Object[]{
                roomId,gameId,(count-1)*10
        };
        return MysqlServiceS.getInstance().select(sql,args);
    }


    //插入红包记录
    public static JSONObject insertRpMessage(String rpId, int roomId, int gameId, int status){
        {//将红包记录插到MySql库里
            String sql = "INSERT INTO `rpMessage`(`rpId`,`roomId`,`gameId`,`status`) VALUE(?,?,?,?)";//发出去是为未领取
            Object[] args = new Object[]{
                    rpId, roomId, gameId, status
            };
            JSONObject res=new JSONObject();
            res.put("SQL",sql);
            res.put("args",args);
            return res;
        }
    }

    //插入红包记录
    public static void insertRpMessage2(String rpId, int roomId, int gameId, int status){
        {//将红包记录插到MySql库里
            String sql = "INSERT INTO `rpMessage`(`rpId`,`roomId`,`gameId`,`status`) VALUE(?,?,?,?)";//发出去是为未领取
            Object[] args = new Object[]{
                    rpId, roomId, gameId, status
            };
            MysqlService.getInstance().insertAsyn(sql, args);
        }
    }

    //修改红包记录状态
    public static JSONObject updateRpMessage(String rpId,int status){
        {//将红包记录的状态修改
            String sql = "update `rpMessage` set `status`=? where `rpId`=? and `status`=1";//发出去是为未领取
            Object[] args = new Object[]{
                    status,rpId
            };
            JSONObject res=new JSONObject();
            res.put("SQL",sql);
            res.put("args",args);
            return res;
        }
    }

    //修改红包记录状态
    public static void updateRpMessage2(String rpId,int status){
        {//将红包记录的状态修改
            String sql = "update `rpMessage` set `status`=? where `rpId`=?";//发出去是为未领取
            Object[] args = new Object[]{
                    status,rpId
            };
            MysqlService.getInstance().updateAsyn(sql, args);

        }


    }
}
