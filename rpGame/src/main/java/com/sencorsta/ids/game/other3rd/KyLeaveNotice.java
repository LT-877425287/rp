package com.sencorsta.ids.game.other3rd;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.game.db.Wallet;
import com.sencorsta.ids.game.world.bean.Player;
import com.sencorsta.ids.game.world.bean.World;
import com.sencorsta.utils.string.CodeUtil;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.io.IOException;

/**
* @description: 获取房间详情
* @author TAO
* @date 2019/11/19 15:01
*/
public class KyLeaveNotice extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        //agent=71765&timestamp=1579239020000&param=eD3OchYZWsblHXZFsXJLB1nQERcxeN895u0VXxE0Q5c%3D&key=f1404ac20ff2e1a007f8da52a1e852cd
        try {
            String agent=params.getString("agent");
            String timestamp=params.getString("timestamp");
            String param=params.getString("param");
            String key=params.getString("key");

            //先验证签名
            boolean sign=KyService.checkSign(key,timestamp);
            if (sign){
                //解密参数
                //（s=11&account=111111）
                JSONObject paramJ=KyService.decodeParam(param);
                String userId=paramJ.getString("account").replaceAll(agent+"_","");
                Out.info("userId:",userId);
                //查询玩家的钱
                long kyMoney=KyService.getBalance(userId);

                if(KyService.xiafen(userId,kyMoney)){
                    if(World.getInstance().allPlayers.containsKey(userId)){
                        //在游戏中就把钱扣了 直接上分
                        Player player=World.getInstance().allPlayers.get(userId);
                        player.amount+=kyMoney;
                    }
                    Wallet.addMoneySP(userId,kyMoney);
                    return "{\"s\":101,\"m\":\"/channelHandle\",\"d\":{\"code\":0}}";
                }else {
                    Out.warn("玩家下分失败 userId:"+userId+" money:"+kyMoney);
                }
            }else {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        return null;
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/3rd/KyLeaveNotice";
    }
}
