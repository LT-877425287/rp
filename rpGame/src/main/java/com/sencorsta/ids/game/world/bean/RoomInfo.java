package com.sencorsta.ids.game.world.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.MysqlService;

public class RoomInfo {
    int id=1; //id 用不到
    int gameId=1; //游戏id
    int roomId=1; //房间id
    int sequence=1; //房间序号 前端用
    long minAmount=300; //最小金额
    long maxAmount=3000;//最大金额
    double magnification=1.0;//房间倍率
    String main="1000000";//房主id
    String icon="ED7E52102BB5A1719E7943CB1A42CF51";//房间头像
    String roomName="3-30扫雷7包（不参与豹子顺子等奖励）";//房间名称
    String regulation="3元-30元固定发7包，1.6倍赔率，可发";//房间规则描述
    String playingMethodDescribe="扫雷群规1111111";//群规
    String announcement="每天08:00-24:00每个时间整点发放福利红包（当天发包抢包流水达到100元）";//说明
    String notice="此群红包抢到顺子 倒顺 豹子没有额外奖励的";//注意事项
    int type=1;//'其他配置 : 1 扫雷:1.没有特殊奖励 .2.有特殊奖励 2牛牛:1.不翻倍 2.翻倍 3 禁抢:1.不倍率 2.算倍率  4 龙虎:1.1倍 2.2倍 3.5倍'
    int isSys=0;//'房间类型 0系统  1玩家'
    int status=3;//'状态: 0.未审核 1 未通过  2开启  3冻结'
    long create_time=1575584396000l;
    long update_time=1575584396000l;

    long sMinAmount=300;// 机器人最小发包
    long sMaxAmount=3000;// 机器人最大发包


    public static void main(String[] args) {
        JSONObject test=JSON.parseObject("{\n" +
                "\t\"gameId\": 1,\n" +
                "\t\"minAmount\": 300,\n" +
                "\t\"magnification\": 1.0,\n" +
                "\t\"create_time\": 1575584396000,\n" +
                "\t\"icon\": \"ED7E52102BB5A1719E7943CB1A42CF51\",\n" +
                "\t\"main\": \"1000000\",\n" +
                "\t\"sMinAmount\": 300,\n" +
                "\t\"type\": 1,\n" +
                "\t\"roomId\": 1,\n" +
                "\t\"roomName\": \"3-30扫雷7包（不参与豹子顺子等奖励）\",\n" +
                "\t\"sequence\": 1,\n" +
                "\t\"update_time\": 1575584396000,\n" +
                "\t\"regulation\": \"3元-30元固定发7包，1.6倍赔率，可发\",\n" +
                "\t\"sMaxAmount\": 3000,\n" +
                "\t\"playingMethodDescribe\": \"扫雷群规1111111\",\n" +
                "\t\"id\": 1,\n" +
                "\t\"maxAmount\": 3000,\n" +
                "\t\"isSys\": 0,\n" +
                "\t\"announcement\": \"每天08:00-24:00每个时间整点发放福利红包（当天发包抢包流水达到100元）\",\n" +
                "\t\"notice\": \"此群红包抢到顺子 倒顺 豹子没有额外奖励的\",\n" +
                "\t\"status\": 3\n" +
                "}");
        RoomInfo info= test.toJavaObject(RoomInfo.class);


        System.out.println(info.gameId);
    }
}
