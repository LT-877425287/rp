package com.sencorsta.ids.game.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.utils.random.ICeRandom;
import com.sencorsta.utils.templet.Weights;

import java.util.*;

/**
 * @author TAO
 * @description: 生成红包金额
 * @date 2019/7/12 17:40
 */
public class PacketMoney {
    public static JSONArray getPacketMoney(long money, int size) {
        List<Integer> list = new ArrayList<>();
        for (int m = 0; m < size; m++) {
            //money越大贫富差距越大   越小贫富差距越小
            list.add(ICeRandom.getDynamic().nextInt((int) money) + 20);
        }
        Weights ww = new Weights(list);

        long count = 0;

        JSONArray listMoney = new JSONArray();
        for (int i = 0; i < size - 1; i++) {
            long myMoney = (money - size) * ww.getByIndex(i) / ww.getTotal() + 1;
            count += myMoney;
            listMoney.add(myMoney);
        }
        long myMoneyLast = money - count;
        listMoney.add(myMoneyLast);
        return listMoney;
    }


    public static Weights getRandomWeightsList(long money, int size) {
        List<Long> list = new ArrayList<>();
        for (int m = 0; m < size; m++) {
            //money越大贫富差距越小   越小贫富差距越大
            list.add(ICeRandom.getDynamic().nextLong(money*ICeRandom.getDynamic().nextLong(1,10)) + 1);
        }
        return new Weights(list);
    }

    public static List<MoneyPiece> getListAtboom(long money, int size, int boomCount, int boomValue, int type) {
        long avg = money / size;
        List<MoneyPiece> list = new ArrayList<>();
        int total = 0;
        int fixed = 0;
        for (int m = 0; m < size - 1; m++) {
            int temp;
            if (boomCount > 0) {
                temp = ICeRandom.getDynamic().nextInt((int) (fixed + avg - 10)) + 10;
                while (temp % 10 != boomValue) {
                    temp--;
                }
                list.add(new MoneyPiece(temp, type));
                boomCount--;
                total += temp;
                fixed += avg - temp;
            } else {
                temp = ICeRandom.getDynamic().nextInt((int) (fixed + avg)) + 1;
                total += temp;
                fixed += avg - temp;
                list.add(new MoneyPiece(temp, type));
            }
        }
        int finaltemp = (int) (money - total);
        list.add(new MoneyPiece(finaltemp, type));
        total += finaltemp;
        Collections.shuffle(list);
        return list;
    }

    public static List<MoneyPiece> getPacketMoney(Weights weights, long money, int size, int type) {
        long count = 0;
        List<MoneyPiece> listMoney = new ArrayList<>();
        MoneyPiece moneyPiece;
        for (int i = 0; i < size - 1; i++) {
            long myMoney = (money - size) * weights.getByIndex(i) / weights.getTotal() + 1;
            count += myMoney;
            moneyPiece = new MoneyPiece(myMoney, type);
            listMoney.add(moneyPiece);
        }
        long myMoneyLast = money - count;
        moneyPiece = new MoneyPiece(myMoneyLast, type);
        listMoney.add(moneyPiece);

        return listMoney;

    }

    public static List<MoneyPiece> getListAtboomMut(long money, int size, int boomCount, Set<Integer> boomValue, int type, int redType) {
        long avg = money / size;
        List<MoneyPiece> list = new ArrayList<>();

        int total = 0;
        int fixed = 0;
        for (int m = 0; m < size - 1; m++) {
            int temp;
            if (boomCount > 0) {
                temp = ICeRandom.getDynamic().nextInt((int) (fixed + avg - 10)) + 10;
                if (redType == 666) {
                    while (boomValue.contains(temp % 10)) {
                        temp--;
                    }
                } else {
                    while (!boomValue.contains(temp % 10)) {
                        boomValue.remove(temp % 10);
                        temp--;
                    }
                }
                list.add(new MoneyPiece(temp, type));
                boomCount--;
                total += temp;
                fixed += avg - temp;
            } else {
                temp = ICeRandom.getDynamic().nextInt((int) (fixed + avg)) + 1;
                total += temp;
                fixed += avg - temp;
                list.add(new MoneyPiece(temp, type));
            }
        }
        int finaltemp = (int) (money - total);
        list.add(new MoneyPiece(finaltemp, type));
        total += finaltemp;
        Collections.shuffle(list);
        return list;
    }
}






