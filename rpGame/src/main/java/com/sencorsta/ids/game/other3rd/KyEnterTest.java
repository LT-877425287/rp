//package com.sencorsta.ids.game.other3rd;
//
//import com.alibaba.fastjson.JSONObject;
//import com.sencorsta.ids.common.service.RedisService;
//import com.sencorsta.ids.core.tcp.http.HttpHandler;
//import io.netty.channel.ChannelHandlerContext;
//import io.netty.handler.codec.http.FullHttpRequest;
//
///**
//* @description: 获取房间详情
//* @author TAO
//* @date 2019/11/19 15:01
//*/
//public class KyEnterTest extends HttpHandler implements RedisService {
//    @Override
//    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
//        try {
//            //return KyService.Login();
//            //String token=params.getString("token");
//            String res=KyService.Login();
//            if (res.equals("error")){
//                return error(500,"服务器异常");
//            }
//            return redirectGET(res);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return error(500,"服务器异常");
//    }
//
//    @Override
//    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
//        return doPost(ctx, request, params);
//    }
//
//    @Override
//    public String getPath() {
//        return "/3rd/KyEnterTest";
//    }
//}
