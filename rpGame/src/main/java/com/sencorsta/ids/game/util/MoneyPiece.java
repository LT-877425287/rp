package com.sencorsta.ids.game.util;

public class MoneyPiece {
    long money=0;
    boolean isRobed=false;
    int type=0;
    int thunderNumber=0;
    int niuNumber=0;
    int score=0;

    boolean isSend=false;

    boolean isWin=false;

    public MoneyPiece(long money, int type) {
        this.money = money;
        this.type = type;
        score= (int) money;
        switch (type) {
            case 1:
                thunderNumber=getThunderNumber(money);
                break;
            case 2:
                niuNumber=getNiuNumber(money);
                if(niuNumber==0){
                    niuNumber=10;
                }
                score+=niuNumber*10000000;
                break;
        }
    }

    public boolean isSend() {
        return isSend;
    }

    public void setSend(boolean send) {
        isSend = send;
    }

    public boolean isWin() {
        return isWin;
    }

    public void setWin(boolean win) {
        isWin = win;
    }

    public long getMoney() {
        return money;
    }


    public boolean isRobed() {
        return isRobed;
    }

    public void setRobed(boolean robed) {
        isRobed = robed;
    }

    public int getType() {
        return type;
    }

    public int getThunderNumber() {
        return thunderNumber;
    }


    public int getNiuNumber() {
        return niuNumber;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    //得到金额对应的牛数

    public static int getNiuNumber(long money) {
        String moneyStr = String.valueOf(money);
        int length = moneyStr.length();
        switch (length) {
            case 1:
                moneyStr = "00" + moneyStr;//抢到的金额只有一位时向前补充两个0
                break;
            case 2:
                moneyStr = "0" + moneyStr;//抢到的金额只有一位时向前补充一个0
                break;
            case 3:
                moneyStr = moneyStr;//三位保持不变
                break;
            default:
                moneyStr = moneyStr.substring(length - 3);
                break;
        }

        Integer B = Integer.valueOf(moneyStr.substring(0, 1));
        Integer T = Integer.valueOf(moneyStr.substring(1, 2));
        Integer H = Integer.valueOf(moneyStr.substring(2));
        String SUM = String.valueOf(B + T + H);
        return Integer.parseInt(SUM.substring(SUM.length() - 1));
    }

    //得到金额对应的雷数

    public static int getThunderNumber(long money) {
        String moneyStr = String.valueOf(money);
        int length = moneyStr.length();
        return Integer.parseInt(moneyStr.substring(length - 1));
    }

}
