package com.sencorsta.ids.resource.http;


import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.configure.SysConfig;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.date.DateUtil;
import com.sencorsta.utils.string.Md5Tools;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.io.*;
import java.util.Arrays;

/**
* @description: 资源上传
* @author TAO
* @date 2019/6/20 10:35
*/
public class ResourcesUpload extends HttpHandler  {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("--------------------------资源上传ResourcesUpload-------------------------");
        String typey=params.getString("type");

        if (StringUtil.isEmpty(typey)){

        }

        int type=Integer.valueOf(typey);

        JSONObject fileJson=params.getJSONObject("file");


        if (fileJson==null){

        }

        //获取文件名
        String fileName=fileJson.getString("name");

        Out.debug(fileName+"-------------fileName");
        //文件后缀
        //String format=fileName.substring(fileName.lastIndexOf("."));

        byte[] by=fileJson.getBytes("data");
        Out.debug(fileName+"-------------fileName");
        try {
            String MD5FileName= Md5Tools.MD5(Arrays.toString(by));
            Out.debug("--------------------path"+MD5FileName);
            FileOutputStream fos =null;

            switch (type){
                case 1:
                    Out.debug("进入聊天头像");
                    String icon= "C:\\Users\\Administrator\\Desktop\\66\\";
                    fos=new FileOutputStream(new File(icon+MD5FileName+".jpg"));
                    break;
                case 2:
                    Out.debug("进入聊天图片");
                    String pictures=SysConfig.getInstance().get("R.Pictures");
                    String uploadpath= SysConfig.getInstance().get("R.uploadpath");
                    //1.存入Redis以Md5做key  文件名+后缀、上传日期
                    JSONObject fileInfo=new JSONObject();
                    fileInfo.put("fileName",fileName);
                    fileInfo.put("createTime", DateUtil.getDateTime());
                    fileInfo.put("url",uploadpath+MD5FileName+".jpg");
                    Out.debug(fileInfo);

                    //将手机用户存入REDIS


                    fos=new FileOutputStream(new File(pictures+MD5FileName+".jpg"));
                    break;
                case 1001:
                    Out.debug("OTC二维码");
                    String otcCode=SysConfig.getInstance().get("R.OtcCode");
                    fos=new FileOutputStream(new File(otcCode+MD5FileName+".jpg"));
                    break;
                case 10001:
                    Out.debug("开源码二维码");
                    String KyCode=SysConfig.getInstance().get("R.KyCode");
                    fos=new FileOutputStream(new File(KyCode+MD5FileName+".jpg"));
                    break;
                case 10002:
                    Out.debug("开源活动");
                    String KyActivity=SysConfig.getInstance().get("R.KyActivity");
                    fos=new FileOutputStream(new File(KyActivity+MD5FileName+".jpg"));
                    break;
            }
            fos.write(by,0,by.length  );
            fos.flush();
            fos.close();
            return success(MD5FileName);
        } catch (IOException e) {


        }
        return null;
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx,request,params);
    }

    public String getPath() {
        return "/idsResource/ResourcesUpload";
    }


}
