package com.sencorsta.ids.resource.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 资源下载
* @author TAO
* @date 2019/7/8 17:42
*/
public class ResourcesDownload extends HttpHandler  {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("--------------ResourcesDownload----拉取文件信息---------------");
        //获取MD5文件名
        String MD5FileName=params.getString("MD5FileName");

        Out.debug("----------MD5FileName----------"+MD5FileName);
        //MD5FileName非空
        if (StringUtil.isEmpty(MD5FileName)){
            //return error(ErrorCodeList.MD5FILENAME_IS_NULL);
        }

        //通过MD5FileName读取Redis中的文件信息
        //JSONObject jsonFile=JSON.parseObject(R_SYSTEM.hget(FILE,MD5FileName));

        //判断文件非空
       /* if (jsonFile==null){
            return error(ErrorCodeList.FILE_IS_NULL);
        }
        return success(jsonFile);*/
       return null;
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    public String getPath() {
        return "/idsResource/ResourcesDownload";
    }
}
