package com.sencorsta.ids.resource;


import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

public class HelloWorld extends HttpHandler {


	@Override
	public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
		return "HelloWorld.....当前imResource资源的http服务器运行正常";
	}

	@Override
	public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
		return doPost(ctx,request,params);
	}

	public String getPath() {
		return "/HelloWorld";
	}

}
