package com.sencorsta.ids.robot.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.robot.userSimulation.SimulationCenter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.Map;

/**
* @description: 改变发包金额大小
* @author TAO
* @date 2019/12/20 17:42
*/
public class ChangeSendAmount extends HttpHandler {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("------------------改变发包金额大小--------ChangeSendAmount----------------");
        int SEND_MIN_AMOUNT=params.getInteger("SEND_MIN_AMOUNT");
        if (SEND_MIN_AMOUNT<5){
            SEND_MIN_AMOUNT=5;
        }
        int SEND_MAX_AMOUNT=params.getInteger("SEND_MAX_AMOUNT");
        if (SEND_MAX_AMOUNT<SEND_MIN_AMOUNT){
            return error(ErrorCodeList.MIN_MAX_IS_ERROR);
        }

        SimulationCenter.SEND_MIN_AMOUNT=SEND_MIN_AMOUNT;
        SimulationCenter.SEND_MAX_AMOUNT=SEND_MAX_AMOUNT;

        JSONObject data=new JSONObject();
        data.put("SEND_MAX_AMOUNT", SEND_MAX_AMOUNT);
        data.put("SEND_MIN_AMOUNT", SEND_MIN_AMOUNT);
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Robot/ChangeSendAmount";
    }
}
