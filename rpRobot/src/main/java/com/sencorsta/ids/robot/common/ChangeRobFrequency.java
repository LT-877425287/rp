package com.sencorsta.ids.robot.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.robot.userSimulation.SimulationCenter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 改变抢包频率
* @author TAO
* @date 2019/12/20 17:28
*/
public class ChangeRobFrequency extends HttpHandler {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("------------------改变抢包频率---------ChangeRobFrequency-----------------");
        int ROB_INTERVAL=params.getInteger("ROB_INTERVAL");
        if (ROB_INTERVAL==0){
            ROB_INTERVAL=1;
        }
        SimulationCenter.ROB_INTERVAL=ROB_INTERVAL;
        JSONObject data=new JSONObject();
        data.put("ROB_INTERVAL",ROB_INTERVAL);
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Robot/ChangeRobFrequency";
    }
}
