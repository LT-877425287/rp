package com.sencorsta.ids.robot.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.robot.userSimulation.GameSimUser;
import com.sencorsta.ids.robot.userSimulation.SimulationCenter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 获取机器人配置详情
* @author TAO
* @date 2019/12/20 17:49
*/
public class GetRoboConftInfo extends HttpHandler {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("-----------------获取机器人配置详情-----GetRoboConftInfo-------------------");
        int activeCount=0;//先查找当前已经激活的机器人
        int inRoomCount=0;//先查找当前已经在房间的机器人
        int onlineCount=0;//先查找当前已经在房间的机器人
        for (var simulation: SimulationCenter.simulationUserMap.keySet()){
            GameSimUser gameSimUser=(GameSimUser)SimulationCenter.simulationUserMap.get(simulation);
            if (gameSimUser.isActive){
                activeCount++;
            }
            if (gameSimUser.isInRoom){
                inRoomCount++;
            }
            if (gameSimUser.isOnline){
                onlineCount++;
            }
        }


        JSONObject data=new JSONObject();
        data.put("SEND_MAX_AMOUNT", SimulationCenter.SEND_MAX_AMOUNT);
        data.put("SEND_MIN_AMOUNT", SimulationCenter.SEND_MIN_AMOUNT);
        data.put("IS_SEND", SimulationCenter.IS_SEND);
        data.put("IS_ROB", SimulationCenter.IS_ROB);
        data.put("SEND_INTERVAL", SimulationCenter.SEND_INTERVAL);
        data.put("ROB_INTERVAL", SimulationCenter.ROB_INTERVAL);
        data.put("START_SIMULATION", activeCount);
        data.put("ACTIVATION_SIMULATION", inRoomCount);
        data.put("ONLINE_SIMULATION", onlineCount);
        data.put("TOTAL_SIMULATION", SimulationCenter.SIMCOUNT);

        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Robot/GetRoboConfInfo";
    }
}
