package com.sencorsta.ids.robot.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.utils.random.ICeRandom;
import com.sencorsta.utils.templet.Weights;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TAO
 * @description: 生成红包金额
 * @date 2019/7/12 17:40
 */
public class PacketMoney {
    public static JSONArray getPacketMoney(long money, int size) {
        List<Integer> list = new ArrayList<>();
        for (int m = 0; m < size; m++) {
            //money越大贫富差距越大   越小贫富差距越小
            list.add(ICeRandom.getDynamic().nextInt((int) money) + 20);
        }
        Weights ww = new Weights(list);

        long count = 0;

        JSONArray listMoney = new JSONArray();
        for (int i = 0; i < size - 1; i++) {
            long myMoney = (money - size) * ww.getByIndex(i) / ww.getTotal() + 1;
            count += myMoney;
            listMoney.add(myMoney);
        }
        long myMoneyLast = money - count;
        listMoney.add(myMoneyLast);
        return listMoney;
    }





    public static List<JSONObject> getPacketMoney(long money, int size,int type) {
        List<Integer> list = new ArrayList<>();
        for (int m = 0; m < size; m++) {
                                                //money越大贫富差距越小   越小贫富差距越大
            list.add(ICeRandom.getDynamic().nextInt((int) money) + 1);
        }
        Weights ww = new Weights(list);

        long count = 0;

        List<JSONObject> listMoney = new ArrayList<>();
        JSONObject MoneyAndNiuNumber;
        for (int i = 0; i < size - 1; i++) {
            MoneyAndNiuNumber = new JSONObject();
            long myMoney = (money - size) * ww.getByIndex(i) / ww.getTotal() + 1;
            count += myMoney;
            MoneyAndNiuNumber.put("money", myMoney);
            switch (type){
                case 1:
                    MoneyAndNiuNumber.put("thunderNumber", getThunderNumber(myMoney));
                    break;
                case 2:
                    MoneyAndNiuNumber.put("niuNumber", getNiuNumber(myMoney));
                    break;
            }
            listMoney.add(MoneyAndNiuNumber);
        }
        long myMoneyLast = money - count;
        MoneyAndNiuNumber = new JSONObject();
        MoneyAndNiuNumber.put("money", myMoneyLast);
        switch (type){
            case 1:
                MoneyAndNiuNumber.put("thunderNumber", getThunderNumber(myMoneyLast));
                break;
            case 2:
                MoneyAndNiuNumber.put("niuNumber", getNiuNumber(myMoneyLast));
                break;

        }

        listMoney.add(MoneyAndNiuNumber);



        //TODO 模拟测试数据


        //0 1 2 3 4 6 9
        return listMoney;

    }


    //得到金额对应的牛数
    public static int getNiuNumber(long money) {
        String moneyStr = String.valueOf(money);
        int length = moneyStr.length();
        switch (length) {
            case 1:
                moneyStr = "00" + moneyStr;//抢到的金额只有一位时向前补充两个0
                break;
            case 2:
                moneyStr = "0" + moneyStr;//抢到的金额只有一位时向前补充一个0
                break;
            case 3:
                moneyStr = moneyStr;//三位保持不变
                break;
            default:
                moneyStr = moneyStr.substring(length - 3);
                break;
        }

        Integer B = Integer.valueOf(moneyStr.substring(0, 1));
        Integer T = Integer.valueOf(moneyStr.substring(1, 2));
        Integer H = Integer.valueOf(moneyStr.substring(2));
        String SUM = String.valueOf(B + T + H);
        return Integer.parseInt(SUM.substring(SUM.length() - 1));
    }
    public static void main(String[] args) {
        List<JSONObject> jsz = PacketMoney.getPacketMoney(20000, 7,1);
        System.out.println(jsz);
    }

    //得到金额对应的雷数
    public static int getThunderNumber(long money) {
        String moneyStr = String.valueOf(money);
        int length = moneyStr.length();
        return Integer.parseInt(moneyStr.substring(length-1));
    }

}






