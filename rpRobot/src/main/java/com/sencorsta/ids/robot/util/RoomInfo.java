package com.sencorsta.ids.robot.util;

/**
* @description: 红包类
* @author TAO
* @date 2019/12/16 22:24
*/
public class RoomInfo {
    public int gameId;
    public int roomId;
    public long time;

    public RoomInfo(int gameId, int roomId, long time){
        this.gameId = gameId;
        this.roomId = roomId;
        this.time=time;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

}
