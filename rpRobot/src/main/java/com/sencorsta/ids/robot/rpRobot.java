package com.sencorsta.ids.robot;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.core.application.Application;
import com.sencorsta.ids.core.data.redis.RedisProvider;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.robot.userSimulation.SimulationCenter;
import com.sencorsta.ids.robot.util.ProcessService;
import redis.clients.jedis.Jedis;

import java.util.Set;


public class rpRobot extends Application {

    static {
        instance = new rpRobot();
    }

    public static rpRobot getInstance() {
        return (rpRobot) instance;
    }


    @Override
    public void onStarted(){
        MysqlService.getInstance().waitUtilLoad();
        //clean();
        SimulationCenter sc=new SimulationCenter();
        sc.init();
        ProcessService.newZone(sc);
    }

    public void clean(){

        Out.warn("开始数据清理。。。");

        JSONArray array = new JSONArray();
        {
            JSONObject res=new JSONObject();
            res.put("SQL","DELETE FROM `settlement_result`");
            res.put("args",new Object[0]);
            array.add(res);
        }
        {
            JSONObject res=new JSONObject();
            res.put("SQL","DELETE FROM `rpMessage`");
            res.put("args",new Object[0]);
            array.add(res);
        }
        {
            JSONObject res=new JSONObject();
            res.put("SQL","UPDATE `wallet` SET `amount`=10000000 ,`freeze`=0");
            res.put("args",new Object[0]);
            array.add(res);
        }
        {
            JSONObject res=new JSONObject();
            res.put("SQL","UPDATE `wallet` SET `amount`=0 WHERE `userId`=1000000");
            res.put("args",new Object[0]);
            array.add(res);
        }
        MysqlService.getInstance().executeMultipleAsyn(array);
        Jedis A = RedisProvider.getJedis("total");
        A.select(8);
        A.flushDB();

        A.select(0);


        for (int i = 1; i < 14; i++) {
            A.set("waterLine-"+i,"1000000");
            A.set("waterPool-"+i,"0");
        }

        A.select(1);
        Set<String> keys = A.keys("*");
        for (String key:keys
        ) {
            try {
                if (Long.parseLong(key)>=90000000000l&&Long.parseLong(key)<90000000099l){
                    if(Long.parseLong(key)%2==0) {
                        A.hset(key, "type", "1");
                    }else {
                        A.hset(key, "type", "2");
                    }
                }
            }catch (Exception e){
                System.err.println(e.getMessage());
            }
        }

        Out.warn("数据清理完毕！！");

    }




}
