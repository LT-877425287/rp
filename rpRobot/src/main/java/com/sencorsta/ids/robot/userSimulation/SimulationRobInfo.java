package com.sencorsta.ids.robot.userSimulation;


import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 机器人加入-指定游戏房间
* @author TAO
* @date 2019/12/5 20:50
*/
public class SimulationRobInfo extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("--------------------机器人激活-----SimulationActiveRob--------------------");

        int activeCount=0;//先查找当前已经激活的机器人
        int inRoomCount=0;//先查找当前已经在房间的机器人
        int onlineCount=0;//先查找当前已经在房间的机器人
        for (var simulation: SimulationCenter.simulationUserMap.keySet()){
            GameSimUser gameSimUser=(GameSimUser)SimulationCenter.simulationUserMap.get(simulation);
            if (gameSimUser.isActive){
                activeCount++;
            }
            if (gameSimUser.isInRoom){
                inRoomCount++;
            }
            if (gameSimUser.isOnline){
                onlineCount++;
            }
        }
        Out.info("onlineCount:",onlineCount,"activeCount:",activeCount,"inRoomCount:",inRoomCount);

        JSONObject res=new JSONObject();
        res.put("activeCount",activeCount);
        res.put("onlineCount",onlineCount);
        res.put("inRoomCount",inRoomCount);
        return success(res);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Robot/SimulationRobInfo";
    }
}
