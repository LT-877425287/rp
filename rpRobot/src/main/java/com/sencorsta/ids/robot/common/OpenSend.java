package com.sencorsta.ids.robot.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.robot.userSimulation.SimulationCenter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 开启机器人发包
* @author TAO
* @date 2019/12/20 17:11
*/
public class OpenSend extends HttpHandler {

    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("----------------------开启机器人发包-----OpenSend------------------");
        SimulationCenter.IS_SEND=true;
        JSONObject data=new JSONObject();
        data.put("IS_SEND",SimulationCenter.IS_SEND);
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Robot/OpenSend";
    }
}
