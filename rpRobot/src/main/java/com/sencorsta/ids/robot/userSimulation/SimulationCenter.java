package com.sencorsta.ids.robot.userSimulation;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.service.MysqlService;
import com.sencorsta.ids.core.application.zone.Zone;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.opensocket.client.OpenClientBootstrap;
import com.sencorsta.ids.core.tcp.opensocket.client.SimulationUser;
import com.sencorsta.ids.robot.util.RedPackageInfo;
import com.sencorsta.ids.robot.util.RoomInfo;


import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class SimulationCenter extends Zone {

    public static Map<String, RedPackageInfo> redPackage = new ConcurrentHashMap<>();

    public static Map<String, RoomInfo> roomInfo = new ConcurrentHashMap<>();

    public static Map<String, SimulationUser> simulationUserMap = new ConcurrentHashMap<String, SimulationUser>();

    public static Map<String, SimulationUser> activateSimulationUserMap = new ConcurrentHashMap<String, SimulationUser>();

    public static OpenClientBootstrap openClientBootstrap;

    public static int SEND_MAX_AMOUNT = 30;//机器人发包金额上限

    public static int SEND_MIN_AMOUNT = 5;//机器人发包金额下限

    public static boolean IS_SEND = false;//机器人是否发包

    public static boolean IS_ROB = false;//机器人是否抢包

    public static int SEND_INTERVAL = 120;//周期内发包频率

    public static int ROB_INTERVAL = 10;//周期内抢包频率

    public static int SIMCOUNT=2000;


    //初始化机器人系统机器人个数
    public void init() {
        openClientBootstrap = new OpenClientBootstrap();
        String sql = "SELECT `phone`,`phone_password` FROM `account` WHERE `type`=2 LIMIT 0,"+SIMCOUNT;
        JSONArray result = MysqlService.getInstance().select(sql);
        for (int i = 0; i < result.size(); i++) {
            GameSimUser user = new GameSimUser();
            user.phone = JSONObject.parseObject(String.valueOf(result.get(i))).getString("phone");
            user.userId = user.phone;
            user.password=user.phone;
            simulationUserMap.put(user.userId, user);
        }
        onStarted();

        Out.debug("机器人模块 启动。。");
    }

    @Override
    public void update() {
        for (SimulationUser user : simulationUserMap.values()) {
            user.onUpdate();
        }

        if (IS_ROB) {
            for (RedPackageInfo rp : redPackage.values()) {
                if (rp.unUsed) {
                    continue;
                }
                if (new Date().getTime() > rp.time) {
                    rp.setTime(new Date().getTime() + new Random().nextInt(ROB_INTERVAL * 1000));
//                    rp.setTime(new Date().getTime() + new Random().nextInt(50));

                    List<GameSimUser> gameSimUserList = new ArrayList<>();
                    for (SimulationUser simulationUser : activateSimulationUserMap.values()) {
                        if (rp.gameId == ((GameSimUser) simulationUser).gameId && rp.roomId == ((GameSimUser) simulationUser).roomId) {
                            gameSimUserList.add((GameSimUser) simulationUser);
                        }
                    }
                    if (gameSimUserList.size() == 0) {
                        Out.debug("------没有合适的抢包机器人------", "游戏id为==>", rp.gameId, "__房间==>", rp.roomId, "__红包Id==》", rp.rpId);
                        continue;
                    }
                    Collections.shuffle(gameSimUserList);
                    gameSimUserList.get(0).simulationUserRob(rp.gameId, rp.roomId, rp.rpId);
                }
            }
            LinkedList<String> list=new LinkedList<>();
            for (RedPackageInfo rp : redPackage.values()) {
                if (rp.unUsed) {
                    list.add(rp.rpId);
                }
            }
            for (String id:list) {
                Out.debug("移除红包", "红包Id为==》", id);
                redPackage.remove(id);
            }
        }


        //发包
        if (IS_SEND) {
            for (RoomInfo roomInfo : roomInfo.values()) {
                if (new Date().getTime() > roomInfo.time) {
                    roomInfo.setTime(new Date().getTime() + new Random().nextInt(SEND_INTERVAL * 1000));
//                    roomInfo.setTime(new Date().getTime() + new Random().nextInt(50));
                    List<GameSimUser> gameSimUserList = new ArrayList<>();
                    for (SimulationUser simulationUser : activateSimulationUserMap.values()) {
                        if (roomInfo.gameId == ((GameSimUser) simulationUser).gameId && roomInfo.roomId == ((GameSimUser) simulationUser).roomId) {
                            gameSimUserList.add((GameSimUser) simulationUser);
                        }
                    }
                    if (gameSimUserList.size() == 0) {
                        Out.debug("------没有合适的抢包机器人------", "游戏id为==>", roomInfo.gameId, "__房间==>", roomInfo.roomId, "__红包Id==》");
                        continue;
                    }
                    Collections.shuffle(gameSimUserList);
                    gameSimUserList.get(0).sendPackageAll();
                }
            }
        }

    }


    //每个机器人初始化完了做的第一件事情（每个机器人只做一次）
    public void onStarted() {
//        for (SimulationUser user : simulationUserMap.values()) {
//            user.onStarted();
//        }
    }

    //产生最大到10的包数
    //TODO 这里有问题
    public static int randomCount(int min) {
        Random random = new Random();
        return random.nextInt(5) + min + 1;
    }


    //产生1-100随机概率
    public static int randomProbability() {
        Random random = new Random();
        return random.nextInt(100) + 1;
    }

    //产生机器人发包随机金额
    public static int randomAmount() {
        Random random = new Random();
        return (random.nextInt(SEND_MAX_AMOUNT - SEND_MIN_AMOUNT) + SEND_MIN_AMOUNT) * 100;
    }




    //产生机器人发包随机金额
    public static long sRandomAmount(long min,long max) {
        Random random = new Random();
        if (min==max){//防止操作人员设置固定发包金额
            return min/100*100;
        }
        return ((random.nextInt((int) (max - min)) + min)/100*100);
    }


    public static void main(String[] args) {
        System.out.println(3125/100);

    }


    //产生机器人发包随机雷号--单
    public static int randomOneThunderNumber() {
        Random random = new Random();
        return random.nextInt(10);
    }


    //得到雷列表  最少雷数  最大雷数
    public static Set<Integer> randomThunderNumber(int min, int max) {
        List<Integer> allNumer = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            allNumer.add(i);
        }
        Collections.shuffle(allNumer);
        Random random = new Random();
        Set<Integer> set = new HashSet<>();
        int finalCount = random.nextInt(max - min + 1) + min;

        for (int i = 0; i < finalCount; i++) {
            set.add(allNumer.get(i));
        }
        return set;

    }


    //产生机器人发包随机雷号--多
    public static List<Integer> randomMoreThunderNumber(int count, int redType) {
        Set<Integer> jsz = new HashSet<>();
        if (redType == 0) {//连环雷---单雷
            if (randomProbability() < 40 && count != 10) {
                jsz.add(randomOneThunderNumber());
            } else {
                if (count == 10) {
                    jsz = randomThunderNumber(3, 8);
                } else if (count == 9 || count == 7) {
                    jsz = randomThunderNumber(2, 7);
                } else if (count == 6) {
                    jsz = randomThunderNumber(2, 6);
                } else if (count == 5) {
                    jsz = randomThunderNumber(2, 5);
                }
            }
        } else {//六不中
            jsz = randomThunderNumber(2, 5);
        }
        List<Integer> r = new ArrayList<>();
        for (var i : jsz) {
            r.add(i);
        }
        return r;

    }

    //指定随机数范围
    public static int randomAssign(int min, int max) {
        Random random = new Random();
        return random.nextInt(max) + min;
    }


}



