package com.sencorsta.ids.robot.common;

import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import com.sencorsta.ids.robot.userSimulation.SimulationCenter;
import com.sencorsta.utils.string.StringUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
* @description: 改变发包频率
* @author TAO
* @date 2019/12/20 17:28
*/
public class ChangeSendFrequency extends HttpHandler {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("------------------改变发包频率---------ChangeSendFrequency-----------------");
        int SEND_INTERVAL=params.getInteger("SEND_INTERVAL");
        if (SEND_INTERVAL==0){
            SEND_INTERVAL=1;
        }
        SimulationCenter.SEND_INTERVAL=SEND_INTERVAL;
        JSONObject data=new JSONObject();
        data.put("SEND_INTERVAL",SEND_INTERVAL);
        return success(data);
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Robot/ChangeSendFrequency";
    }
}
