package com.sencorsta.ids.robot.util;

import com.google.protobuf.GeneratedMessageV3;
import com.sencorsta.ids.core.tcp.socket.protocol.RpcMessage;
import io.netty.channel.Channel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
* @description: 推送数据转换
* @author TAO
* @date 2019/11/23 13:21
*/
public class PushAndSaved {

    public static byte[] protpToByte(GeneratedMessageV3.Builder res) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            res.build().writeTo(output);
        } catch (IOException var4) {
            var4.printStackTrace();
        }

        byte[] byteArray = output.toByteArray();
        return byteArray;
    }
    private static Channel channel;
    public static RpcMessage pushBytes(String userId, String method, byte[] data, short typeSerialize) {
        RpcMessage puls = new RpcMessage();
        puls.header.type = 2;
        puls.channel = channel;
        puls.method = method;
        puls.serializeType = typeSerialize;
        puls.data = data;
        puls.userId = userId;
        return puls;
    }
    public static RpcMessage pushProto(String userId, String method, GeneratedMessageV3.Builder res) {
        RpcMessage response = pushBytes(userId, method, protpToByte(res), (short)2);
        response.method = method;
        response.userId = userId;
        return response;
    }
}
