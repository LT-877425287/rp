package com.sencorsta.ids.robot.userSimulation;


import com.alibaba.fastjson.JSONObject;
import com.sencorsta.ids.common.information.ErrorCodeList;
import com.sencorsta.ids.common.service.RedisService;
import com.sencorsta.ids.core.function.FunctionSystem;
import com.sencorsta.ids.core.log.Out;
import com.sencorsta.ids.core.tcp.http.HttpHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
* @description: 机器人加入-指定游戏房间
* @author TAO
* @date 2019/12/5 20:50
*/
public class SimulationActiveRob extends HttpHandler implements RedisService {
    @Override
    public String doPost(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        Out.debug("--------------------机器人激活-----SimulationActiveRob--------------------");

        int count=params.getInteger("count");
        //判断count是否大于最大值
        if (count>SimulationCenter.SIMCOUNT||count<0){
            return error(ErrorCodeList.USABLE_SIMULATION_INSUFFICIENT);
        }
        new Thread(()->{
            int activeCount=0;//先查找当前已经激活的机器人
            int inRoomCount=0;//先查找当前已经在房间的机器人
            int onlineCount=0;//先查找当前已经在房间的机器人
            for (var simulation: SimulationCenter.simulationUserMap.keySet()){
                GameSimUser gameSimUser=(GameSimUser)SimulationCenter.simulationUserMap.get(simulation);
                if (gameSimUser.isActive){
                    activeCount++;
                }
                if (gameSimUser.isInRoom){
                    inRoomCount++;
                }
                if (gameSimUser.isOnline){
                    onlineCount++;
                }
            }
            Out.info("onlineCount:",onlineCount,"activeCount:",activeCount,"inRoomCount:",inRoomCount);

            int userAbleCount=SimulationCenter.SIMCOUNT-activeCount;
            //判断是要增加还是要减少
            if (count>onlineCount){
                //增加
                int addCount=count-onlineCount;
                for (var simulation: SimulationCenter.simulationUserMap.keySet()){
                    if (addCount>0) {
                        GameSimUser gameSimUser = (GameSimUser) SimulationCenter.simulationUserMap.get(simulation);
                        if (gameSimUser.isActive == false) {
                            gameSimUser.login();
                            gameSimUser.isActive=true;
                            addCount--;
                            Out.info("增加机器人");
                        }
                    }
                }

            }
            if (count<onlineCount){
                //减少
                int revomeCount=onlineCount-count;
                for (var simulation: SimulationCenter.simulationUserMap.keySet()){
                    if (revomeCount>0) {
                        GameSimUser gameSimUser = (GameSimUser) SimulationCenter.simulationUserMap.get(simulation);
                        if (gameSimUser.isActive==true&&gameSimUser.isInRoom==false) {
                            gameSimUser.isActive=false;
                            gameSimUser.loginOut();
                            revomeCount--;
                            Out.info("减少机器人");
                        }
                    }
                }
            }
        }).start();

        return success();
    }

    @Override
    public String doGet(ChannelHandlerContext ctx, FullHttpRequest request, JSONObject params) {
        return doPost(ctx, request, params);
    }

    @Override
    public String getPath() {
        return "/Robot/SimulationActiveRob";
    }
}
