package com.sencorsta.ids.robot.util;

import com.sencorsta.ids.robot.userSimulation.SimulationCenter;

import java.util.List;

import static com.sencorsta.ids.robot.userSimulation.GameSimUser.packageCount;

/**
 * @author Administrator
 * @title: JinQaingTest
 * @projectName rp-server
 * @description: TODO
 * @date 2019/12/1522:11
 */
public class JinQaingTest {


    static int five5 = 0;
    static int five55 = 0;
    static int five = 0;
    static int six6 = 0;
    static int six66 = 0;
    static int six666 = 0;
    static int six = 0;
    static int seven7 = 0;
    static int seven77 = 0;
    static int seven = 0;
    static int nine9 = 0;
    static int nine99 = 0;
    static int nine = 0;
    static int ten10 = 0;
    static int ten = 0;


    public static void main(String[] args) {

        for (int i = 0; i < 100000000; i++) {
            sendJinQiang();
        }

        System.out.println("five5==>" + five5);
        System.out.println("five55==>" + five55);

        System.out.println("six6==>" + six6);
        System.out.println("six66==>" + six66);
        System.out.println("six666==>" + six666);
        System.out.println("seven7==>" + seven7);
        System.out.println("seven77==>" + seven77);
        System.out.println("nine9==>" + nine9);
        System.out.println("nine99==>" + nine99);
        System.out.println("ten10==>" + ten10);

        System.out.println("five==>" + five);
        System.out.println("six==>" + six);
        System.out.println("seven==>" + seven);
        System.out.println("nine==>" + nine);
        System.out.println("ten==>" + ten);


        System.out.println(five5+five55+six6+six66+six666+seven7+seven77+nine9+nine99+ten10+five+six+seven+nine+ten);
    }

      public static void sendJinQiang() {

        int count = packageCount[SimulationCenter.randomAssign(0, 5)];
        int redType = 0;
        if (SimulationCenter.randomProbability() > 95&&count==6) {
            redType = 666;
        }
        List<Integer> thunderNumberList = SimulationCenter.randomMoreThunderNumber(count, redType);

        if (redType == 0) {
            int size = thunderNumberList.size();
            switch (count) {
                case 10:
                    if (size >= 3 && size <= 8) {
                        ten10++;
                    } else {
                        ten++;
                    }
                    break;
                case 9:
                    if (size >= 2 && size <= 7) {
                        nine99++;
                    } else if (size == 1) {
                        nine9++;
                    } else {
                        nine++;
                    }
                    break;
                case 7:
                    if (size >= 2 && size <= 7) {
                        seven77++;
                    } else if (size == 1) {
                        seven7++;
                    } else {
                        seven++;
                    }
                    break;
                case 6:
                    if (size >= 2 && size <= 6) {
                        six66++;
                    } else if (size == 1) {
                        six6++;
                    } else {
                        six++;
                    }
                    break;
                case 5:
                    if (size >= 2 && size <= 5) {
                        five55++;
                    } else if (size == 1) {
                        five5++;
                    } else {
                        five++;
                    }
                    break;
                default:
                    System.out.println(11);
                    break;
            }


        } else {
            int size = thunderNumberList.size();
            if (count == 6 && size >= 2 && size <= 6) {
                six666++;
            } else {
                System.out.println(count+"-----"+size+"-----"+thunderNumberList);
                six++;
            }
        }
    }
}
